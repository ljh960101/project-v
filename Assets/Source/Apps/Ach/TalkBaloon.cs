﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkBaloon : MonoBehaviour {
    FadeOutSystem fo;
    public GameObject tail1, tail2, tail3;
    public UnityEngine.UI.Text text;
	public void Init ()
    {
        fo = GetComponent<FadeOutSystem>();
        fo.Init(2.0f, false, 1.0f, false, false, FadeOutSystem.ChangeType.Zoom);
        gameObject.SetActive(false);
    }
    public void ShowNotify(short type)
    {
        if (gameObject.activeSelf)
        {
            if (fo.after != null) fo.after.Clear();
            fo.Init(4.0f, false, 1.0f, false, false, FadeOutSystem.ChangeType.Fade);
            fo.FadeOut(delegate { ShowNotify(type); });
            return;
        }
        tail1.SetActive(false);
        tail2.SetActive(false);
        tail3.SetActive(false);
        switch (type)
        {
            case 0:
                text.text = "설정 탭에서 1.5배속 설정이 가능합니다.";
                tail1.SetActive(true);
                break;
            case 1:
                text.text = "업적 20% 달성 시\n게임 속도 1.5배속 설정이 가능합니다.";
                tail1.SetActive(true);
                break;
            case 2:
                text.text = "설정 탭에서 2배속 설정이 가능합니다.";
                tail2.SetActive(true);
                break;
            case 3:
                text.text = "업적 50% 달성 시\n게임 속도 2배속 설정이 가능합니다.";
                tail2.SetActive(true);
                break;
            case 4:
                text.text = "업적 100% 달성 시\n히든 CG를 열람 할 수 있습니다.";
                tail3.SetActive(true);
                break;
            default:
                print("Unknown Notify Type");
                break;
        }
        fo.Init(2.0f, false, 1.0f, false, false, FadeOutSystem.ChangeType.Zoom);
        fo.FadeIn(AfterFade);
    }
    void FadeOut()
    {
        fo.Init(4.0f, false, 1.0f, false, false, FadeOutSystem.ChangeType.Fade);
        fo.FadeOut();
    }
    void AfterFade()
    {
        fo.Init(1.0f, false, 2.0f, false, true, FadeOutSystem.ChangeType.Fade);
        fo.FadeOut(FadeOut);
    }
}
