﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;
public class AchMain : MonoBehaviour
{
    AppMain am;
    GameMain gm;
    MainData md;
    public UnityEngine.UI.Text score, scorePercent;
    public UnityEngine.UI.Image fillPercent, bt1, bt2, bt3;
    public Transform list;
    public GameObject achPanel;
    public TalkBaloon talkBaloon;
    [HideInInspector] public int maxScore;
    List<UnityEngine.UI.ScrollRect> fuckingScrolls;
    int count=0;
    public void Reload()
    {
        gameObject.GetComponent<FadeOutSystem>().FadeIn(() =>
        {
            foreach (UnityEngine.UI.ScrollRect sr in fuckingScrolls)
                sr.enabled = true;
        });
        Resort();
        count = 0;
        am.SetAlram(count);
    }
    public void Init()
    {
        am = GetComponent<AppMain>();
        gm = GameMain.GetInstance();
        md = gm.mainData;
        maxScore = 0;
        talkBaloon.Init();
        foreach (var i in md.achMainData.achDatas) maxScore += i.Value.score;
        foreach (var i in md.achMainData.finishList) maxScore += i.score;
        fuckingScrolls = new List<UnityEngine.UI.ScrollRect>();
        foreach (Transform child in transform)
        {
            FindScrollRects(child);
        }
        Resort();
    }
    public void Close()
    {
        foreach (UnityEngine.UI.ScrollRect sr in fuckingScrolls)
            sr.enabled = false;
        am.Close();
    }
    public void FindScrollRects(Transform myTransform)
    {
        UnityEngine.UI.ScrollRect sr = myTransform.GetComponent<UnityEngine.UI.ScrollRect>();
        if (sr)
        {
            fuckingScrolls.Add(sr);
            sr.enabled = false;
        }
        foreach (Transform child in myTransform)
        {
            FindScrollRects(child);
        }
    }
    List<GameObject> panels;
    public void OnButton(int type)
    {
        switch (type)
        {
            case 0:
                if ((float)md.achMainData.score / (float)maxScore >= 0.2f)
                    talkBaloon.ShowNotify(0);
                else
                    talkBaloon.ShowNotify(1);
                break;
            case 1:
                if ((float)md.achMainData.score / (float)maxScore >= 0.5f)
                    talkBaloon.ShowNotify(2);
                else
                    talkBaloon.ShowNotify(3);
                break;
            case 2:
                if ((float)md.achMainData.score / (float)maxScore >= 1f)
                    Modal.GetInstance().CreateImageModal("ach");
                else
                    talkBaloon.ShowNotify(4);
                break;
            default:
                print("wrong Type");
                break;
        }
    }
    public GameObject myEffect;
    public void Resort()
    {
        score.text = md.achMainData.score + " / " + maxScore + "점";
        scorePercent.text = (int)((float)md.achMainData.score/(float)maxScore*100) + "%";
        fillPercent.fillAmount = (float)md.achMainData.score / (float)maxScore;
        if ((float)md.achMainData.score / (float)maxScore >= 1f)
        {
            bt1.color = new Color(1, 1, 1);
            bt2.color = new Color(1, 1, 1);
            bt3.color = new Color(1, 1, 1);
        }
        else if ((float)md.achMainData.score / (float)maxScore >= 0.5f)
        {
            bt1.color = new Color(1, 1, 1);
            bt2.color = new Color(1, 1, 1);
            bt3.color = new Color(0.7f, 0.7f, 0.7f);
        }
        else if ((float)md.achMainData.score / (float)maxScore >= 0.2f)
        {
            bt1.color = new Color(1, 1, 1);
            bt2.color = new Color(0.7f, 0.7f, 0.7f);
            bt3.color = new Color(0.7f, 0.7f, 0.7f);
        }
        else
        {
            bt1.color = new Color(0.7f, 0.7f, 0.7f);
            bt2.color = new Color(0.7f, 0.7f, 0.7f);
            bt3.color = new Color(0.7f, 0.7f, 0.7f);
        }
        if (panels == null) panels = new List<GameObject>();
        foreach (var i in panels) Destroy(i);
        panels.Clear();
        foreach (var i in md.achMainData.achDatas)
        {
            GameObject newObject = Instantiate(achPanel, list);
            newObject.transform.Find("Check").gameObject.SetActive(false);
            newObject.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = i.Value.arcName;
            newObject.transform.Find("UnCheck").Find("Score").GetComponent<UnityEngine.UI.Text>().text = i.Value.score + "점";
            newObject.transform.Find("Content").GetComponent<UnityEngine.UI.Text>().text = i.Value.arcContent;
            panels.Add(newObject);
        }
        foreach (var i in md.achMainData.finishList)
        {
            GameObject newObject = Instantiate(achPanel, list);
            newObject.GetComponent<UnityEngine.UI.Image>().color = new Color(0.7f, 0.7f, 0.7f, 1.0f);
            newObject.transform.Find("Check").gameObject.SetActive(true);
            newObject.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = i.arcName;
            newObject.transform.Find("UnCheck").Find("Score").GetComponent<UnityEngine.UI.Text>().text = i.score + "점";
            newObject.transform.Find("Content").GetComponent<UnityEngine.UI.Text>().text = i.arcContent;
            panels.Add(newObject);
        }
    }
    void CompleteAch(int appCode)
    {
        var achData = md.achMainData.achDatas[appCode];
        md.achMainData.score += achData.score;
        md.achMainData.finishList.Add(achData);
        md.achMainData.achDatas.Remove(achData.arcCode);
        ToolClass.MakeNotify(AppType.Ach, "<color=green>" + achData.arcName + "</color> 달성!");
        Instantiate(myEffect);
        ++count;
        am.SetAlram(count);
    }
    public void UpdateAch()
    {
        int miniGameScore = 0, randChatMany = 0, lowestLevel = 999, picCount = 0;
        List<int> completeAchNumbers = new List<int>();
        foreach (var j in md.miniGameData.scores)
        {
            if (j.charCode == 0)
            {
                miniGameScore = j.score;
                break;
            }
        }
        foreach (var j in md.characters)
        {
            if (j.alreadyRandChated == true)
            {
                ++randChatMany;
            }
        }
        foreach (var j in md.characters)
        {
            if (!j.isGuest)
            {
                if (lowestLevel > j.level)
                {
                    if (j.charCode == 1 && j.level + 2 < lowestLevel) lowestLevel = j.level + 2;
                    else lowestLevel = j.level;
                }
            }
            picCount += j.collectedPhotos.Count;
        }
        int lanchAppCount = 0;
        foreach (var j in md.achMainData.lanchAppChecker)
        {
            if (j.Value == true)
            {
                ++lanchAppCount;
            }
        }
        int endingCount = 0;
        foreach (var j in md.characters)
        {
            if (j.afterEnding == true)
            {
                ++endingCount;
            }
        }
        foreach (var i in md.achMainData.achDatas)
        {
            switch (i.Value.achType)
            {
                case AchType.MiniGameScoreAch:
                    if (miniGameScore >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
                case AchType.RandomChatMany:
                    if (randChatMany >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
                case AchType.CharacterLevel:
                    if (lowestLevel >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
                case AchType.CollectPic:
                    if(picCount >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
                case AchType.LoginMany:
                    if (md.achMainData.loginCount >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
                case AchType.EndingMany:
                    if (endingCount >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
                case AchType.LanchApp:
                    if (lanchAppCount >= i.Value.arcValue) completeAchNumbers.Add(i.Key);
                    break;
            }
        }
        foreach(var i in completeAchNumbers)
        {
            CompleteAch(i);
        }
    }
    void FixedUpdate ()
    {
        BackButtonProc();
    }
    private static AchMain instance;
    public static AchMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("AchMain").GetComponent<AchMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                backButtonTime = 0.0f;
                Close();
            }
        }
    }
}