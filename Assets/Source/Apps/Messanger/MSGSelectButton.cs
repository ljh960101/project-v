﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSGSelectButton : MonoBehaviour {

    // Use this for initialization
    MessangerMain mcm;
	void Start () {
        mcm = MessangerMain.GetInstance();
        UnityEngine.UI.Button btn = GetComponent<UnityEngine.UI.Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    void TaskOnClick()
    {
        mcm.SelectComplete(this.name);
    }
}
