﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using MyDesign;
using MyTool;

public class MessangerMain : MonoBehaviour
{
    public GameObject chatPanelPrefab;
    public GameObject targetToChatList;

    [HideInInspector] public AppMain am;
    GameMain gm;
    GameObject Tab_friendList, Tab_ChatList, Tab_Chat;
    List<UnityEngine.UI.ScrollRect> fuckingScrolls;
    [HideInInspector] public EMSGScreen currentScreen;
    List<MSG_ChatPanel> chatPanels;
    GameObject lastChatPanel;
    ChatLog clickedChatLog;

    // Chat Move Proc
    bool onMove;
    bool onAutoScroll;
    Vector2 lastTouch;
    
    public Transform friendListContent;
    public GameObject friendProfile;
    public MSGMiniPopup profileMiniPopUp;
    public MSGProfileEditPanel profileEditPanel;
    public GameObject resetPanel;
    List<GameObject> friendPanels;
    public void OnClickResetBtn()
    {
        resetPanel.transform.Find("ProfileEditView").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "[<color=blue>" + ToolClass.GetCharacter(chattingTargetCharCode).name + "</color>]의 채팅을 처음부터 다시 시작 하시겠습니까?\n<color=red>주의!</color> 되돌릴 수 없습니다.";
        resetPanel.GetComponent<FadeOutSystem>().FadeIn();
    }
    public void SoftReset()
    {
        ChangeScreen(EMSGScreen.FRIENDLIST);
        var md = GameMain.GetInstance().mainData;
        foreach(var i in md.chatLogs)
        {
            if(i.charCode == chattingTargetCharCode)
            {
                md.chatLogs.Remove(i);
                break;
            }
        }
        var character = ToolClass.GetCharacter(chattingTargetCharCode);
        character.level = 1;
        character.love = 0;
        character.scriptPriority = 1;
        character.currentScriptCode = 0;
        character.currentScriptPosition = 0;
        character.currentScriptProcess = 0;
        character.onGameCompetition = false;
        character.onOverlapScript = false;
        character.scriptCheck = new Dictionary<int, bool>();
        GameMain.GetInstance().GotoNextScript(character);
        Resort();
        CloseResetBtn();
    }
    public void CloseResetBtn()
    {
        resetPanel.GetComponent<FadeOutSystem>().FadeOut();
    }
    public void Reload()
    {
        ToolClass.Save();
        Transform cover = gameObject.transform.Find("Cover");
        if (cover)
        {
            cover.gameObject.SetActive(true);
            cover.GetComponent<FadeOutSystem>().Init(2, true, 3f, false);
            cover.GetComponent<FadeOutSystem>().FadeOut();

            // 게임 설치후 첫 방문
            if (false && GameMain.GetInstance().mainData.msgName == "[Default]")
            {
                GameMain.GetInstance().mainData.msgName = "";
                transform.Find("ChangeName").gameObject.SetActive(true);
                transform.Find("ChangeName").GetComponent<MSGProfileEditPanel>().Init();
                transform.Find("ChangeName").GetComponent<FadeOutSystem>().FadeIn();
            }
            else
            {
                Destroy(transform.Find("ChangeName").gameObject);
            }
        }
        gameObject.GetComponent<FadeOutSystem>().FadeIn(() =>
        {
            foreach (UnityEngine.UI.ScrollRect sr in fuckingScrolls)
                sr.enabled = true;
        });
        Resort();
    }
    public void OnProfileClick(string name)
    {
        profileMiniPopUp.Open(int.Parse(name));
    }
    public void OnMyProfileClick()
    {
        profileEditPanel.Open();
    }
    void RefreshFriendList()
    {
        friendListContent.Find("MyProfile").Find("MyProfilePanel").Find("CircularPhoto").Find("Name").GetComponent<UnityEngine.UI.Text>().text = GameMain.GetInstance().mainData.msgName;
        for (int i=0; i<friendPanels.Count; ++i)
        {
            Destroy(friendPanels[i]);
        }
        friendPanels.Clear();

        foreach(MyCharacter character in gm.mainData.characters)
        {
            if(ToolClass.GetCharacterChatType(character) == ChatType.CHAT)
            {

                GameObject newProfile = Instantiate(friendProfile, friendListContent);
                newProfile.name = character.charCode + "";
                newProfile.transform.Find("Image").Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curMsgPic);
                newProfile.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = character.name;
                if (character.statusMsg == null || character.statusMsg.Length <= 0)
                {
                    newProfile.transform.Find("MsgBox").gameObject.SetActive(false);
                }
                else
                {
                    newProfile.transform.Find("MsgBox").gameObject.SetActive(true);
                    newProfile.transform.Find("MsgBox").Find("MsgText").GetComponent<UnityEngine.UI.Text>().text = ToolClass.MakeStringPretty(character.statusMsg, 15, true, 2);
                }
                newProfile.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnProfileClick(newProfile.name); });
                friendPanels.Add(newProfile);
            }
        }
    }
    public void AddToLog(int charCode, string script)
    {
        bool exist = false;
        MainData mainData = GameMain.GetInstance().mainData;
        foreach (ChatLog log in mainData.chatLogs)
        {
            if (log.charCode == charCode && !log.isDead)
            {
                exist = true;
                BeforeChat bc = new BeforeChat();
                bc.chat = script;
                bc.date = DateTime.Now;
                log.beforeChats.Add(bc);
                string smallScript = ToolClass.ParseToSmallScript(script);
                if (smallScript != "")
                {
                    ToolClass.MakeNotify(AppType.Messenger, ToolClass.GetName(log.charCode, ChatType.CHAT) + ": " + smallScript, charCode);
                    AddAlram(charCode, smallScript);
                }
                break;
            }
        }
        if (!exist)
        {

            ChatLog cl = new ChatLog();
            cl.alram = 0;
            cl.beforeChats = new List<BeforeChat>();
            cl.charCode = charCode;
            cl.isDead = false;
            gm.mainData.chatLogs.Add(cl);
            AddToLog(charCode, script);
        }
        else
        {
            Resort();
        }
    }

    public void ScrollMoved()
    {
        float val = chat_panel.parent.Find("Scrollbar").GetComponent<Scrollbar>().value;
        if (val <= 0.01) TurnOnAutoScroll(true);
        else onAutoScroll = false;
    }
    public void AddAlram(int charCode, string text)
    {
        // 현재 채팅중이면 걍 씹음
        if (gm.currentApp == gameObject && (chattingTargetCharCode == charCode && currentScreen == EMSGScreen.CHAT)) return;
        if (gm.currentApp == gameObject && !gm.gameObject.GetComponent<ScriptMaker>()) Modal.GetInstance().CreateMSGAlram(text, charCode);

        // beforeLog에 알람 추가.
        foreach (ChatLog log in gm.mainData.chatLogs)
        {
            if(log.charCode == charCode)
            {
                ++log.alram;
                break;
            }
        }
        RefreshHomeAlram();
    }

    public enum EMSGScreen
    {
        FRIENDLIST = 0,
        CHATLIST = 1,
        CHAT = 2,
    }
    public void FindScrollRects(Transform myTransform)
    {
        UnityEngine.UI.ScrollRect sr = myTransform.GetComponent<UnityEngine.UI.ScrollRect>();
        if (sr)
        {
            fuckingScrolls.Add(sr);
            sr.enabled = false;
        }
        foreach (Transform child in myTransform)
        {
            FindScrollRects(child);
        }
    }
    public Transform menus;
    public void Init()
    {
        am = GetComponent<AppMain>();
        gm = GameMain.GetInstance();

        fuckingScrolls = new List<UnityEngine.UI.ScrollRect>();
        foreach (Transform child in transform)
        {
            FindScrollRects(child);
        }

        Tab_Chat = menus.Find("Chat").gameObject;
        Tab_friendList = menus.Find("SubMenus").Find("SubPanel").Find("FriendList").gameObject;
        Tab_ChatList = menus.Find("SubMenus").Find("SubPanel").Find("ChatList").gameObject;
        selectPanel = Tab_Chat.transform.Find("LowerBar").Find("SelectPanel").gameObject;
        lowerBar = Tab_Chat.transform.Find("LowerBar").gameObject;
        typeText = lowerBar.transform.Find("TypeText").GetComponent<UnityEngine.UI.Text>();
        chat_panel = Tab_Chat.transform.Find("ChatList").Find("Background").Find("Chats");
        lastChatPanel = lowerBar.transform.Find("LastChatPanel").gameObject;
        currentScreen = EMSGScreen.FRIENDLIST;
        TurnOnAutoScroll();
        profileMiniPopUp.Init();
        profileEditPanel.Init();

        chatPanels = new List<MSG_ChatPanel>();
        chats = new List<GameObject>();
        friendPanels = new List<GameObject>();

        Tab_friendList.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, true);
        Tab_ChatList.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, true);
        Tab_Chat.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        resetPanel.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);

        //Tab_friendList.SetActive(true);
        //Tab_ChatList.SetActive(false);
        //Tab_Chat.SetActive(false);
        ChangeScreen(EMSGScreen.FRIENDLIST);
    }
    void RefreshChatList()
    {
        if (gm.mainData.chatLogs.Count >= 2)
        {
            gm.mainData.chatLogs.Sort(delegate (ChatLog A, ChatLog B)
            {
                if (A.beforeChats.Count == 0) return 1;
                else if (B.beforeChats.Count == 0) return -1;
                else if (A.beforeChats[A.beforeChats.Count - 1].date < B.beforeChats[B.beforeChats.Count - 1].date) return 1;
                else if (A.beforeChats[A.beforeChats.Count - 1].date > B.beforeChats[B.beforeChats.Count - 1].date) return -1;
                else return 0;
            });
        }

        float firstPos;
        if (chatPanels.Count >= 1) firstPos = ((RectTransform)chatPanels[0].transform).anchoredPosition.y;
        else firstPos = -10f;
        foreach(MSG_ChatPanel chatPanel in chatPanels)
        {
            Destroy(chatPanel.gameObject);
        }
        chatPanels.Clear();

        foreach (MyCharacter character in gm.mainData.characters)
        {
            if (ToolClass.GetCharacterChatType(character) != ChatType.CHAT) continue;

            bool haveLastChat = false;
            for (int i = 0; i < gm.mainData.chatLogs.Count; ++i)
            {
                // 캐릭터와 같은 챗로그를 찾음
                if (character.charCode == gm.mainData.chatLogs[i].charCode)
                {
                    // 마지막 채팅 형식을 찾는다.
                    GameObject newObj;
                    // 마지막 스크립트를 파싱해서 만들어줌.
                    for (int j = gm.mainData.chatLogs[i].beforeChats.Count - 1; j >= 0; --j)
                    {
                        string result = ToolClass.ParseToSmallScript(gm.mainData.chatLogs[i].beforeChats[j].chat);
                        if (result != "")
                        {
                            newObj = Instantiate(chatPanelPrefab, targetToChatList.transform);
                            newObj.GetComponent<MSG_ChatPanel>().Init(ToolClass.GetName(gm.mainData.chatLogs[i].charCode, ChatType.CHAT),
                            result,
                            character.charCode,
                            character.curMsgPic,
                            gm.mainData.chatLogs[i].beforeChats[j].date,
                            gm.mainData.chatLogs[i].alram,
                            gm.mainData.chatLogs[i]);
                            chatPanels.Add(newObj.GetComponent<MSG_ChatPanel>());
                            Vector2 beforePos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            if (chatPanels.Count >= 2) beforePos.y = ((RectTransform)chatPanels[chatPanels.Count - 2].transform).anchoredPosition.y - 75f;
                            else beforePos.y = firstPos;
                            newObj.GetComponent<RectTransform>().anchoredPosition = beforePos;
                            haveLastChat = true;
                            newObj.SetActive(true);
                            break;
                        }
                    }
                    // 마지막 채팅이 없다면 빈화면으로 만들어준다.
                    if (!haveLastChat)
                    {
                        newObj = Instantiate(chatPanelPrefab, targetToChatList.transform);
                        newObj.GetComponent<MSG_ChatPanel>().Init(ToolClass.GetName(character.charCode, ChatType.CHAT),
                        "",
                        character.charCode,
                        character.curMsgPic,
                        DateTime.Now,
                        gm.mainData.chatLogs[i].alram,
                        gm.mainData.chatLogs[i]);
                        chatPanels.Add(newObj.GetComponent<MSG_ChatPanel>());
                        Vector2 beforePos = newObj.GetComponent<RectTransform>().anchoredPosition;
                        if (chatPanels.Count >= 2) beforePos.y = ((RectTransform)chatPanels[chatPanels.Count - 2].transform).anchoredPosition.y - 75f;
                        else beforePos.y = firstPos;
                        newObj.GetComponent<RectTransform>().anchoredPosition = beforePos;
                        haveLastChat = true;
                        newObj.SetActive(false);
                    }
                    break;
                }
            }
            // 친구지만 챗로그가 없는 경우 껍데기만 만들어준다.
            if (!haveLastChat)
            {
                ChatLog cl = new ChatLog();
                cl.alram = 0;
                cl.beforeChats = new List<BeforeChat>();
                cl.charCode = character.charCode;
                cl.isDead = false;
                gm.mainData.chatLogs.Add(cl);

                RefreshChatList();
                return;
            }
        }
    }
    public void Close()
    {
        foreach (UnityEngine.UI.ScrollRect sr in fuckingScrolls)
            sr.enabled = false;
        am.Close();
    }
    public void Back()
    {
        switch (currentScreen)
        {
            case EMSGScreen.FRIENDLIST:
                {
                    Close();
                    break;
                }
            case EMSGScreen.CHATLIST:
                {
                    ChangeScreen(EMSGScreen.FRIENDLIST);
                    break;
                }
            case EMSGScreen.CHAT:
                {
                    ChangeScreen(EMSGScreen.CHATLIST);
                    break;
                }
        }
    }

    public void ChangeScreen(int type)
    {
        ChangeScreen((EMSGScreen)type);
    }

    public Transform friend_chat_topBar;
    public GameObject resetBtn;
    float changedDelay = 0f;
    public void ChangeScreen(EMSGScreen type) // Matching = 0, ChatList, Chat
    {
        changedDelay = 0.2f; 
        switch (type)
        {
            case EMSGScreen.FRIENDLIST:
                friend_chat_topBar.Find("topBarL").Find("F0").gameObject.SetActive(false);
                friend_chat_topBar.Find("topBarL").Find("F1").gameObject.SetActive(true);

                friend_chat_topBar.Find("topBarR").Find("C0").gameObject.SetActive(true);
                friend_chat_topBar.Find("topBarR").Find("C1").gameObject.SetActive(false);
                break;
            case EMSGScreen.CHATLIST:
                friend_chat_topBar.Find("topBarL").Find("F0").gameObject.SetActive(true);
                friend_chat_topBar.Find("topBarL").Find("F1").gameObject.SetActive(false);

                friend_chat_topBar.Find("topBarR").Find("C0").gameObject.SetActive(false);
                friend_chat_topBar.Find("topBarR").Find("C1").gameObject.SetActive(true);
                break;
            case EMSGScreen.CHAT:
                friend_chat_topBar.Find("topBarL").Find("F0").gameObject.SetActive(true);
                friend_chat_topBar.Find("topBarL").Find("F1").gameObject.SetActive(false);

                friend_chat_topBar.Find("topBarR").Find("C0").gameObject.SetActive(false);
                friend_chat_topBar.Find("topBarR").Find("C1").gameObject.SetActive(true);
                if(currentScreen!=EMSGScreen.CHAT) ScrollDown(true);
                if (ToolClass.GetCharacter(chattingTargetCharCode).afterEnding) resetBtn.SetActive(true);
                else resetBtn.SetActive(false);
                break;
        }
        currentScreen = type;
        /*switch (type)
        {
            case EMSGScreen.FRIENDLIST:
                {
                    RefreshFriendList();
                    if (Tab_ChatList.activeSelf)
                    {
                        Tab_ChatList.GetComponent<FadeOutSystem>().Reset();
                        Tab_ChatList.SetActive(false);
                    }
                    if (Tab_Chat.activeSelf)
                    {
                        Tab_Chat.GetComponent<FadeOutSystem>().Reset();
                        Tab_Chat.SetActive(false);
                    }
                    if (!Tab_friendList.activeSelf)
                    {
                        Tab_friendList.GetComponent<FadeOutSystem>().Reset();
                        Tab_friendList.SetActive(true);
                    }
                    currentScreen = EMSGScreen.FRIENDLIST;
                    break;
                }
            case EMSGScreen.CHATLIST:
                {
                    RefreshChatList();
                    if (!Tab_ChatList.activeSelf)
                    {
                        Tab_ChatList.GetComponent<FadeOutSystem>().Reset();
                        Tab_ChatList.SetActive(true);
                    }
                    if (Tab_Chat.activeSelf)
                    {
                        Tab_Chat.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        Tab_Chat.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    if (Tab_friendList.activeSelf)
                    {
                        Tab_friendList.GetComponent<FadeOutSystem>().Reset();
                        Tab_friendList.SetActive(false);
                    }
                    currentScreen = EMSGScreen.CHATLIST;
                    break;
                }
            case EMSGScreen.CHAT:
                {
                    if (Tab_ChatList.activeSelf)
                    {
                        Tab_ChatList.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    if (!Tab_Chat.activeSelf)
                    {
                        Tab_Chat.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        Tab_Chat.GetComponent<FadeOutSystem>().FadeIn(delegate
                        {
                            ScrollDown();
                        });
                        ScrollDown();
                    }
                    if (Tab_friendList.activeSelf)
                    {
                        Tab_friendList.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    currentScreen = EMSGScreen.CHAT;
                    break;
                }
        }*/
    }

    public GameObject Chat_Photo, Chat_MyText, Chat_Image, Chat_Text, Chat_Line, Chat_Address, Chat_Game_NPC, Chat_Game_PC, Chat_Profile;
    List<GameObject> chats;
    Transform chat_panel;
    const float fontLineGab = 62.1f; // 폰트 1줄당 갭
    const float textBoxGab = 30f; // 박스의 패딩값
    const int defaultGab = 60; // 기본 간격
    const int firstLineGab = -20; // 첫 줄의 간격
    [HideInInspector] public int chattingTargetCharCode;

    // charcode,알람으로 찾아가기 위한 함수
    public void OpenChat(int charCode)
    {
        foreach (MSG_ChatPanel panel in chatPanels)
        {
            if (panel.charCode == charCode && !panel.cl.isDead)
            {
                OpenChat(panel);
                return;
            }
        }
        ScrollDown();
    }
    // 갱신해서 찾아가기 위한 함수
    public void OpenChat(ChatLog cl)
    {
        foreach (MSG_ChatPanel panel in chatPanels)
        {
            if (panel.cl == cl)
            {
                OpenChat(panel);
                break;
            }
        }
    }
    public void OnClickAddress(string charCode)
    {
        GameObject sns = SnsMain.GetInstance().gameObject;

        ToolClass.CollectPhoto(int.Parse(charCode), ToolClass.GetCharacter(int.Parse(charCode)).curSnsPic);
        Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
        sns.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        sns.GetComponent<FadeOutSystem>().FadeIn();
        SnsMain.GetInstance().Reload();
        gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        gameObject.GetComponent<FadeOutSystem>().FadeOut();
        SnsMain.GetInstance().ChangeScreen(SnsMain.ESnsScreenType.Main);
        Invoke("SetSnsImage", 0.3f);
    }
    public void SetSnsImage()
    {
        SnsMain.GetInstance().st.SwipeTo(1);
    }

    [HideInInspector] public bool lastIsPC = true; // 마지막으로 대화한게 PC인가?
    int lastScriptPos;
    float lastGab;
    bool firstChat;
    GameObject selectPanel;
    GameObject lowerBar;
    public GameObject selectButton, chatBackground;
    UnityEngine.UI.Text typeText;
    public void SetTypeText(string text)
    {
        typeText.text = text;
    }
    public void OpenChat(MSG_ChatPanel clickedChatPanel)
    {
        SetUnderScreen(false);
        Tab_Chat.transform.Find("IMAGE_Title").Find("Name").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(clickedChatPanel.charCode).name;

        for (int k = 0; k < selectPanel.transform.childCount; ++k)
            Destroy(selectPanel.transform.GetChild(k).gameObject);

        // 다른 창에서 넘어왔다면 리스트 비워줌
        clickedChatLog = clickedChatPanel.cl;

        bool isRefresh = false;
        if ((currentScreen != EMSGScreen.CHAT || chattingTargetCharCode != clickedChatPanel.charCode))
        {
            SetTypeText("");
            isRefresh = true;
            for (int k = chats.Count - 1; k >= 0; --k)
            {
                Destroy(chats[k].gameObject);
                chats.Remove(chats[k]);
            }
        }
        chattingTargetCharCode = clickedChatPanel.charCode;

        // 새로고침이면 먼저 PC체크
        if (isRefresh)
        {
            firstChat = true;
            lastIsPC = true;
        }

        // 먼저 일치하는 로그를 찾는다.
        for (int i=0 ; i < gm.mainData.chatLogs.Count; ++i)
        {
            if (gm.mainData.chatLogs[i] == clickedChatPanel.cl)
            {
                // 이후 실행함.

                // 새로고침이면 기본값, 아니면 마지막위치를 - gab 가져옴
                float nextPoint = firstLineGab;
                if (!isRefresh)
                {
                    if (chats.Count <= 0) nextPoint = firstLineGab;
                    else nextPoint = ((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y - lastGab;
                }

                lastGab = -9999;
                int j = 0;
                if (!isRefresh) j = lastScriptPos;
                else lastScriptPos = 0;
                if (gm.currentApp.GetComponent<MessangerMain>()) gm.mainData.chatLogs[i].alram = 0;
                RefreshHomeAlram();

                for (; j < gm.mainData.chatLogs[i].beforeChats.Count; ++j)
                {
                    BeforeChat bc = gm.mainData.chatLogs[i].beforeChats[j];
                    GameObject newObj = null;
                    ScriptParsedData chat = ToolClass.ParseScript(bc.chat);
                    // 내 채팅
                    // 상대 채팅
                    if (chat.type != ScriptType.NORMAL) // 특수 채팅
                    {
                        if (chat.type == ScriptType.PHOTO) // 사진
                        {
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Image, chat_panel);
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.GetComponent<PhotoScript>().SetImageByCode(chat.val);
                        }
                        else if (chat.type == ScriptType.PROFILE) // 메신저 프로필
                        {
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Profile, chat_panel);
                            newObj.name = chattingTargetCharCode + "";
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.transform.Find("CircularPhoto").GetComponent<CircularPhoto>().Init(ToolClass.GetCharacter(chattingTargetCharCode).curMsgPic);
                            newObj.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(chattingTargetCharCode).name;
                        }
                        else if (chat.type == ScriptType.ADRESS) // 메신저 프로필
                        {
                            ToolClass.GetCharacter(chattingTargetCharCode).bOpenSns = true;
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Address, chat_panel);
                            newObj.name = chattingTargetCharCode + "";
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(chattingTargetCharCode).snsId;
                            newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnClickAddress(newObj.name); });
                        }
                        else if (chat.type == ScriptType.GAME_NPC) // 게임 프로필
                        {
                            int score = int.Parse(chat.val);
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Game_NPC, chat_panel);
                            newObj.name = chattingTargetCharCode + "GAME";
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.transform.Find("Title").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(chattingTargetCharCode).name + "님이 새로운 기록을 달성하셨어요!\n<size=45>" + ToolClass.GetCharacter(chattingTargetCharCode).name + "님의 점수</size>\n<color=#FA9042FF><size=80>" + score + "</size></color>";
                            newObj.transform.Find("Button").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
                                delegate
                                {
                                    GameObject game = MiniGameMain.GetInstance().gameObject;

                                    Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
                                    game.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                                    game.GetComponent<FadeOutSystem>().FadeIn();
                                    MiniGameMain.GetInstance().Reload();
                                    gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                                    gameObject.GetComponent<FadeOutSystem>().FadeOut();
                                    MiniGameMain.GetInstance().ChangeScreen(0);
                                });
                        }
                        else if (chat.type == ScriptType.GAME_PC) // 게임 프로필
                        {
                            int score = int.Parse(chat.val);
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Game_PC, chat_panel);
                            newObj.name = chattingTargetCharCode + "GAME";
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.transform.Find("Title").GetComponent<UnityEngine.UI.Text>().text = GameMain.GetInstance().mainData.msgName + "님이 새로운 기록을 달성하셨어요!\n<size=45>" + GameMain.GetInstance().mainData.msgName + "님의 점수</size>\n<color=#FA9042FF><size=80>" + score + "</size></color>";
                            newObj.transform.Find("Button").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
                                delegate
                                {
                                    GameObject game = MiniGameMain.GetInstance().gameObject;

                                    Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
                                    game.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                                    game.GetComponent<FadeOutSystem>().FadeIn();
                                    MiniGameMain.GetInstance().Reload();
                                    gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                                    gameObject.GetComponent<FadeOutSystem>().FadeOut();
                                    MiniGameMain.GetInstance().ChangeScreen(0);
                                });
                        }
                        else if (chat.type == ScriptType.WAIT) // 대기
                        {
                            continue;
                        }
                        else if (chat.type == ScriptType.SELECT_START) // 선택
                        {
                            continue;
                        }
                        else if (chat.type == ScriptType.LINE) // 라인
                        {
                            firstChat = true;
                            nextPoint -= defaultGab * 2;
                            newObj = Instantiate(Chat_Line, chat_panel);
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= defaultGab * 2;
                            lastIsPC = true;
                        }
                        else if (chat.type == ScriptType.NPC_START) // NPC (포토올림)
                        {
                            if (lastIsPC)
                            {
                                firstChat = true;
                                lastIsPC = false;
                                nextPoint -= defaultGab / 2;
                                newObj = Instantiate(Chat_Photo, chat_panel);
                                Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                                pos.y = nextPoint;
                                newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                                newObj.transform.Find("CircularPhoto").GetComponent<CircularPhoto>().Init(ToolClass.GetCharacter(clickedChatPanel.charCode).curMsgPic);
                                newObj.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(clickedChatPanel.charCode).name;
                            }
                            else continue;
                        }
                        else if (chat.type == ScriptType.NPC_NOREGI) // NPC (포토올림)
                        {
                            firstChat = true;
                            lastIsPC = false;
                            nextPoint -= defaultGab / 2;
                            newObj = Instantiate(Chat_Photo, chat_panel);
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            newObj.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = chat.val;
                        }
                        else if (chat.type == ScriptType.PC_START) // PC
                        {
                            if (!lastIsPC) { 
                                firstChat = true;
                                lastIsPC = true;
                            }
                            continue;
                        }
                    }
                    // 일반 채팅
                    else if (lastIsPC) // PC의 채팅
                    {
                        string prettyChat = ToolClass.MakeStringPretty(chat.val, 16, false);
                        int lineMany = prettyChat.Split('\n').Length;
                        {
                            bool chk = false;
                            for (int k = j - 1; k >= 0; --k)
                            {
                                switch (ToolClass.ParseScript(gm.mainData.chatLogs[i].beforeChats[k].chat).type)
                                {
                                    case ScriptType.ADRESS:
                                    case ScriptType.LINE:
                                    case ScriptType.NPC_NOREGI:
                                    case ScriptType.NPC_START:
                                    case ScriptType.PC_START:
                                    case ScriptType.PHOTO:
                                    case ScriptType.PROFILE:
                                        nextPoint -= defaultGab;
                                        chk = true;
                                        break;
                                    case ScriptType.NORMAL:
                                        nextPoint -= defaultGab / 20f;
                                        chk = true;
                                        break;
                                }
                                if (chk) break;
                            }
                        }
                        newObj = Instantiate(Chat_MyText, chat_panel);
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<UnityEngine.UI.Text>().text = prettyChat;
                        newObj.transform.Find("Textbox").Find("Text").Find("RealText").GetComponent<UnityEngine.UI.Text>().text = prettyChat;
                        Vector2 beforePos = newObj.GetComponent<RectTransform>().anchoredPosition;
                        beforePos.y = nextPoint;
                        newObj.transform.GetComponent<RectTransform>().anchoredPosition = beforePos;

                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").Find("Text").transform);
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").transform);
                        ((RectTransform)newObj.transform).sizeDelta = ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta;

                        if (Mathf.Abs(((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y) <= 40)
                        {
                            if (lineMany * fontLineGab + textBoxGab < 92f)
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, 92f);
                                nextPoint -= 92f + defaultGab / 3;
                            }
                            else
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, lineMany * fontLineGab + textBoxGab);
                                nextPoint -= lineMany * fontLineGab + textBoxGab + defaultGab / 3;
                            }
                        }
                        else
                        {
                            nextPoint -= ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y + defaultGab / 3;
                        }
                        if (firstChat)
                        {
                            firstChat = false;
                        }
                        else
                        {
                            newObj.transform.Find("Tail").GetComponent<UnityEngine.UI.Image>().color = new Color(0, 0, 0, 0);
                        }
                    }
                    else
                    {
                        // NPC의 채팅
                        string prettyChat = ToolClass.MakeStringPretty(chat.val, 16, false);
                        int lineMany = prettyChat.Split('\n').Length;
                        {
                            bool chk = false;
                            for (int k = j - 1; k >= 0; --k)
                            {
                                switch (ToolClass.ParseScript(gm.mainData.chatLogs[i].beforeChats[k].chat).type)
                                {
                                    case ScriptType.ADRESS:
                                    case ScriptType.LINE:
                                    case ScriptType.NPC_START:
                                    case ScriptType.NPC_NOREGI:
                                    case ScriptType.PC_START:
                                    case ScriptType.PHOTO:
                                    case ScriptType.PROFILE:
                                        nextPoint -= defaultGab;
                                        chk = true;
                                        break;
                                    case ScriptType.NORMAL:
                                        nextPoint -= defaultGab / 20f;
                                        chk = true;
                                        break;
                                }
                                if (chk) break;
                            }
                        }
                        newObj = Instantiate(Chat_Text, chat_panel);
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<UnityEngine.UI.Text>().text = ToolClass.MakeStringPretty(prettyChat, 16, false);
                        newObj.transform.Find("Textbox").Find("Text").Find("RealText").GetComponent<UnityEngine.UI.Text>().text = ToolClass.MakeStringPretty(prettyChat, 16, false);
                        Vector2 beforePos = newObj.transform.GetComponent<RectTransform>().anchoredPosition;
                        beforePos.y = nextPoint;
                        newObj.transform.GetComponent<RectTransform>().anchoredPosition = beforePos;

                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").Find("Text").transform);
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").transform);
                        ((RectTransform)newObj.transform).sizeDelta = ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta;

                        if (Mathf.Abs(((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y) <= 40)
                        {
                            if (lineMany * fontLineGab + textBoxGab < 92f)
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, 92f);
                                nextPoint -= 92f + defaultGab / 3;
                            }
                            else
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, lineMany * fontLineGab + textBoxGab);
                                nextPoint -= lineMany * fontLineGab + textBoxGab + defaultGab / 3;
                            }
                        }
                        else
                        {
                            nextPoint -= ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y + defaultGab / 3;
                        }
                        if (firstChat)
                        {
                            firstChat = false;
                        }
                        else
                        {
                            newObj.transform.Find("Tail").GetComponent<UnityEngine.UI.Image>().color = new Color(0, 0, 0, 0);
                        }
                    }

                    if (newObj != null) chats.Add(newObj);
                }

                if (gm.mainData.chatLogs[i].beforeChats.Count >= 1)
                {
                    // 마지막이 선택지 선택이거나 게임 시작이라면..
                    ScriptParsedData chat2 = ToolClass.ParseScript(gm.mainData.chatLogs[i].beforeChats[gm.mainData.chatLogs[i].beforeChats.Count - 1].chat);
                    if (chat2.type == ScriptType.SELECT_START) // 선택
                    {
                        // 마지막 대화가 Select이고 진행도가 1. 즉 지금 해야할때라면?
                        if (ToolClass.GetCharacter(chattingTargetCharCode).currentScriptProcess == 1f)
                        {
                            // 먼저 모든 버튼을 삭제
                            for (int k = 0; k < selectPanel.transform.childCount; ++k)
                                Destroy(selectPanel.transform.GetChild(k).gameObject);

                            string[] selects = chat2.val.Split('$');
                            foreach (string select in selects)
                            {
                                GameObject sb = Instantiate(selectButton, selectPanel.transform);
                                sb.name = select;
                                sb.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = select;
                            }
                            SetUnderScreen(true);
                            if (chats.Count <= 0) lastGab = 0;
                            else lastGab = (((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y) - nextPoint;
                        }
                    }
                    else if (chat2.type == ScriptType.GAME_START) // 게임 시작
                    {
                        // 마지막 대화가 GAME_START이고 진행도가 1. 즉 지금 해야할때라면?
                        if (ToolClass.GetCharacter(chattingTargetCharCode).currentScriptProcess == 1f)
                        {
                            // 먼저 모든 버튼을 삭제
                            for (int k = 0; k < selectPanel.transform.childCount; ++k)
                                Destroy(selectPanel.transform.GetChild(k).gameObject);

                            {
                                GameObject sb = Instantiate(selectButton, selectPanel.transform);
                                sb.name = ScriptType.GAME_START.ToString();
                                sb.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "도전을 받아들인다.";
                            }
                            {
                                GameObject sb = Instantiate(selectButton, selectPanel.transform);
                                sb.name = ScriptType.GAME_IGNORE.ToString();
                                sb.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "정중히 거절한다.";
                            }
                            SetUnderScreen(true);
                            if (chats.Count <= 0) lastGab = 0;
                            else lastGab = (((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y) - nextPoint;
                        }
                    }
                }

                if (chats.Count >= 2)
                {
                    ((RectTransform)chatBackground.transform).sizeDelta = new Vector2(((RectTransform)chatBackground.transform).sizeDelta.x,
                            (
                                ((RectTransform)chats[0].transform).anchoredPosition.y) -
                                ((((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y -
                                ((RectTransform)chats[chats.Count - 1].transform).sizeDelta.y - 100)
                            )
                        );
                }
                else
                {
                    ((RectTransform)chatBackground.transform).sizeDelta = new Vector2(((RectTransform)chatBackground.transform).sizeDelta.x, 200f);
                }
                lastScriptPos = j;
                // 이미 갱신됬다면 무시(select에서)
                if (lastGab == -9999)
                {
                    if (chats.Count <= 0) lastGab = 0;
                    else lastGab = (((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y) - nextPoint;
                }

                // 갱신이거나, 자동 스크롤중이라면 화면을 아래로 내려줌
                if (isRefresh || (!isRefresh && onAutoScroll))
                {
                    TurnOnAutoScroll();
                    ScrollDown();
                }
                // 갱신이 아닌데, 자동 스크롤이아니라면 아래에 표시
                if(!isRefresh && !onAutoScroll)
                {
                    string result = ToolClass.ParseToSmallScript(gm.mainData.chatLogs[i].beforeChats[gm.mainData.chatLogs[i].beforeChats.Count - 1].chat);
                    if(result!="") SetLastChatText(result);
                }
                ChangeScreen(EMSGScreen.CHAT);
                return;
            }
        }
    }
    void TurnOnAutoScroll(bool ignoreTouch = false)
    {
        if ((Input.touchCount > 0 || bMLDown) && !ignoreTouch) return;
        onAutoScroll = true;
        SetLastChatText("");
    }
    public void SetLastChatText(string text)
    {
        if (text == "") lastChatPanel.SetActive(false);
        else
        {
            var str = ToolClass.MakeStringPretty(text, 17, true, 1);
            if (str.Length >= 16) str += "...";
            lastChatPanel.SetActive(true);
            lastChatPanel.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = str;
        }
    }

    public void ScrollDown(bool immediately = false)
    {
        if (!immediately)
        {
            if (Input.touchCount > 0 || bMLDown) return;
            if (chats.Count <= 0) return;
        }
        TurnOnAutoScroll();
        chat_panel.parent.Find("Scrollbar").GetComponent<Scrollbar>().value = 1;
        chat_panel.parent.Find("Scrollbar").GetComponent<Scrollbar>().value = 0;
    }
    float saved_under_sizeDelta = -171717;
    void SetUnderScreen(bool isUp)
    {
        const float selectPanelSize = 580f;
        if(saved_under_sizeDelta == -171717)
        {
            saved_under_sizeDelta = ((RectTransform)chat_panel.transform.parent).sizeDelta.y;
        }
        if (isUp)
        {
            Vector2 beforePos = ((RectTransform)lowerBar.transform).anchoredPosition;
            beforePos.y = selectPanelSize;
            ((RectTransform)lowerBar.transform).anchoredPosition = beforePos;
            beforePos = ((RectTransform)chatBackground.transform).offsetMin;
            beforePos.y += selectPanelSize;
            ((RectTransform)chatBackground.transform).offsetMin = beforePos;
            ((RectTransform)chat_panel.transform.parent).sizeDelta = new Vector2(((RectTransform)chat_panel.transform.parent).sizeDelta.x, saved_under_sizeDelta - selectPanelSize);
        }
        else
        {
            Vector2 beforePos = ((RectTransform)lowerBar.transform).anchoredPosition;
            beforePos.y = 0f;
            ((RectTransform)lowerBar.transform).anchoredPosition = beforePos;
            beforePos = ((RectTransform)chatBackground.transform).offsetMin;
            beforePos.y -= selectPanelSize;
            ((RectTransform)chatBackground.transform).offsetMin = beforePos;
            ((RectTransform)chat_panel.transform.parent).sizeDelta = new Vector2(((RectTransform)chat_panel.transform.parent).sizeDelta.x, saved_under_sizeDelta);
        }
    }
    // 게임 결과별 이동하긔
    public void ActiveGameSelect(ScriptType type)
    {
        SetUnderScreen(false);
        ScrollDown(true);

        // 모든 select 버튼을 삭제
        for (int k = 0; k < selectPanel.transform.childCount; ++k)
            Destroy(selectPanel.transform.GetChild(k).gameObject);

        var character = ToolClass.GetCharacter(clickedChatLog.charCode);
        var currentScript = ToolClass.GetScript(character.currentScriptCode);
        for (int k = character.currentScriptPosition + 1; k < currentScript.scripts.Count; ++k)
        {
            ScriptParsedData scriptData = ToolClass.ParseScript(currentScript.scripts[k]);
            if (scriptData.type == type)
            {
                character.currentScriptPosition = k + 1;
                character.currentScriptProcess = 0f;
                break;
            }
        }
    }
    public void SelectComplete(string selectName)
    {
        if(selectName == ScriptType.GAME_START.ToString())
        {
            GameObject game = MiniGameMain.GetInstance().gameObject;

            Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
            game.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
            game.GetComponent<FadeOutSystem>().FadeIn();
            MiniGameMain.GetInstance().Reload();
            gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
            gameObject.GetComponent<FadeOutSystem>().FadeOut();
            MiniGameMain.GetInstance().ChangeScreen(0);
        }
        else if (selectName == ScriptType.GAME_IGNORE.ToString())
        {
            SetUnderScreen(false);
            ScrollDown(true);

            // 모든 select 버튼을 삭제
            for (int k = 0; k < selectPanel.transform.childCount; ++k)
                Destroy(selectPanel.transform.GetChild(k).gameObject);

            var character = ToolClass.GetCharacter(clickedChatLog.charCode);
            var currentScript = ToolClass.GetScript(character.currentScriptCode);
            for (int k = character.currentScriptPosition + 1; k < currentScript.scripts.Count; ++k)
            {
                ScriptParsedData scriptData = ToolClass.ParseScript(currentScript.scripts[k]);
                if (scriptData.type == ScriptType.GAME_IGNORE)
                {
                    character.currentScriptPosition = k + 1;
                    character.currentScriptProcess = 0f;
                    break;
                }
            }
        }
        else
        {
            SetUnderScreen(false);
            ScrollDown(true);

            // 모든 select 버튼을 삭제
            for (int k = 0; k < selectPanel.transform.childCount; ++k)
                Destroy(selectPanel.transform.GetChild(k).gameObject);

            // 해당 리스트로 찾아간다.
            MyCharacter character = ToolClass.GetCharacter(chattingTargetCharCode);
            ChatScript script = ToolClass.GetScript(character.currentScriptCode);
            for (int i = character.currentScriptPosition + 1; i < script.scripts.Count; ++i)
            {
                ScriptParsedData scriptData = ToolClass.ParseScript(script.scripts[i]);
                if (scriptData.type == ScriptType.SELECT_LIST && scriptData.val == selectName)
                {
                    character.currentScriptPosition = i + 1;
                    character.currentScriptProcess = 0f;
                    Resort();
                    break;
                }
            }
        }
    }
    public void Resort()
    {
        // 친구목록 갱신
        RefreshFriendList();

        // 챗 리스트 갱신
        RefreshChatList();

        // 채팅 상황이면 갱신
        if(currentScreen == EMSGScreen.CHAT)
        {
            OpenChat(clickedChatLog);
        }
    }
    public void RefreshHomeAlram()
    {
        // 홈화면 알람 갯수 설정
        MainData mainData = GameMain.GetInstance().mainData;
        int count = 0;
        foreach (ChatLog chatLog in mainData.chatLogs)
        {
            count += chatLog.alram;
        }
        am.SetAlram(count);
    }
    private static MessangerMain instance;
    public static MessangerMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("MessengerMain").GetComponent<MessangerMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }


    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                Back();
                backButtonTime = 0.0f;
            }
        }

    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
    bool bMLDown;
    public RectTransform rt_mm, rt_sm;
    bool beforeIsTouched;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) bMLDown = true;
        else if (Input.GetMouseButtonUp(0)) bMLDown = false;
        const int SCREEN_HEIGHT = 2000;
        const int SCREEN_UPPER_OFFSET = 500;
        if (currentScreen == EMSGScreen.CHAT)
        {
            RectTransform rt_ChatPanel = (RectTransform)(chatBackground.transform);
            for (int k = 0; k < chats.Count; ++k)
            {
                RectTransform rt = (RectTransform)(chats[k].transform);
                if(rt_ChatPanel.anchoredPosition.y - SCREEN_UPPER_OFFSET <= -rt.anchoredPosition.y && -rt.anchoredPosition.y <= rt_ChatPanel.anchoredPosition.y + SCREEN_HEIGHT) chats[k].SetActive(true);
                else chats[k].SetActive(false);
            }
        }

        if (changedDelay > 0f)
        {
            changedDelay -= Time.deltaTime;
        }
        // 자동 스와이핑
        if (!bMLDown && Input.touchCount == 0)
        {
            if (beforeIsTouched)
            {
                if (changedDelay <= 0f)
                {
                    switch (currentScreen)
                    {
                        case EMSGScreen.FRIENDLIST:
                            if (rt_sm.anchoredPosition.x <= 300) ChangeScreen(EMSGScreen.CHATLIST);
                            break;
                        case EMSGScreen.CHATLIST:
                            if (rt_sm.anchoredPosition.x >= -300) ChangeScreen(EMSGScreen.FRIENDLIST);
                            else if (rt_mm.anchoredPosition.x <= 300) ChangeScreen(EMSGScreen.CHAT);
                            break;
                        case EMSGScreen.CHAT:
                            if (rt_mm.anchoredPosition.x >= -300) ChangeScreen(EMSGScreen.CHATLIST);
                            break;
                    }
                }
                beforeIsTouched = false;
            }

            switch (currentScreen)
            {
                case EMSGScreen.FRIENDLIST:
                    rt_mm.anchoredPosition = Vector2.Lerp(new Vector2(600, 0), rt_mm.anchoredPosition, 0.6f);
                    rt_sm.anchoredPosition = Vector2.Lerp(new Vector2(600, 0), rt_sm.anchoredPosition, 0.6f);
                    break;
                case EMSGScreen.CHATLIST:
                    rt_mm.anchoredPosition = Vector2.Lerp(new Vector2(600, 0), rt_mm.anchoredPosition, 0.6f);
                    rt_sm.anchoredPosition = Vector2.Lerp(new Vector2(-600, 0), rt_sm.anchoredPosition, 0.6f);
                    break;
                case EMSGScreen.CHAT:
                    rt_mm.anchoredPosition = Vector2.Lerp(new Vector2(-600, 0), rt_mm.anchoredPosition, 0.6f);
                    rt_sm.anchoredPosition = Vector2.Lerp(new Vector2(-600, 0), rt_sm.anchoredPosition, 0.6f);
                    break;
            }
        }
        // 수동 스와이핑
        else
        {
            beforeIsTouched = true;
        }
    }
}