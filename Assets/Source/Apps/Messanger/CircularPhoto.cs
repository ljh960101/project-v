﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularPhoto : MonoBehaviour {
    string photoCode;
    public void Init(string photoCode)
    {
        this.photoCode = photoCode;
        transform.Find("PhotoImage").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + photoCode);
        gameObject.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
        gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(Click);
    }
    void Click()
    {
        Modal.GetInstance().CreateImageModal(photoCode + "");
    }
}
