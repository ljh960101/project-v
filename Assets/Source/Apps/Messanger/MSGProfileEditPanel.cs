﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSGProfileEditPanel : MonoBehaviour {
    UnityEngine.UI.InputField nameField;
    FadeOutSystem fo;
    public void Init()
    {
        nameField = transform.Find("ProfileEditView").Find("Name").GetComponent<UnityEngine.UI.InputField>();
        fo = GetComponent<FadeOutSystem>();
        fo.Init(4f, false, 1f, false);
        Close();
    }
    public void Open()
    {
        nameField.text = GameMain.GetInstance().mainData.msgName;
        fo.FadeIn();
    }
    public void Close()
    {
        fo.FadeOut();
    }
    public void Confirm()
    {
        bool chk = false;
        if (nameField.text.Length>=2)
        {
            foreach(char character in nameField.text)
            {
                if(character != ' ')
                {
                    chk = true;
                    break;
                }
            }
        }
        if (!chk)
        {
            Modal.GetInstance().CreateNewOkModal("최소 2자이상의\n이름으로 정해주세요.");
        }
        else
        {
            GameMain.GetInstance().mainData.msgName = nameField.text;
            MessangerMain.GetInstance().Resort();
            Close();
        }
    }
}
