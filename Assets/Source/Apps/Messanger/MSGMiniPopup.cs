﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyTool;
using MyDesign;

public class MSGMiniPopup : MonoBehaviour {
    int currentCharacterCode;
    UnityEngine.UI.Text name, status;
    CircularPhoto profile;
    FadeOutSystem fo;
    public void Open(int charCode)
    {
        fo.FadeIn();
        MyCharacter character = ToolClass.GetCharacter(charCode);
        profile.Init(ToolClass.GetCharacter(charCode).curMsgPic);
        name.text = character.name;
        status.text = character.statusMsg;
        currentCharacterCode = charCode;
    }
    public void OpenChat()
    {
        fo.FadeOut();
        MessangerMain.GetInstance().ScrollDown();
        MessangerMain.GetInstance().OpenChat(currentCharacterCode);
    }
    public void Cansle()
    {
        fo.FadeOut();
    }
	public void Init () {
        profile = transform.Find("ProfileMiniView").Find("Image").Find("CircularPhoto").GetComponent<CircularPhoto>();
        name = transform.Find("ProfileMiniView").Find("Name").GetComponent<UnityEngine.UI.Text>();
        status = transform.Find("ProfileMiniView").Find("Msg").GetComponent<UnityEngine.UI.Text>();
        fo = GetComponent<FadeOutSystem>();
        fo.Init(4f, false, 1f, false);
        Cansle();
    }
}
