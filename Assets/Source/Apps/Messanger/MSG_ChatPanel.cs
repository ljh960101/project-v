﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MyDesign;
using MyTool;

public class MSG_ChatPanel : EventTrigger
{
    MessangerMain mcm;
    UnityEngine.UI.Text ui_name, ui_chat, ui_time;
    public int charCode;
    string photoCode;
    System.DateTime time;
    public ChatLog cl;
    bool onDrag;
    CircularPhoto cp;

    public override void OnDrag(PointerEventData data)
    {
        mcm.targetToChatList.transform.parent.GetComponent<ScrollRect>().OnDrag(data);
    }
    public override void OnBeginDrag(PointerEventData data)
    {
        onDrag = true;
        mcm.targetToChatList.transform.parent.GetComponent<ScrollRect>().OnBeginDrag(data);
    }
    public override void OnScroll(PointerEventData data)
    {
        mcm.targetToChatList.transform.parent.GetComponent<ScrollRect>().OnScroll(data);
    }
    public override void OnEndDrag(PointerEventData data)
    {
        onDrag = false;
        mcm.targetToChatList.transform.parent.GetComponent<ScrollRect>().OnEndDrag(data);
    }
    public override void OnPointerClick(PointerEventData data)
    {
        if (!onDrag)
        {
            mcm.OpenChat(this);
            mcm.ScrollDown();
        }
    }
    public void Init(string name, string chat, int charCode, string photoCode, System.DateTime time, int alram, ChatLog cl)
    {
        if (!ui_chat)
        {
            ui_name = transform.Find("Photo").Find("Name").GetComponent<UnityEngine.UI.Text>();
            ui_chat = transform.Find("Photo").Find("Chat").GetComponent<UnityEngine.UI.Text>();
            ui_time = transform.Find("AlramImage").Find("Time").GetComponent<UnityEngine.UI.Text>();
            cp = transform.Find("CircularPhoto").GetComponent<CircularPhoto>();
            mcm = MessangerMain.GetInstance();
        }
        SetPanel(name, chat, charCode, photoCode, time);
        if(alram <= 0)
        {
            transform.Find("AlramImage").gameObject.SetActive(false);
        }
        else
        {
            transform.Find("AlramImage").Find("Text").gameObject.GetComponent<UnityEngine.UI.Text>().text = alram + "";
        }
        this.cl = cl;
    }

    public void SetPanel(string name, string chat, int charCode, string photoCode, System.DateTime time)
    {
        ui_name.text = name;
        ui_chat.text = ToolClass.MakeStringPretty(chat, 15, false, 2);
        ui_time.text = time.ToString("HH:MM");
        cp.Init(photoCode);

        this.time = time;
        this.charCode = charCode;
        this.photoCode = photoCode;
    }
}