﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MSGSelector : MonoBehaviour {
    int selectCode;
    UnityEngine.UI.Text text;
    MessangerMain mm;

    public void Init(string text, int code)
    {
        mm = MessangerMain.GetInstance();
        this.text = transform.Find("Text").GetComponent<UnityEngine.UI.Text>();
        this.text.text = text;
        selectCode = code;
    }
}
