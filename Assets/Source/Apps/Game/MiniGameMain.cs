﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class MiniGameMain : MonoBehaviour {
    public Transform Tap_Main, Tap_Ranking, Tap_CharacterSelect, Tap_Game, Tap_Pause, Tap_End, background, Tap_RankingList, Tap_GameRankingList, Tap_CharacterList;
    public GameObject characterPrefab, patternA, patternB, patternC, rankPanel, charSelectPrefab;
    public GameObject[] inGameBackgrounds;
    public AudioClip gameOver, getScore;
    GMainCharacter myCharacter;
    [HideInInspector] public List<GameObject> dangers;
    float level, gameTimer;
    [HideInInspector] public int currentScore;

    [HideInInspector] public ScreenState currentState;
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            if (currentState == ScreenState.Main) ChangeScreen(ScreenState.Pause);
        }
    }
    List<GameObject> characterList;
    public void RefreshCharacterList()
    {
        if (characterList == null) characterList = new List<GameObject>();
        foreach (var character in characterList) Destroy(character.gameObject);
        characterList.Clear();

        MiniGameData mgd = GameMain.GetInstance().mainData.miniGameData;
        Tap_CharacterList.parent.Find("CurrentCharacter").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Sprite/Apps/MiniGame/" + GameMain.GetInstance().mainData.miniGameData.currentCharacterCode);
        foreach (var character in mgd.characters)
        {
            GameObject newObj = Instantiate(charSelectPrefab, Tap_CharacterList);
            newObj.name = character.miniGameCharacterCode + "";
            newObj.transform.Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Sprite/Apps/MiniGame/" + character.miniGameCharacterCode);
           
            if (character.onUnlocked)
            {
                newObj.transform.Find("Image").GetComponent<UnityEngine.UI.Image>().color = new Color(1f, 1f, 1f);
                newObj.transform.Find("Image").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate {
                    mgd.currentCharacterCode = character.miniGameCharacterCode;
                    Tap_CharacterList.parent.Find("CurrentCharacter").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Sprite/Apps/MiniGame/" + GameMain.GetInstance().mainData.miniGameData.currentCharacterCode);
                });
            }
            else
            {
                newObj.transform.Find("Image").GetComponent<UnityEngine.UI.Image>().color = new Color(0f, 0f, 0f);
                newObj.transform.Find("Image").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate {
                    MyCharacter characterObject = ToolClass.GetCharacter(character.charCode);
                    if(characterObject == null) Modal.GetInstance().CreateNewOkModal("해당 캐릭터가 없습니다.");
                    else Modal.GetInstance().CreateNewOkModal("<b><size=80>조건</size></b>\n\n" + characterObject.name + "<color=red>[친한 친구]</color> 달성.");
                });
            }
            characterList.Add(newObj);
            character.onAlram = false;
        }

        GetComponent<AppMain>().SetAlram(0);
    }
    public Sprite secondCrown, firstCrown, thirdCrown;
    public void Close()
    {
        if (currentState == ScreenState.InGame) ChangeScreen(ScreenState.Pause);
        if (myCharacter)
        {
            myCharacter.GetComponent<Rigidbody2D>().simulated = false;
        }
        GetComponent<AppMain>().Close();
        if(background) background.GetComponent<FadeOutSystem>().FadeOut();
    }
    List<GameObject> rankingList;
    public void RefreshRanking()
    {
        if (rankingList == null) rankingList = new List<GameObject>();
        foreach (var rankingPanel in rankingList) Destroy(rankingPanel.gameObject);
        rankingList.Clear();

        var miniGameData = GameMain.GetInstance().mainData.miniGameData;
        miniGameData.scores.Sort(delegate (MiniGameScore A, MiniGameScore B)
            {
                if (A.score < B.score) return 1;
                else if (A.score > B.score) return -1;
                else return 0;
            });

        // 랭킹 리스트 그리기
        Tap_GameRankingList.Find("MyRanking").Find("Name").GetComponent<UnityEngine.UI.Text>().text = GameMain.GetInstance().mainData.msgName;
        Tap_GameRankingList.Find("MyRanking").Find("Score").GetComponent<UnityEngine.UI.Text>().text = currentScore + "";
        Tap_GameRankingList.parent.parent.Find("Score").GetComponent<UnityEngine.UI.Text>().text = currentScore + "";

        int ranking = 0, lastScore = int.MaxValue;
        foreach (var score in miniGameData.scores)
        {
            for (int i = 0; i <= 1; ++i) // 0 일반 1 게임 랭킹
            {
                if (lastScore > score.score)
                {
                    ++ranking;
                    lastScore = score.score;
                }

                GameObject newRankingPanel;
                if(i==0) newRankingPanel = Instantiate(rankPanel, Tap_RankingList);
                else newRankingPanel = Instantiate(rankPanel, Tap_GameRankingList);
                // 주인공?
                if (score.charCode == 0)
                {
                    newRankingPanel.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = GameMain.GetInstance().mainData.msgName;
                }
                else
                {
                    newRankingPanel.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(score.charCode).name;
                    newRankingPanel.transform.Find("CircularPhoto").GetComponent<CircularPhoto>().Init(ToolClass.GetCharacter(score.charCode).curMsgPic);
                }
                newRankingPanel.transform.Find("Score").GetComponent<UnityEngine.UI.Text>().text = score.score + "";
                newRankingPanel.transform.transform.Find("RankingImage").Find("Ranking").GetComponent<UnityEngine.UI.Text>().text = ranking + "";
                if (ranking == 1) newRankingPanel.transform.Find("RankingImage").GetComponent<UnityEngine.UI.Image>().sprite = firstCrown;
                else if (ranking == 2) newRankingPanel.transform.Find("RankingImage").GetComponent<UnityEngine.UI.Image>().sprite = secondCrown;
                else if (ranking == 3) newRankingPanel.transform.Find("RankingImage").GetComponent<UnityEngine.UI.Image>().sprite = thirdCrown;
                else newRankingPanel.transform.Find("RankingImage").GetComponent<UnityEngine.UI.Image>().color = new Color(0,0,0,0.001f);
                rankingList.Add(newRankingPanel);
            }
        }
    }
    public void Reload()
    {
        if (background) background.GetComponent<FadeOutSystem>().FadeIn();
        RefreshRanking();
    }
    public void MakeAlram()
    {
        if(!(GameMain.GetInstance().currentApp == gameObject && currentState == ScreenState.CharacterSelect))
        {
            ToolClass.MakeNotify(AppType.Game, "새로운 캐릭터가 해금되었습니다!");
            int count = 0;
            var mgd = GameMain.GetInstance().mainData.miniGameData;
            foreach (var character in mgd.characters)
            {
                if (character.onAlram) ++count;
            }
            GetComponent<AppMain>().SetAlram(count);
        }
    }
    public void Init()
    {
        dangers = new List<GameObject>();
        currentScore = 0;

        Tap_Main.GetComponent<FadeOutSystem>().Init(2f, false, 1f, false, false, FadeOutSystem.ChangeType.MoveFromLeft);
        Tap_Ranking.GetComponent<FadeOutSystem>().Init(2f, false, 1f, false, false, FadeOutSystem.ChangeType.MoveFromRight);
        Tap_CharacterSelect.GetComponent<FadeOutSystem>().Init(2f, false, 1f, false, false, FadeOutSystem.ChangeType.MoveFromRight);
        Tap_Game.GetComponent<FadeOutSystem>().Init(2f, false, 1f, false, false, FadeOutSystem.ChangeType.MoveFromRight);
        Tap_Pause.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, false, FadeOutSystem.ChangeType.MoveFromDown);
        Tap_End.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, false, FadeOutSystem.ChangeType.MoveFromDown);
        Tap_End.Find("CompetitionAlram").GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, false);
        if (background) background.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, false);
        foreach (var back in inGameBackgrounds)
        {
            back.GetComponent<MiniGameBackground>().Init();
        }

        Tap_Main.gameObject.SetActive(true);
        Tap_Ranking.gameObject.SetActive(false);
        Tap_CharacterSelect.gameObject.SetActive(false);
        Tap_Game.gameObject.SetActive(false);
        Tap_Pause.gameObject.SetActive(false);
        Tap_End.gameObject.SetActive(false);
        if(background) background.gameObject.SetActive(false);

        currentState = ScreenState.Main;

        inGameBackgrounds[0].GetComponent<MiniGameBackground>().SetSpeed(1f);
        inGameBackgrounds[1].GetComponent<MiniGameBackground>().SetSpeed(1f);
        inGameBackgrounds[2].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
        inGameBackgrounds[3].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
        inGameBackgrounds[4].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
        inGameBackgrounds[5].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
        inGameBackgrounds[6].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
    }
    public void ChangeScreen(int val)
    {
        ChangeScreen((ScreenState)val);
    }
    public void AddScore(int score)
    {
        if(score>=5) ToolClass.PlaySound(getScore);
        currentScore += score;
        Tap_Game.Find("Score").GetComponent<UnityEngine.UI.Text>().text = currentScore + "";
    }
    public void ChangeScreen(ScreenState target)
    {
        switch (target)
        {
            case ScreenState.Main:
                if (Tap_CharacterSelect.gameObject.activeSelf) Tap_CharacterSelect.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_Pause.gameObject.activeSelf) Tap_Pause.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_End.gameObject.activeSelf) Tap_End.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_Ranking.gameObject.activeSelf) Tap_Ranking.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_Game.gameObject.activeSelf) Tap_Game.GetComponent<FadeOutSystem>().FadeOut();
                if (!Tap_Main.gameObject.activeSelf) Tap_Main.GetComponent<FadeOutSystem>().FadeIn();
                foreach (var danger in dangers) Destroy(danger);
                dangers.Clear();
                if (myCharacter)
                {
                    Destroy(myCharacter.gameObject);
                    myCharacter = null;
                }
                inGameBackgrounds[0].GetComponent<MiniGameBackground>().SetSpeed(1f);
                inGameBackgrounds[1].GetComponent<MiniGameBackground>().SetSpeed(1f);
                inGameBackgrounds[2].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
                inGameBackgrounds[3].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
                inGameBackgrounds[4].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
                inGameBackgrounds[5].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
                inGameBackgrounds[6].GetComponent<MiniGameBackground>().SetSpeed(0.1f);
                break;
            case ScreenState.CharacterSelect:
                if (Tap_Main.gameObject.activeSelf) Tap_Main.GetComponent<FadeOutSystem>().FadeOut();
                if (!Tap_CharacterSelect.gameObject.activeSelf) Tap_CharacterSelect.GetComponent<FadeOutSystem>().FadeIn();
                RefreshCharacterList();
                break;
            case ScreenState.Ranking:
                if (Tap_Main.gameObject.activeSelf) Tap_Main.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_Game.gameObject.activeSelf) Tap_Game.GetComponent<FadeOutSystem>().FadeOut();
                if (!Tap_Ranking.gameObject.activeSelf) Tap_Ranking.GetComponent<FadeOutSystem>().FadeIn();
                RefreshRanking();
                break;
            case ScreenState.InGame:
                if (Tap_Main.gameObject.activeSelf) Tap_Main.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_Pause.gameObject.activeSelf) Tap_Pause.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_End.gameObject.activeSelf) Tap_End.GetComponent<FadeOutSystem>().FadeOut();

                // 게임 시작 처리
                if (currentState == ScreenState.Die || currentState == ScreenState.Main)
                {
                    currentScore = 0;
                    Tap_Game.Find("Score").GetComponent<UnityEngine.UI.Text>().text = currentScore + "";

                    foreach (var danger in dangers)  Destroy(danger);
                    dangers.Clear();
                    if (myCharacter) Destroy(myCharacter.gameObject);

                    myCharacter = Instantiate(characterPrefab, Tap_Game).GetComponent<GMainCharacter>();
                    myCharacter.dieCallback = delegate { ChangeScreen(ScreenState.Die); };
                    myCharacter.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprite/Apps/MiniGame/" + GameMain.GetInstance().mainData.miniGameData.currentCharacterCode);
                    //Instantiate(Resources.Load<GameObject>("Prefab/Apps/Game/Particle/" + GameMain.GetInstance().mainData.miniGameData.currentCharacterCode), myCharacter.transform);
                    gameTimer = 0f;
                    level = 3f;

                    inGameBackgrounds[0].GetComponent<MiniGameBackground>().SetSpeed(level);
                    inGameBackgrounds[1].GetComponent<MiniGameBackground>().SetSpeed(level);
                    inGameBackgrounds[2].GetComponent<MiniGameBackground>().SetSpeed(level/10f);
                    inGameBackgrounds[3].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[4].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[5].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[6].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                }
                // 중지 해제
                else if(currentState == ScreenState.Pause)
                {
                    if (myCharacter)
                    {
                        myCharacter.Pause(false);
                        myCharacter.GetComponent<Rigidbody2D>().simulated = true;
                    }
                    foreach (var danger in dangers)
                    {
                        danger.GetComponent<GDanger>().SetSpeed(level);
                    }
                    inGameBackgrounds[0].GetComponent<MiniGameBackground>().SetSpeed(level);
                    inGameBackgrounds[1].GetComponent<MiniGameBackground>().SetSpeed(level);
                    inGameBackgrounds[2].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[3].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[4].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[5].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                    inGameBackgrounds[6].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
                }
                if (!Tap_Game.gameObject.activeSelf) Tap_Game.GetComponent<FadeOutSystem>().FadeIn();
                break;
            case ScreenState.Pause:
                if (currentState == ScreenState.InGame)
                {
                    if (myCharacter) myCharacter.Pause(true);
                    foreach (var danger in dangers)
                    {
                        danger.GetComponent<GDanger>().SetSpeed(0f);
                    }
                    foreach (var back in inGameBackgrounds)
                    {
                        back.GetComponent<MiniGameBackground>().SetSpeed(0f);
                    }
                    if (!Tap_Pause.gameObject.activeSelf) Tap_Pause.GetComponent<FadeOutSystem>().FadeIn();
                }
                else return;
                break;
            case ScreenState.Die:
                if(!Tap_End.gameObject.activeSelf) Tap_End.GetComponent<FadeOutSystem>().FadeIn();
                ToolClass.PlaySound(gameOver);
                ToolClass.MakeVibration();
                Tap_End.Find("CompetitionAlram").gameObject.SetActive(false);
                MainData mgd = GameMain.GetInstance().mainData;
                MiniGameData miniGameData = mgd.miniGameData;
                // 캐릭터의 기록을 찾아서 낮으면 상승시켜줌
                foreach(var score in miniGameData.scores)
                {
                    if(score.charCode == 0)
                    {
                        if(currentScore>score.score) score.score = currentScore;
                        break;
                    }
                }
                GameMain.GetInstance().mainData.miniGameData.lastScore = currentScore;
                RefreshRanking();

                // 경쟁중인지 확인하고 경쟁중이라면 COMPETITION 버튼 활성화
                bool isOn = false;
                foreach(var character in mgd.characters)
                {
                    if (character.onGameCompetition)
                    {
                        isOn = true;
                        currentCompetitionCharCode = character.charCode;
                        break;
                    }
                }
                if (isOn)
                    Tap_End.Find("Panel").Find("BTN_COMPETITION").gameObject.SetActive(true);
                else
                    Tap_End.Find("Panel").Find("BTN_COMPETITION").gameObject.SetActive(false);

                break;
            default:
                throw new Exception("Wrong Screen");
        }
        currentState = target;
    }
    int currentCompetitionCharCode;
    public void CompetitionAlram()
    {
        Tap_End.Find("CompetitionAlram").Find("Text").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(currentCompetitionCharCode).name + "님께 내 점수\n<color=#700808>" + currentScore + "</color>점을 자랑할까요?";
        Tap_End.Find("CompetitionAlram").GetComponent<FadeOutSystem>().FadeIn();
    }
    public void CompetitionEnd()
    {
        Tap_End.Find("Panel").Find("BTN_COMPETITION").gameObject.SetActive(false);
        CompetitionAlramClose();
        int characterScore = ToolClass.GetMiniGameScore(currentCompetitionCharCode);
        ScriptType findNeededScriptType;
        if (currentScore < characterScore) findNeededScriptType = ScriptType.GAME_LOW;
        else if (currentScore > characterScore) findNeededScriptType = ScriptType.GAME_HIGH;
        else findNeededScriptType = ScriptType.GAME_SAME;

        GameObject msg = MessangerMain.GetInstance().gameObject;
        msg.GetComponent<MessangerMain>().ActiveGameSelect(findNeededScriptType);
        Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
        msg.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        msg.GetComponent<FadeOutSystem>().FadeIn();
        MessangerMain.GetInstance().Reload();
        gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        gameObject.GetComponent<FadeOutSystem>().FadeOut();
    }
    public void CompetitionAlramClose()
    {
        Tap_End.Find("CompetitionAlram").GetComponent<FadeOutSystem>().FadeOut();
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                switch (currentState)
                {
                    case ScreenState.CharacterSelect:
                        ChangeScreen(ScreenState.Main);
                        break;
                    case ScreenState.Die:
                        ChangeScreen(ScreenState.Main);
                        break;
                    case ScreenState.InGame:
                        ChangeScreen(ScreenState.Pause);
                        break;
                    case ScreenState.Main:
                        Close();
                        break;
                    case ScreenState.Pause:
                        ChangeScreen(ScreenState.InGame);
                        break;
                    case ScreenState.Ranking:
                        ChangeScreen(ScreenState.Main);
                        break;
                }
                backButtonTime = 0.0f;
            }
        }

    }
    void Update ()
    {
        if (currentState == ScreenState.InGame)
        {
            gameTimer -= Time.deltaTime;
            level += Time.deltaTime / 10f;
            if (level >= 6f) level = 7f;

            inGameBackgrounds[0].GetComponent<MiniGameBackground>().SetSpeed(level);
            inGameBackgrounds[1].GetComponent<MiniGameBackground>().SetSpeed(level);
            inGameBackgrounds[2].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
            inGameBackgrounds[3].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
            inGameBackgrounds[4].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
            inGameBackgrounds[5].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);
            inGameBackgrounds[6].GetComponent<MiniGameBackground>().SetSpeed(level / 10f);

            if (gameTimer < 0f)
            {
                const float minMax = 1.7f;
                gameTimer += 1.5f - level * 0.1f;
                int randNumber = UnityEngine.Random.Range(0, 5);
                if (randNumber == 0)
                {
                    GameObject newObj = Instantiate(patternA, Tap_Game);
                    newObj.transform.position = newObj.transform.position + new Vector3(4.0f, UnityEngine.Random.Range(-minMax, minMax * 2.5f), 0f);
                    //newObj.transform.localScale = new Vector2(20f, UnityEngine.Random.Range(20, 45));
                    newObj.GetComponent<GDanger>().SetSpeed(level);
                    dangers.Add(newObj);
                }
                else if (randNumber == 1)
                {
                    GameObject newObj = Instantiate(patternB, Tap_Game);
                    newObj.transform.position = newObj.transform.position + new Vector3(4.0f, UnityEngine.Random.Range(-minMax * 2.5f, minMax), 0f);
                    //newObj.transform.localScale = new Vector2(20f, UnityEngine.Random.Range(20, 45));
                    newObj.GetComponent<GDanger>().SetSpeed(level);
                    dangers.Add(newObj);
                }
                else if (randNumber == 2)
                {
                    {
                        GameObject newObj = Instantiate(patternA, Tap_Game);
                        newObj.transform.position = newObj.transform.position + new Vector3(4.0f, UnityEngine.Random.Range(-minMax, minMax), 0f);
                        //newObj.transform.localScale = new Vector2(20f, UnityEngine.Random.Range(20, 35));
                        newObj.GetComponent<GDanger>().SetSpeed(level);
                        dangers.Add(newObj);
                    }
                    {
                        GameObject newObj = Instantiate(patternB, Tap_Game);
                        newObj.transform.position = newObj.transform.position + new Vector3(4.0f, UnityEngine.Random.Range(-minMax, minMax), 0f);
                        //newObj.transform.localScale = new Vector2(20f, UnityEngine.Random.Range(20, 35));
                        newObj.GetComponent<GDanger>().SetSpeed(level);
                        dangers.Add(newObj);
                    }
                }
                else
                {
                    GameObject newObj = Instantiate(patternC, Tap_Game);
                    newObj.transform.position = newObj.transform.position + new Vector3(4.0f, UnityEngine.Random.Range(-4f, 4f), 0f);
                    newObj.GetComponent<GDanger>().SetSpeed(level);
                    dangers.Add(newObj);
                }
            }
        }
        BackButtonProc();
    }
    private static MiniGameMain instance;
    public static MiniGameMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("GameMain").GetComponent<MiniGameMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
}
public enum ScreenState
{
    Main = 0,
    Ranking,
    CharacterSelect,
    InGame,
    Pause,
    Die
}