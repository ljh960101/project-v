﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMainCharacter : MonoBehaviour {
    const float speed_up = 8f;
    const float speed_down = 8f;
    public bool onDie;
    bool pause = false;
    Rigidbody2D rb;
    public delegate void MyCallback();
    public MyCallback dieCallback;
    public MyCallback scoreCallback;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        onDie = false;
    }
    public void Pause(bool pause)
    {
        this.pause = pause;
        ShowParticle(!pause);
    }
    bool onMouseDown = false;
    float speed = 0f;
    float angle = 0f;
    void Update ()
    {
        if (!onDie && !pause)
        {
            if (Input.GetMouseButtonDown(0)) onMouseDown = true;
            if (Input.GetMouseButtonUp(0)) onMouseDown = false;

            // Flying
            if (Input.touchCount > 0 || onMouseDown)
            {
                if (speed <= 0f)
                {
                    speed = Mathf.Clamp(speed + Time.deltaTime * 30f, -speed_down, speed_up);
                }
                speed = Mathf.Clamp(speed + Time.deltaTime * 20f, -speed_down, speed_up);
                angle = Mathf.Clamp(speed * 4f, -40f, 40f);
            }
            // Falling Down
            else
            {
                if (speed >= 0f)
                {
                    speed = Mathf.Clamp(speed - Time.deltaTime * 30f, -speed_down, speed_up);
                }
                speed = Mathf.Clamp(speed - Time.deltaTime * 20f, -speed_down, speed_up);
                angle = Mathf.Clamp(speed * 4f, -40f, 40f);
            }
            transform.Translate(new Vector2(0, Time.deltaTime * speed), Space.World);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angle);
        }
    }
    void ShowParticle(bool on)
    {
        //transform.GetChild(0).gameObject.SetActive(on);
    }
    void Die()
    {
        onDie = true;
        rb.gravityScale = 1f;
        if(dieCallback != null) dieCallback();
        foreach(var danger in MiniGameMain.GetInstance().dangers)
        {
            danger.GetComponent<GDanger>().SetSpeed(0f);
        }
        foreach (var back in MiniGameMain.GetInstance().inGameBackgrounds)
        {
            back.GetComponent<MiniGameBackground>().SetSpeed(0f);
        }
        //Destroy(transform.GetChild(0).gameObject);
    }
    void GetScore(int score = 1)
    {
        MiniGameMain.GetInstance().AddScore(score);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (onDie) return;
        if (collision.transform.tag == "Danger")
        {
            Die();
        }
        else if (collision.transform.tag == "Score")
        {
            MiniGameMain.GetInstance().dangers.Remove(collision.gameObject);
            if (collision.gameObject.GetComponent<SpriteRenderer>().enabled)
            {
                collision.gameObject.GetComponent<Coin>().MoveTo();
            }
            else
            {
                GetScore();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (onDie) return;
        if (collision.transform.tag == "Danger")
        {
            Die();
        }
        else if (collision.transform.tag == "Score")
        {
            MiniGameMain.GetInstance().dangers.Remove(collision.gameObject);
            if (collision.gameObject.GetComponent<SpriteRenderer>().enabled)
            {
                collision.gameObject.GetComponent<Coin>().MoveTo();
            }
            else
            {
                GetScore();
            }
        }
    }
}
