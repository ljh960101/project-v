﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject particle;
    bool onMove = false;
    Transform moveTarget;
    Vector2 beforeLocation, beforeScale;
    float location;
    MiniGameMain mgm;
    public void MoveTo(Transform _target = null)
    {
        if (_target == null) moveTarget = MiniGameMain.GetInstance().Tap_Game.Find("Score").transform;
        else moveTarget = _target;

        gameObject.GetComponent<Collider2D>().enabled = false;
        onMove = true;
        location = 0.0f;
        beforeLocation = transform.position;
        beforeScale = transform.localScale;
    }
    private void Start()
    {
        mgm = MiniGameMain.GetInstance();
        beforePos = transform.position.y;
    }
    bool isUp = false;
    float upTimer = 0f, beforePos;
    void Update()
    {
        if (onMove && mgm.currentState == ScreenState.InGame)
        {
            location += Time.deltaTime * 3f;
            transform.position = Vector2.Lerp(beforeLocation, moveTarget.position, location);
            transform.localScale = Vector2.Lerp(beforeScale, Vector2.zero, location);
            transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z + Time.deltaTime * 1000f);
            if (location > 1.0f)
            {
                Instantiate(particle, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)));
                MiniGameMain.GetInstance().AddScore(10);
                Destroy(gameObject);
            }
        }
        else if (!onMove && mgm.currentState == ScreenState.InGame)
        {
            if (isUp)
            {
                transform.position = new Vector2(transform.position.x, beforePos + upTimer / 3);
                upTimer += Time.deltaTime / 2f;
                if (upTimer > 1.0f)
                {
                    isUp = !isUp;
                }
            }
            else
            {
                transform.position = new Vector2(transform.position.x, beforePos + upTimer / 3);
                upTimer -= Time.deltaTime / 2f;
                if (upTimer < 0f)
                {
                    isUp = !isUp;
                }
            }
        }
    }
}
