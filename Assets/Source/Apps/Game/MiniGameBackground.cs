﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameBackground : MonoBehaviour {
    float speed;
    RectTransform myRect;
    public void Init()
    {
        myRect = (RectTransform)transform;
        speed = 0f;
    }
    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }
    private void Update()
    {
        myRect.anchoredPosition = new Vector2(myRect.anchoredPosition.x - speed * Time.deltaTime * 100f, myRect.anchoredPosition.y);
        if(myRect.anchoredPosition.x <= -1540f)
            myRect.anchoredPosition = new Vector2(myRect.anchoredPosition.x + 4000f, myRect.anchoredPosition.y);
    }
}
