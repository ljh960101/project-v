﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDanger : MonoBehaviour {
    float speed = 3f;
    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }
	void Update () {
        transform.Translate(new Vector3(Time.deltaTime * -speed, 0f, 0f));
        if(transform.position.x <= -4)
        {
            MiniGameMain.GetInstance().dangers.Remove(gameObject);
            Destroy(gameObject);
        }
	}
}
