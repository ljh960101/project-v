﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class PhotoMain : MonoBehaviour {
    public enum ScreenType
    {
        ALBUM, INALBUM, PHOTO, NOT
    }
    MainData md;
    ScreenType currentScreen;
    int currentCharCode, currentPicPos;
    public void Init()
    {
        currentScreen = ScreenType.ALBUM;
        md = GameMain.GetInstance().mainData;
        Tap_Album.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        Tap_InAlbum.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        Tap_Photo.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        Tap_Album.gameObject.SetActive(true);
        Tap_InAlbum.gameObject.SetActive(false);
        Tap_Photo.gameObject.SetActive(false);
        albumLists = new List<GameObject>();
        ThumnailLists = new List<GameObject>();
        Reload();
    }
    public void Reload()
    {
        RefreshAlbum();
        if(currentScreen==ScreenType.INALBUM || currentScreen == ScreenType.PHOTO) RefreshInAlbum();
    }
    public GameObject AlbumPrefab, ThumnailPrefab, Tap_Album, Tap_InAlbum, Tap_Photo;
    public Transform AlbumList, ThumnailList;
    List<GameObject> albumLists, ThumnailLists;
    void RefreshAlbum()
    {
        if (albumLists == null) return;
        foreach (var obj in albumLists) Destroy(obj);
        albumLists.Clear();

        // 친구 추가된 캐릭터의 포토 리스트를 만듬
        foreach(var character in md.characters)
        {
            if (character.collectedPhotos!=null && character.collectedPhotos.Count >= 1)
            {
                string name = character.name;
                if (!character.bOnChat) name = "???";
                GameObject newList = Instantiate(AlbumPrefab, AlbumList);
                newList.transform.Find("LeftSide").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "<size=63><color=#48484A>" + name + "</color></size>\n" + character.collectedPhotos.Count;
                newList.transform.Find("LeftSide").Find("PhotoFrame").Find("RealPhoto").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + character.collectedPhotos[character.collectedPhotos.Count-1]);
                newList.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate
                {
                    OpenAlbum(character.charCode);
                });
                albumLists.Add(newList);
            }
        }
    }
    void RefreshInAlbum()
    {
        foreach (var obj in ThumnailLists) Destroy(obj);
        ThumnailLists.Clear();

        // 현재 캐릭터의 컬렉트 포토를 역순으로 꺼내서 그려줌
        MyCharacter character = ToolClass.GetCharacter(currentCharCode);
        for (int i = character.collectedPhotos.Count - 1; i >= 0; --i)
        {
            GameObject newList = Instantiate(ThumnailPrefab, ThumnailList);
            newList.transform.Find("RealImage").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + character.collectedPhotos[i]);
            newList.name = i + "";
            newList.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate
            {
                OpenPhoto(newList.name);
            });
            ThumnailLists.Add(newList);
        }
        inAlbumTitle.text = character.name;
    }
    public UnityEngine.UI.Text inAlbumTitle;
    void OpenAlbum(int charCode)
    {
        currentCharCode = charCode;
        RefreshInAlbum();
        ChangeScreen(ScreenType.INALBUM);
    }
    void OpenPhoto(string picPos)
    {
        currentPicPos = int.Parse(picPos);
        RefreshPhoto();
        Canvas.ForceUpdateCanvases();
        scrollbar.horizontalNormalizedPosition = 0.5f;
        Canvas.ForceUpdateCanvases();
        ChangeScreen(ScreenType.PHOTO);
    }
    public UnityEngine.UI.ScrollRect scrollbar;
    public void OnPhotoMoved()
    {
        if (currentScreen != ScreenType.PHOTO) return;
        MyCharacter character = ToolClass.GetCharacter(currentCharCode);
        Canvas.ForceUpdateCanvases();
        Vector2 val = scrollbar.normalizedPosition;
        if (val.x <= 0.05f && currentPicPos + 1 < character.collectedPhotos.Count)
        {
            ++currentPicPos;
            RefreshPhoto();
            scrollbar.horizontalNormalizedPosition = val.x + 0.5f;
        }
        if (val.x >= 0.95f && currentPicPos - 1 >= 0)
        {
            --currentPicPos;
            RefreshPhoto();
            scrollbar.horizontalNormalizedPosition = val.x - 0.5f;
        }
        Canvas.ForceUpdateCanvases();
    }
    void RefreshPhoto()
    {
        MyCharacter character = ToolClass.GetCharacter(currentCharCode);
        if (currentPicPos - 1 >= 0)
        {
            Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo3").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + character.collectedPhotos[currentPicPos - 1]);
            Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo3").GetComponent<UnityEngine.UI.Image>().enabled = true;
        }
        else Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo3").GetComponent<UnityEngine.UI.Image>().enabled = false;
        Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo2").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + character.collectedPhotos[currentPicPos]);
        if (currentPicPos + 1 < character.collectedPhotos.Count)
        {
            Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo1").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + character.collectedPhotos[currentPicPos + 1]);
            Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo1").GetComponent<UnityEngine.UI.Image>().enabled = true;
        }
        else Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo1").GetComponent<UnityEngine.UI.Image>().enabled = false;
        RectTransform photo_transform = (RectTransform)Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo2").transform;
        photo_transform.anchoredPosition = new Vector2(0.0f, 0.0f);
        photo_transform.sizeDelta = new Vector2(1080, 1500);
        zoom = 1f;
    }
    public void ChangeScreen(int type)
    {
        ChangeScreen((ScreenType)type);
    }
    public GameObject upperBar, downbar;
    public void HideUpperBar()
    {
        if (onZoom) return;
        upperBar.SetActive(!upperBar.activeSelf);
        downbar.SetActive(!downbar.activeSelf);
    }
    public void ChangeScreen(ScreenType type)
    {
        if (type == currentScreen) return;
        switch (type)
        {
            case ScreenType.ALBUM:
                Tap_Album.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                Tap_Album.GetComponent<FadeOutSystem>().FadeIn();
                if (Tap_InAlbum.activeSelf) Tap_InAlbum.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                if (Tap_InAlbum.activeSelf) Tap_InAlbum.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_Photo.activeSelf) Tap_Photo.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                if (Tap_Photo.activeSelf) Tap_Photo.GetComponent<FadeOutSystem>().FadeOut();
                break;
            case ScreenType.INALBUM:
                if(currentScreen == ScreenType.ALBUM)
                {
                    Tap_Album.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                    Tap_Album.GetComponent<FadeOutSystem>().FadeOut();
                    Tap_InAlbum.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                    Tap_InAlbum.GetComponent<FadeOutSystem>().FadeIn();
                }
                else
                {
                    Tap_Photo.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                    Tap_Photo.GetComponent<FadeOutSystem>().FadeOut();
                    Tap_InAlbum.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                    Tap_InAlbum.GetComponent<FadeOutSystem>().FadeIn();
                }
                break;
            case ScreenType.PHOTO:
                if (Tap_Album.activeSelf) Tap_Album.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                if (Tap_Album.activeSelf) Tap_Album.GetComponent<FadeOutSystem>().FadeOut();
                if (Tap_InAlbum.activeSelf) Tap_InAlbum.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                if (Tap_InAlbum.activeSelf) Tap_InAlbum.GetComponent<FadeOutSystem>().FadeOut();
                Tap_Photo.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                Tap_Photo.GetComponent<FadeOutSystem>().FadeIn();
                break;
            default:
                print("unkown type");
                break;
        }
        currentScreen = type;
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                switch (currentScreen)
                {
                    case ScreenType.ALBUM:
                        GetComponent<AppMain>().Close();
                        break;
                    case ScreenType.INALBUM:
                        ChangeScreen(ScreenType.ALBUM);
                        break;
                    case ScreenType.PHOTO:
                        ChangeScreen(ScreenType.INALBUM);
                        break;
                }
                backButtonTime = 0.0f;
            }
        }

    }
    public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
    public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.
    float beforeMousePosY;
    void Update()
    {
        // If there are two touches on the device...
        if (currentScreen == ScreenType.PHOTO)
        {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
            if (Input.GetMouseButtonDown(1))
            {
                if (!onZoom)
                {
                    beforeMousePosY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
                    onZoom = true;
                }
            }
            if (onZoom)
            {
                Tap_Photo.transform.Find("Background").GetComponent<UnityEngine.UI.ScrollRect>().enabled = false;

                float gab = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - beforeMousePosY;
                beforeMousePosY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;

                RectTransform photo_transform = (RectTransform)Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo2").transform;
                zoom += gab * orthoZoomSpeed;
                print(gab + " " + zoom);
                zoom = Mathf.Clamp(zoom, 1f, 4f);
                photo_transform.sizeDelta = new Vector2(1080 * zoom, 1450 * zoom);
            }
            if(Input.GetMouseButtonUp(1))
            {
                onZoom = false;
                Tap_Photo.transform.Find("Background").GetComponent<UnityEngine.UI.ScrollRect>().enabled = true;
            }
#elif UNITY_ANDROID
            if (Input.touchCount == 2)
            {
                onZoom = true;
                Tap_Photo.transform.Find("Background").GetComponent<UnityEngine.UI.ScrollRect>().enabled = false;

                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                RectTransform photo_transform = (RectTransform)Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo2").transform;
                zoom += deltaMagnitudeDiff * orthoZoomSpeed * -0.01f;
                zoom = Mathf.Clamp(zoom, 1f, 4f);
                photo_transform.sizeDelta = new Vector2(1080 * zoom, 1450 * zoom);
            }
            else
            {
                onZoom = false;
                Tap_Photo.transform.Find("Background").GetComponent<UnityEngine.UI.ScrollRect>().enabled = true;
            }
#endif

            if (Input.touchCount == 0 && !onZoom)
            {
                RectTransform photosTrans = (RectTransform)Tap_Photo.transform.Find("Background").Find("Photos").transform;
                RectTransform picTrans = (RectTransform)Tap_Photo.transform.Find("Background").Find("Photos").Find("Photo2").transform;
                MyCharacter character = ToolClass.GetCharacter(currentCharCode);
                // 좌측
                if (picTrans.sizeDelta.x / 2 - 1080 / 2 < photosTrans.anchoredPosition.x)
                {
                    // 넘어갔을때
                    if (picTrans.sizeDelta.x / 2 < photosTrans.anchoredPosition.x && currentPicPos + 1 < character.collectedPhotos.Count)
                        photosTrans.anchoredPosition =
                            new Vector2(Mathf.Lerp(photosTrans.anchoredPosition.x, photosTrans.anchoredPosition.x + 1080, 0.1f), photosTrans.anchoredPosition.y);
                    // 안넘어갔으면 고정
                    else
                        photosTrans.anchoredPosition =
                            new Vector2(Mathf.Lerp(photosTrans.anchoredPosition.x, picTrans.sizeDelta.x / 2 - 1080 / 2, 0.1f), photosTrans.anchoredPosition.y);
                }
                if (-(picTrans.sizeDelta.x / 2 - 1080 / 2) > photosTrans.anchoredPosition.x)
                {
                    if (-(picTrans.sizeDelta.x / 2) > photosTrans.anchoredPosition.x && currentPicPos - 1 >= 0)
                        photosTrans.anchoredPosition =
                            new Vector2(Mathf.Lerp(photosTrans.anchoredPosition.x, photosTrans.anchoredPosition.x - 1080, 0.1f), photosTrans.anchoredPosition.y);
                    else
                        photosTrans.anchoredPosition =
                            new Vector2(Mathf.Lerp(photosTrans.anchoredPosition.x, -(picTrans.sizeDelta.x / 2 - 1080 / 2), 0.1f), photosTrans.anchoredPosition.y);
                }
                OnPhotoMoved();
            }
        }
        BackButtonProc();
    }
    float zoom;
    bool onZoom;

    private static PhotoMain instance;
    public static PhotoMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("PhotoMain").GetComponent<PhotoMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }
        return instance;
    }
}
