﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class SettingMain : MonoBehaviour {
    MainData mainData;
    public UnityEngine.UI.Toggle airplane, gameAlram, androidAlram, vibration;
    public UnityEngine.UI.Slider sound;
    public UnityEngine.UI.Image bt1, bt15, bt2;
    public Sprite sound_zero, sound_nonZero, img1o, img1f, img2o, img2f, img3o, img3f;
    public UnityEngine.UI.Image sound_circle;
    public void SpeedButton(int type)
    {
        switch (type)
        {
            case 0:
                break;
            case 1:
                if ((float)mainData.achMainData.score / (float)AchMain.GetInstance().maxScore > 0.2f)
                {
                    mainData.settingData.speed = 1.5f;
                }
                else
                    Modal.GetInstance().CreateNewOkModal("업적을 20% 달성 해야 합니다.");
                break;
            case 2:
                if ((float)mainData.achMainData.score / (float)AchMain.GetInstance().maxScore > 0.5f)
                {
                    mainData.settingData.speed = 2f;
                }
                else
                    Modal.GetInstance().CreateNewOkModal("업적을 50% 달성 해야 합니다.");
                break;
            default:
                print("Wrong Type");
                break;
        }
        if (mainData.settingData.speed == 1.5f)
        {
            bt1.sprite = img1o;
            bt15.sprite = img2o;
            bt2.sprite = img3o;
        }
        else if (mainData.settingData.speed == 2f)
        {
            bt1.sprite = img1o;
            bt15.sprite = img2o;
            bt2.sprite = img3f;
        }
        else
        {
            bt1.sprite = img1o;
            bt15.sprite = img2f;
            bt2.sprite = img3f;
        }
    }
    public void Init()
    {
        mainData = GameMain.GetInstance().mainData;
        gameAlram.isOn = mainData.settingData.gameAlram;
        androidAlram.isOn = mainData.settingData.androidAlram;
        sound.value = mainData.settingData.sound;
        vibration.isOn = mainData.settingData.vibration;
        if (mainData.settingData.speed == 1.5f)
        {
            bt1.sprite = img1o;
            bt15.sprite = img2o;
            bt2.sprite = img3o;
        }
        else if (mainData.settingData.speed == 2f)
        {
            bt1.sprite = img1o;
            bt15.sprite = img2o;
            bt2.sprite = img3f;
        }
        else
        {
            bt1.sprite = img1o;
            bt15.sprite = img2f;
            bt2.sprite = img3f;
        }
        gameAlram.gameObject.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().enabled = !mainData.settingData.gameAlram;
        androidAlram.gameObject.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().enabled = !mainData.settingData.androidAlram;
        vibration.gameObject.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().enabled = !mainData.settingData.vibration;
    }
    public void Airplane()
    {
        Modal.GetInstance().CreateNewOkModal("친구들과 더 대화하고 싶다.");
    }
    public void GameAlram(bool val)
    {
        mainData.settingData.gameAlram = val;
        gameAlram.gameObject.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().enabled = !val;
    }
    public void AndroidAlram(bool val)
    {
        mainData.settingData.androidAlram = val;
        androidAlram.gameObject.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().enabled = !val;
    }
    public void Sound(float val)
    {
        mainData.settingData.sound = val;
        if (val == 0f) sound_circle.sprite = sound_zero;
        else sound_circle.sprite = sound_nonZero;
    }
    public void Vibration(bool val)
    {
        mainData.settingData.vibration = val;
        vibration.gameObject.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().enabled = !val;
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
}