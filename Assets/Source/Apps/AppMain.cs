﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class AppMain : MonoBehaviour
{
    [HideInInspector] public FadeOutSystem fos;
    [HideInInspector] public AppType appType;
    [HideInInspector] public Home myHome;
    [HideInInspector] public DockApp dockApp;
    [HideInInspector] public GameMain gm;

    int _count;

    public void Close()
    {
        myHome.fos.FadeIn();
        fos.SetType(FadeOutSystem.ChangeType.Zoom);
        fos.FadeOut();
    }

    public void SetAlram(int count)
    {
        GameMain.GetInstance().mainData.appAlramLogs[appType] = count;
        _count = count;
        dockApp.SetNotification(_count);
    }
    public void AddAlram(int count)
    {
        GameMain.GetInstance().mainData.appAlramLogs[appType] = count;
        _count = _count + count;
        dockApp.SetNotification(_count);
    }

    public void Init(Home home, DockApp myApp)
    {
        myHome = home;
        dockApp = myApp;
        appType = dockApp.appType;
        fos = GetComponent<FadeOutSystem>();
        fos.Init(4f, false, 1f, true);
    }
}