﻿using MyTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureMain : MonoBehaviour
{
    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Text scriptText;
    public GameObject chatPanel;
    float scriptProcess;
    string currentScript;
    float faceProcess;
    // Use this for initialization
    void Start ()
    {
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 6);
            if (rand == 1)
            {
                nextScript = "데굴데굴~";
            }
            else if (rand == 2)
            {
                nextScript = "재미있어?\n다행이다!";
            }
            else if (rand == 3)
            {
                nextScript = "모을 수 있는 인형은\n총 8개야!";
            }
            else if (rand == 4)
            {
                nextScript = "터치하면\n움직일 수 있어!";
            }
            else if (rand == 5)
            {
                nextScript = "너무 흔들지는 마..\n어지러워...";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 3.0f * speed;
    public GameObject char1, char2, char3, char4;
    void UpdateScript()
    {
        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            ChangeScript();
        }

        if (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    public Sprite normal, closeEye;
    void UpdateFace()
    {
        if (head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye;
            }
        }
        else if (head.sprite == closeEye)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = normal;
            }
        }
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    void Update ()
    {
        EnterTouch();
        UpdateScript();
        UpdateFace();
        UpdatePosition();
        BackButtonProc();
    }
    GameObject currentObject;
    public void EnterTouch()
    {
        foreach(var touch in Input.touches)
        {
            if(touch.phase == TouchPhase.Began)
            {
                Vector2 position = Camera.main.ScreenToWorldPoint(touch.position);
                if (char1.GetComponent<Collider2D>().OverlapPoint(position))
                {
                    currentObject = char1;
                }
                else if (char2.GetComponent<Collider2D>().OverlapPoint(position))
                {
                    currentObject = char2;
                }
                else if (char3.GetComponent<Collider2D>().OverlapPoint(position))
                {
                    currentObject = char3;
                }
                else if (char4.GetComponent<Collider2D>().OverlapPoint(position))
                {
                    currentObject = char4;
                }

                if (currentObject != null)
                {
                    currentObject.GetComponent<Collider2D>().enabled = false;
                }
            }
        }
    }
    public void ReleaseObject()
    {
        if (currentObject != null)
        {
            currentObject.GetComponent<Collider2D>().enabled = true;
            currentObject = null;
        }
    }
    void UpdatePosition()
    {
        if (Input.touchCount >= 1 && currentObject != null)
        {
            currentObject.transform.position = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        }
        else
        {
            if (currentObject != null)
            {
                currentObject.GetComponent<Collider2D>().enabled = true;
                currentObject = null;
            }
        }
    }
    private static FigureMain instance;
    public static FigureMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("FigureMain").GetComponent<FigureMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
}
