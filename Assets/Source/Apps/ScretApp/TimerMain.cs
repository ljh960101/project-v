﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using Assets.SimpleAndroidNotifications;
using MyTool;
using System;
using UnityEngine.UI;

public class TimerMain : MonoBehaviour {
    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Text scriptText;
    public UnityEngine.UI.Text m, s;
    public GameObject chatPanel;
    bool onStart;
    float scriptProcess;
    string currentScript;
    float faceProcess;

    void Init()
    {
        isInited = true;
        onStart = false;
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
    }
    public void Plus(int type) // 01 ms
    {
        if (onStart)
        {
            ChangeScript("시간을 재고 있잖아!");
            return;
        }
        if (type == 0)
        {
            int min = int.Parse(m.text);
            ++min;
            if (min >= 100) min = 0;
            if (min < 10) m.text = "0" + min;
            else m.text = min + "";
        }
        else
        {
            int sec = int.Parse(s.text);
            ++sec;
            if (sec >= 60) sec = 0;
            if (sec < 10) s.text = "0" + sec;
            else s.text = sec + "";
        }
    }
    public void Minus(int type) // 01 ms
    {
        if (onStart)
        {
            ChangeScript("시간을 재고 있잖아!");
            return;
        }
        if (type == 0)
        {
            int min = int.Parse(m.text);
            --min;
            if (min < 0) min = 99;
            if (min < 10) m.text = "0" + min;
            else m.text = min + "";
        }
        else
        {
            int sec = int.Parse(s.text);
            --sec;
            if (sec < 0) sec = 59;
            if (sec < 10) s.text = "0" + sec;
            else s.text = sec + "";
        }
    }
    public void StartTimer()
    {
        onStart = !onStart;
        if (onStart)
        {
            _m = float.Parse(m.text);
            _s = float.Parse(s.text);
            ChangeScript("시간이 되면 알려줄게.");
        }
        else
        {
            ChangeScript("알겠어.\n그만 셀게.");
            t.SetActive(true);
        }
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 5);
            if (rand == 1)
            {
                nextScript = "커피 향이\n너무 좋다.";
            }
            else if (rand == 2)
            {
                nextScript = "나중에 같이\n카페 갈래?";
            }
            else if (rand == 3)
            {
                nextScript = "시간은 언제나\n잘 지켜야해.";
            }
            else if (rand == 4)
            {
                nextScript = "약속을 지키지\n않는 건 남을\n무시하는 행동이야.";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 3.0f * speed;
    void UpdateScript()
    {
        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            ChangeScript();
        }

        if (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    float _m, _s;
    bool isInited;
    public GameObject t;
    public void UpdateTimer(float time)
    {
        if (!isInited) Init();
        if (onStart)
        {
            _s -= time;
            if (_s <= 0)
            {
                if (_m <= 0.0f)
                {
                    // 알람처리
                    ToolClass.MakeNotify(AppType.Timer, "시간 됐어!");
                    ChangeScript("시간 됐어!");
                    onStart = false;
                    t.SetActive(true);
                    _s = 0f;
                    _m = 0f;
                }
                else
                {
                    _m -= 1.0f;
                    _s += 60.0f;
                }
            }
            s.text = (int)_s + "";
            m.text = (int)_m + "";

            if (s.text.Length <= 1) s.text = "0" + s.text;
            if (m.text.Length <= 1) m.text = "0" + m.text;
            if ((int)_s % 2 == 0 && onStart)
                t.SetActive(false);
            else t.SetActive(true);
        }
    }
    public Sprite normal, putCup, middle;
    int next = 0, id = 0;
    public void OnPause(bool pauseStatus)
    {
        if (pauseStatus && onStart)
        {
            NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(_m * 60 + _s),
                        "내 폰 속 로맨스",
                        "소유: 시간 됐어!", new Color(1, 0.3f, 0.15f));
        }
    }
    void UpdateFace()
    {
        if (head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 2.0f)
            {
                faceProcess -= 2.0f;
                head.sprite = middle;
                next = 0;
            }
        }
        else if (head.sprite == middle)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.5f)
            {
                faceProcess -= 0.5f;
                if (next == 0) head.sprite = putCup;
                else head.sprite = normal;
            }
        }
        else if (head.sprite == putCup)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 5.0f)
            {
                faceProcess -= 5.0f;
                head.sprite = middle;
                next = 1;
            }
        }
    }
    void Update()
    {
        UpdateScript();
        UpdateFace();
        //UpdateTimer();
        BackButtonProc();
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    private static TimerMain instance;
    public static TimerMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("TimerMain").GetComponent<TimerMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }
        return instance;
    }
}
