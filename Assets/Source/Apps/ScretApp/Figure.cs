﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour {
    const float speed = 500.0f;
    void FixedUpdate()
    {
        float x = Input.acceleration.x * Time.deltaTime * speed;
        float y = Input.acceleration.y * Time.deltaTime * speed;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(x, y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Danger")
        {
            FigureMain.GetInstance().ReleaseObject();
        }
    }
}
