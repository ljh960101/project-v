﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using MyTool;

public class ClockMain : MonoBehaviour {
    public UnityEngine.UI.Text text_h, text_m, text_s;
    public UnityEngine.UI.Text scriptText;
    public UnityEngine.UI.Image head;
    public GameObject chatPanel;
    public GameObject comma1, comma2;
    public Sprite normal, closeEye, sleep, shock;
    float scriptProcess;
    string currentScript;
    float faceProcess;
    float sleepCount;
    Vector2 defaultHeadpos;
	// Use this for initialization
	void Start () {
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
        sleepCount = 0.0f;
        defaultHeadpos = ((RectTransform)head.transform).anchoredPosition;
    }
    public void OnClick()
    {
        sleepCount = 0.0f;
        if(head.sprite == sleep)
        {
            head.sprite = shock;
            faceProcess = 0.0f;
            ChangeScript("깜짝이야!");
        }
        else if(head.sprite != shock)
        {
            ChangeScript();
        }
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 3.0f * speed;
    void UpdateScript()
    {
        if(head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if(faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye;
            }
        }
        else if (head.sprite == closeEye)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = normal;

            }
        }
        else if (head.sprite == shock)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.3f)
            {
                faceProcess -= 0.3f;
                head.sprite = normal;
            }
        }

        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            if(head.sprite == normal) ChangeScript();
            else if(head.sprite == sleep) ChangeScript("Zzz");
        }

        if(currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 6);
            if (rand == 1)
            {
                nextScript = "째깍 째깍~";
            }
            else if (rand == 2)
            {
                nextScript = "시간이 지나가고 있어!";
            }
            else if (rand == 3)
            {
                nextScript = "오늘은 뭐했어?";
            }
            else if (rand == 4)
            {
                nextScript = "남은 하루도 행복해!";
            }
            else if (rand == 5)
            {
                nextScript = "시간은 돈으로\n바꿀 수 없댔어.";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
	void Update ()
    {
        string h = DateTime.Now.Hour + "";
        string m = DateTime.Now.Minute + "";
        string s = DateTime.Now.Second + "";
        if (h.Length <= 1) h = "0" + h;
        if (m.Length <= 1) m = "0" + m;
        if (s.Length <= 1) s = "0" + s;
        text_h.text = h;
        text_m.text = m;
        text_s.text = s;
        if (DateTime.Now.Second % 2 == 0)
        {
            comma1.SetActive(false);
            comma2.SetActive(false);
        }
        else
        {
            comma1.SetActive(true);
            comma2.SetActive(true);
        }
        UpdateScript();
        UpdateState();
        UpdateHeadPosition();
        BackButtonProc();
    }

    float rot;
    bool rotOn;
    void UpdateHeadPosition()
    {
        if (head.sprite == normal || head.sprite == closeEye)
        {
            if (rotOn)
            {
                rot += Time.deltaTime * 10;
                if (rot >= 5.0f) rotOn = !rotOn;
            }
            else
            {
                rot -= Time.deltaTime * 10;
                if (rot <= -8.0f) rotOn = !rotOn;
            }
            Vector3 rotationEuler = Vector3.forward * rot; //increment 30 degrees every second
            head.transform.rotation = Quaternion.Euler(rotationEuler);
        }
        else if (head.sprite == sleep)
        {
            if (rotOn)
            {
                rot += Time.deltaTime * 10;
                if (rot >= 5.0f) rotOn = !rotOn;
            }
            else
            {
                rot -= Time.deltaTime * 10;
                if (rot <= -5.0f) rotOn = !rotOn;
            }
            ((RectTransform)head.transform).anchoredPosition = new Vector2(defaultHeadpos.x, defaultHeadpos.y + rot);
        }
        else {
            ((RectTransform)head.transform).anchoredPosition = defaultHeadpos;
        }
    }
    void UpdateState()
    {
        const float sleepTime = 10.0f;
        sleepCount += Time.deltaTime;
        if(sleepCount >= sleepTime && head.sprite == normal && (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1))
        {
            head.sprite = sleep;
            ChangeScript("...");
        }
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
}
