﻿using MyTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectGameMain : MonoBehaviour {
    List<GameObject> selects;
    public Transform selectList;
    public GameObject selectPrefab;
    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Text scriptText;
    public GameObject chatPanel;
    public Sprite completeBar, noneCompleteBar;
    float scriptProcess;
    string currentScript;
    float faceProcess;

    // Use this for initialization
    public void OnSelectChanged()
    {
        foreach (var select in selects)
            select.gameObject.GetComponent<UnityEngine.UI.Image>().sprite = noneCompleteBar;
    }
    void Start ()
    {
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
        selects = new List<GameObject>();
        GameObject newObj = Instantiate(selectPrefab, selectList);
        newObj.GetComponent<UnityEngine.UI.InputField>().onValueChanged.AddListener(delegate { OnSelectChanged(); });
        selects.Add(newObj);
	}
    public void Plus()
    {
        if (selects.Count >= 5)
        {
            ChangeScript("너무 선택지가 많아!");
            return;
        }
        OnSelectChanged();
        GameObject newObj = Instantiate(selectPrefab, selectList);
        newObj.GetComponent<UnityEngine.UI.InputField>().onValueChanged.AddListener(delegate{ OnSelectChanged(); });
        selects.Add(newObj);
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 5);
            if (rand == 1)
            {
                nextScript = "선택하는 건\n언제나 힘든 일이야.";
            }
            else if (rand == 2)
            {
                nextScript = "내가 언제나 좋은\n선택을 해줄 수 있는 건\n아니지만..";
            }
            else if (rand == 3)
            {
                nextScript = "내가 도움을\n줄 수 있다면\n정말 다행이야!";
            }
            else if (rand == 4)
            {
                nextScript = "선택을 할 수\n있다는 것은\n정말 행복한 일이야.";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 5.0f * speed;
    void UpdateScript()
    {
        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            ChangeScript();
        }

        if (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    public void Minus()
    {
        if (selects.Count <= 1)
        {
            ChangeScript("너무 선택지가 적잖아!");
            return;
        }
        OnSelectChanged();
        GameObject topObj = selects[selects.Count - 1];
        Destroy(topObj);
        selects.Remove(topObj);
    }
    public void StartGame()
    {
        bool onComplete = false;
        foreach (var obj in selects)
        {
            if (obj.GetComponent<UnityEngine.UI.InputField>().text.Length < 1)
            {
                ChangeScript("선택지를 다 채워줘!");
                return;
            }
            if(obj.GetComponent<UnityEngine.UI.Image>().sprite == completeBar)
            {
                onComplete = true;
            }
        }
        if (onComplete)
        {
            int i = UnityEngine.Random.Range(0, 2);
            if (i == 0)
                ChangeScript("이미 선택했어!");
            else
                ChangeScript("설마 내 선택이\n싫은 건 아니지?");
        }
        else
        {
            int i = UnityEngine.Random.Range(0, selects.Count);
            ChangeScript(selects[i].GetComponent<UnityEngine.UI.InputField>().text + "!\n이게 제일 좋은 것 같아!");
            selects[i].GetComponent<UnityEngine.UI.Image>().sprite = completeBar;
            head.sprite = selectSuccess;
            faceProcess = 0.0f;
            if(selects.Count == 1)
            {
                ChangeScript(selects[i].GetComponent<UnityEngine.UI.InputField>().text + "..\n답이 정해져 있잖아!");
            }
        }
    }
    public Sprite normal, closeEye, selectSuccess, closeEye2;
	void UpdateFace()
    {
        if (head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye;
            }
        }
        else if (head.sprite == closeEye)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = normal;
            }
        }
        else if (head.sprite == selectSuccess)
        {
            faceProcess += Time.deltaTime;
            if(currentScript.Length + hideChatDelay < scriptProcess)
            {
                faceProcess = 0.0f;
                head.sprite = normal;
            }
            else if (faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye2;
            }
        }
        else if (head.sprite == closeEye2)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = selectSuccess;
            }
        }
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    void Update () {
        UpdateScript();
        UpdateFace();
        BackButtonProc();
    }
}
