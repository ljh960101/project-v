﻿using MyTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarExpressMain : MonoBehaviour {
    public enum ScreenState
    {
        Default,
        Express
    };
    ScreenState currentState;
    public GameObject Tap_Default, Tap_Express;
    public UnityEngine.UI.Dropdown dropdown;
    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Text scriptText;
    public GameObject chatPanel;
    float scriptProcess;
    string currentScript;
    float faceProcess;
    // Use this for initialization
    void Start () {
        currentState = ScreenState.Default;
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
        Tap_Default.SetActive(true);
        Tap_Express.SetActive(false);
        Tap_Default.GetComponent<FadeOutSystem>().Init(2.0f, false, 1, false);
        Tap_Express.GetComponent<FadeOutSystem>().Init(2.0f, false, 1, false);
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 5);
            if (rand == 1)
            {
                nextScript = "믿거나 말거나~";
            }
            else if (rand == 2)
            {
                nextScript = "별자리는 항해에\n큰 도움을 준대..";
            }
            else if (rand == 3)
            {
                nextScript = "별자리는 기원전부터\n연구해왔대!";
            }
            else if (rand == 4)
            {
                nextScript = "자신의 별자리는\n생일날 볼 수 없어.";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 3.0f * speed;
    void UpdateScript()
    {
        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            ChangeScript();
        }

        if (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    public Sprite normal, closeEye;
    void UpdateFace()
    {
        if (head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye;
            }
        }
        else if (head.sprite == closeEye)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = normal;
            }
        }
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    public void ShowDetail()
    {
        ChangeScreen(ScreenState.Express);
        ChangeScript("음...\n그렇구나!");
    }
    public void Back()
    {
        ChangeScreen(ScreenState.Default);
        ChangeScript("다른 별자리도\n확인해볼래?");
    }
    public UnityEngine.UI.Text expression;
    void ChangeExpression()
    {
        int val = dropdown.value;
        string text = "";
        switch (val)
        {
            case 0:
                {
                    text = "<size=80><color=blue>양</color></size>\n\n";
                    text += "양자리는 욕망, 야망, 내면의 힘, 자신감 그리고 카리스마를 가지고 있습니다.\n우리는 양을 온순하다고 알고 있지만 원래 양은 대담함을 상징한다고 합니다.\n이 별자리의 사람은 모험심이 많고,\n다른 사람들이 그들에게 몰려들도록 하는 천성을 지니고 있습니다.";
                }
                break;
            case 1:
                {
                    text = "<size=80><color=blue>황소</color></size>\n\n";
                    text += "황소자리는 물질 만능주의자이며, 꽤 섬세한 면을 지니고 있습니다.\n이 사람들은 현실적이고, 실용적이며, 조직적으로, 미식가의 취향을 가지고 있습니다.\n그들은 일반적으로 친절하고, 성실하며, 충성심이 강한 사람들이며, 확고하고 다소 검소하다고 알려졌지만, 그들은 가족과 친구, 심지어 낯선 사람들에게까지 아낌없이 도움을 줍니다.";
                }
                break;
            case 2:
                {
                    text = "<size=80><color=blue>쌍둥이</color></size>\n\n";
                    text += "쌍둥이자리는 지능과 창의력이 높은 것으로 알려졌고, 또한 이중성을 상징합니다.\n그들은 웅변가이자 위대한 사상가이며, 다양한 삶을 즐기고 싶어 합니다.\n그러므로 그들을 예의주시하면서, 숨 쉴 공간을 주도록 해야 합니다.\n그렇지 않으면 그들은 멀리 가버릴 것입니다.";
                }
                break;
            case 3:
                {
                    text = "<size=80><color=blue>게</color></size>\n\n";
                    text += "게자리는 별자리계의 어머니라고도 알려졌으며 자연계의 양육자입니다.\n게자리 사람들은 천성적으로 이해심이 많습니다.\n가장 좋은 조합은 안정적인 황소자리 또는 근면하고 현실적인 처녀자리입니다.";
                }
                break;
            case 4:
                {
                    text = "<size=80><color=blue>사자</color></size>\n\n";
                    text += "사자자리는 따뜻하고 카리스마가 넘치며 웅장한 행동을 하는 경향이 있습니다.\n사자자리는 쾌활하고, 장난기가 넘치며, 모임을 좋아하며 화제의 중심이 되고자 합니다.\n특히 뛰어난 지도력을 자랑합니다.";
                }
                break;
            case 5:
                {
                    text = "<size=80><color=blue>처녀</color></size>\n\n";
                    text += "처녀자리는 강한 정신력을 상징합니다.\n세심한 성격으로 유명한 처녀자리는 또한 다른 사람들에게도 최고의 배려를 제공합니다.\n그들은 문제 해결사로, 혼란의 한 가운데에서도 명확하게 생각할 수 있습니다.\n처녀자리와 가장 잘 어울리는 별자리는 게자리나 물고기자리입니다.";
                }
                break;
            case 6:
                {
                    text = "<size=80><color=blue>천칭</color></size>\n\n";
                    text += "천칭자리는 공정과 균형을 상징하며 별자리의 인도주의자입니다.\n그들은 매력적이고 친절합니다.\n천칭자리 사람들은 재미있는 사자자리나 이상적인 궁수자리와 잘 어울립니다.";
                }
                break;
            case 7:
                {
                    text = "<size=80><color=blue>전갈</color></size>\n\n";
                    text += "전갈은 이름에서 알 수 있듯이 용기와 힘과 창의력을 강조합니다.\n훌륭한 연인이며 강한 성격을 지닌 전갈자리는 별자리 중의 수수께끼입니다.";
                }
                break;
            case 8:
                {
                    text = "<size=80><color=blue>켄타우로스</color></size>\n\n";
                    text += "궁수자리는 단지 궁수가 아니라 켄타우로스로 말의 몸을 가진 사람입니다.\n그들은 유머 감각, 모험심, 자유로운 영혼의 소유자로 유명합니다.\n이 별자리는 다른 사람뿐만 아니라 자신을 위해서도 삶의 즐거움을 추구합니다.";
                }
                break;
            case 9:
                {
                    text = "<size=80><color=blue>염소</color></size>\n\n";
                    text += "염소자리는 가장 매력적인 성격의 소유자로 강직함을 상징합니다.\n그들은 당신이 의지할 수 있는 사람입니다.\n두드러진 점으로는 현실에 근거하여 확고한 기반을 확보하고 있으며 책임감과 충성도가 강합니다.";
                }
                break;
            case 10:
                {
                    text = "<size=80><color=blue>물병</color></size>\n\n";
                    text += "물병자리는 규칙이나 전례를 신경 쓰지 않는 사람들입니다.\n독창적이고 활기찬 대화를 이끌어 가는 성향은 물병자리의 상징입니다.\n수줍음이 많고 조용하지만 다른 한편 별나고, 수다스러우며, 활동적인 모험을 좋아합니다.";
                }
                break;
            case 11:
                {
                    text = "<size=80><color=blue>물고기</color></size>\n\n";
                    text += "물고기자리는 여기저기 끌려다니면서 구체적인 결정을 내리기 힘들어하는 것을 상징합니다.\n매우 낭만적인 그들은 환상적인 삶을 추구하는 경향이 있습니다.\n물고기자리는 융통성 있고 용서하며 이타적이고 항상 다른 사람들을 도우려고 합니다.";
                }
                break;
            default:
                print("Wrong Value");
                break;
        }
        expression.text = text;
    }
    void ChangeScreen(ScreenState state)
    {
        switch (state)
        {
            case ScreenState.Default:
                Tap_Default.GetComponent<FadeOutSystem>().FadeIn();
                Tap_Express.GetComponent<FadeOutSystem>().FadeOut();
                break;
            case ScreenState.Express:
                ChangeExpression();
                Tap_Default.GetComponent<FadeOutSystem>().FadeOut();
                Tap_Express.GetComponent<FadeOutSystem>().FadeIn();
                break;
        }
    }
	// Update is called once per frame
	void Update ()
    {
        UpdateScript();
        UpdateFace();
        BackButtonProc();
    }
    private static StarExpressMain instance;
    public static StarExpressMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("StarExpressMain").GetComponent<StarExpressMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
}
