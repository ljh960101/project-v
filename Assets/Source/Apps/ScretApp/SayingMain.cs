﻿using MyTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SayingMain : MonoBehaviour {
    public GameObject texts, buttons;
    int currentCategori = 0;
    string nextBook, nextName;
    public void BackBtn()
    {
        nextBook = "";
        nextName = "";
        ChangeScreen(true);
    }
    public void Refresh()
    {
        ChangeText();
        ChangeScript("이건 어때?");
    }
    public void Recognize()
    {
        if(currentCategori==0) currentCategori = UnityEngine.Random.Range(1, 9);
        ChangeText();
        ChangeScript("내가 추천하는건\n이거야!");
    }
    public void ClickCatagori(int type)
    {
        currentCategori = type;
        ChangeText();
        ChangeScript("좋은 선택이야!");
    }
    void FadeInText()
    {
        if (nextBook != "")
        {
            texts.transform.Find("Text1").GetComponent<UnityEngine.UI.Text>().text = nextBook;
            texts.transform.Find("Text2").GetComponent<UnityEngine.UI.Text>().text = nextName;
            nextBook = "";
            nextName = "";
            texts.GetComponent<FadeOutSystem>().FadeIn();
        }
    }
    void ChangeText()
    {
        string book = "", name = "";
        int numb = UnityEngine.Random.Range(1, 9);
        // 순서대로 사랑, 인생, 공부, 성공, 친구, 이별, 시간, 도전
        switch (currentCategori)
        {
            case 1:
                switch (numb)
                {
                    case 1:
                        book = "사랑은 자신 이외에 다른 것도 존재한다는\n사실을 어렵사리 깨닫는 것이다.";
                        name = "아이리스 머독";
                        break;
                    case 2:
                        book = "세기의 사랑일지라도\n참고 견뎌내야 한다.";
                        name = "카브리엘 코코 샤넬";
                        break;
                    case 3:
                        book = "사랑은 그저 미친 짓이예요.";
                        name = "윌리엄 셰익스피어";
                        break;
                    case 4:
                        book = "죄를 미워하되\n죄인은 사랑하라.";
                        name = "마하트마 간디";
                        break;
                    case 5:
                        book = "우리는 오로지 사랑을 함으로써\n사랑을 배울 수 있다.";
                        name = "아이리스 머독";
                        break;
                    case 6:
                        book = "얼마나 많이 주느냐보다\n얼마나 많은 사랑을 담느냐가 중요하다.";
                        name = "마더 테레사";
                        break;
                    case 7:
                        book = "사랑이 아니면 결혼하지 말라.\n다만, 당신이 사랑스러운 점을\n사랑한다는 사실을 알라.";
                        name = "윌리엄 펜";
                        break;
                    case 8:
                    default:
                        book = "사랑에 의해 행해지는 것은\n언제나 선악을 초월한다.";
                        name = "프레드리히 니체";
                        break;
                }
                break;
            case 2:
                switch (numb)
                {
                    case 1:
                        book = "행복하게 여행하려면\n가볍게 여행해야 한다.";
                        name = "생텍쥐페리";
                        break;
                    case 2:
                        book = "젊음은 희망을 빨리 갖기 때문에\n그만큼 쉽게 현혹된다.";
                        name = "아리스토텔레스";
                        break;
                    case 3:
                        book = "영원히 살 것처럼 꿈꾸고\n오늘 죽을 것처럼 살아라.";
                        name = "제임스 딘";
                        break;
                    case 4:
                        book = "인생의 비극이란 사람들이 삶을\n사는 동안 내면에서 잃어가는 것들이다.";
                        name = "알버트 아인슈타인";
                        break;
                    case 5:
                        book = "인생에는 서두르는 것 말고도\n더 많은 것이 있다.";
                        name = "마하트마 간디";
                        break;
                    case 6:
                        book = "인생이 진지하다는 증거는\n털끝만큼도 없다.";
                        name = "브렌단 길";
                        break;
                    case 7:
                        book = "나이가 성숙을 보장하지는 않는다.";
                        name = "라와나 블랙웰";
                        break;
                    case 8:
                    default:
                        book = "우리는 나이가 들면서 변하는 게 아니다.\n보다 자기다워지는 것이다.";
                        name = "린 홀";
                        break;
                }
                break;
            case 3:
                switch (numb)
                {
                    case 1:
                        book = "목적없는 공부는 기억에 해가 될 뿐이며,\n머리속에 들어온 어떤 것도 간직하지 못한다.";
                        name = "레오나르도 다빈치";
                        break;
                    case 2:
                        book = "교육은 양날의 칼과 같다.\n제대로 다루지 못하면\n위험한 용도로 쓰일 수 있다.";
                        name = "우 팅-팡";
                        break;
                    case 3:
                        book = "교육은 최상의 노후 대비책이다.";
                        name = "아리스토텔레스";
                        break;
                    case 4:
                        book = "아이들이 무엇을 할 수 있는지 확인해보고\n싶다면 주는 것을 멈추어 보면 된다.";
                        name = "노먼 더글러스";
                        break;
                    case 5:
                        book = "배움은 의무도, 생존도 아니다.";
                        name = "에드워즈 데밍";
                        break;
                    case 6:
                        book = "할 수 있는 자는 행한다.\n할 수 없는 자는 가르친다";
                        name = "조지 버나드 쇼";
                        break;
                    case 7:
                        book = "제자가 계속 제자로만 남는다면\n스승에 대한 고약한 보답이다.";
                        name = "프레드리히 니체";
                        break;
                    case 8:
                    default:
                        book = "교육을 무시하는 것은 무지한 사람뿐이다.";
                        name = "퍼블릴리어스 사이러스";
                        break;
                }
                break;
            case 4:
                switch (numb)
                {
                    case 1:
                        book = "비전만 쫓다 보니 방향을 잃었다.";
                        name = "로빈 그린";
                        break;
                    case 2:
                        book = "성공의 겉모습 만큼 성공하는 것은 없다.";
                        name = "크리스토퍼 래쉬";
                        break;
                    case 3:
                        book = "남에게 이기는 방법의 하나의\n예의범절로 이기는 것이다.";
                        name = "조쉬 빌링스";
                        break;
                    case 4:
                        book = "승리의 순간은 오로지 그 순간만을\n위해 살기에는 너무 짧다.";
                        name = "마르티바 나브라틸로바";
                        break;
                    case 5:
                        book = "명확한 목표는 말의 곁눈 가리개처럼\n목표를 가진 이의 시야를 좁게 하기 마련이다.";
                        name = "로버트 프로스트";
                        break;
                    case 6:
                        book = "완벽함이 아니라 탁월함을 위해서 애써라.";
                        name = "잭슨 브라운 주니어";
                        break;
                    case 7:
                        book = "희망은 볼 수 없는 것을 보고,\n만져질 수 없는 것을 느끼고,\n불가능한 것을 이룬다.";
                        name = "헬렌 켈러";
                        break;
                    case 8:
                    default:
                        book = "성공이란 열정을 잃지 않고\n실패를 거듭할 수 있는 능력이다.";
                        name = "윈스턴 처칠";
                        break;
                }
                break;
            case 5:
                switch (numb)
                {
                    case 1:
                        book = "우정을 끝낼 수 있다면\n그 우정은 실제로 존재하지 않은 것이다.";
                        name = "성 제롬";
                        break;
                    case 2:
                        book = "단지 얘기 들어줄 사람이 필요해\n우정을 키우는 것은 좋지 않다.";
                        name = "라와나 블랙웰";
                        break;
                    case 3:
                        book = "풍요 속에서는 친구들이 나를 알게 되고,\n역경 속에서는 내가 친구를 알게 된다.";
                        name = "존 철튼 콜린스";
                        break;
                    case 4:
                        book = "운명의 기복은 친구의 신뢰를 시험한다.";
                        name = "키케로";
                        break;
                    case 5:
                        book = "아첨꾼은 나보다 열등하거나 그런 척 하는 친구이다.";
                        name = "아리스토텔레스";
                        break;
                    case 6:
                        book = "친구는 제 2의 자신이다.";
                        name = "아리스토텔레스";
                        break;
                    case 7:
                        book = "모든 것을 가졌다 해도 친구가 없다면,\n아무도 살길 원치 않을 것이다.";
                        name = "아리스토텔레스";
                        break;
                    case 8:
                    default:
                        book = "우리는 사랑하는 친구들에 의해서만 알려진다.";
                        name = "윌리엄 셰익스피어";
                        break;
                }
                break;
            case 6:
                switch (numb)
                {
                    case 1:
                        book = "이별의 아픔 속에서만\n사랑의 깊이를 알게 된다.";
                        name = "조지 앨리엇";
                        break;
                    case 2:
                        book = "멀리있는 친구만큼 세상을 넓어\n보이게 하는 것은 없다.\n그들은 위도와 경도가 된다.";
                        name = "헨리 데이비드 소로우";
                        break;
                    case 3:
                        book = "이 사랑의 꽃봉오리는\n여름날 바람에 마냥 부풀었다가,\n다음 만날 때엔 예쁘게 꽃필 거예요.";
                        name = "윌리엄 셰익스피어";
                        break;
                    case 4:
                        book = "작별 인사에 낙담하지 말라.\n재회에 앞서 작별은 필요하다.\n그리고 친구라면 잠시 혹은 오랜 뒤라도\n꼭 재회하게 될 터이니.";
                        name = "리처드 바크";
                        break;
                    case 5:
                        book = "수마일의 거리가 당신과 친구를 떼어놓을 수도 있다…\n하지만 사랑하는 누군가와 정말 함께 있고 싶다면,\n이미 거기 가있지 않겠는가?";
                        name = "리처드 바크";
                        break;
                    case 6:
                        book = "떠날 때가 되었으니,\n이제 각자의 길을 가자.\n나는 죽기 위해서,\n당신들은 살기 위해.\n어느 편이 더 좋은 지는\n오직 신만이 알 뿐이다.";
                        name = "소크라테스";
                        break;
                    case 7:
                        book = "당신을 만나는 모든 사람이 당신과 헤어질 때는\n더 나아지고 더 행복해질 수 있도록 하라.";
                        name = "마더 테레사";
                        break;
                    case 8:
                    default:
                        book = "인간의 감정은 누군가를 만날 때와\n헤어질 때 가장 순수하며 가장 빛난다.";
                        name = "장 폴 리히터";
                        break;
                }
                break;
            case 7:
                switch (numb)
                {
                    case 1:
                        book = "미래는 현재 우리가 무엇을\n하는가에 달려 있다.";
                        name = "마하트마 간디";
                        break;
                    case 2:
                        book = "사람들은 인생이 순전히 시간만\n너무 잡아먹는다는 것을 깨닫는다.";
                        name = "스타니스와프 J. 렉";
                        break;
                    case 3:
                        book = "이나는 과거 속에 사는 편인데\n내 생애 대부분이 거기 있기 때문이다.";
                        name = "허브 캐언";
                        break;
                    case 4:
                        book = "미래를 예측하는 최선의 방법은\n미래를 창조하는 것이다.";
                        name = "알랜 케이";
                        break;
                    case 5:
                        book = "우리 시대의 문제는 미래가\n예전의 미래와 다르다는 것이다.";
                        name = "폴 발레리";
                        break;
                    case 6:
                        book = "과거를 기억 못하는 이들은\n과거를 반복하기 마련이다.";
                        name = "조지 산타야나";
                        break;
                    case 7:
                        book = "유행은 유행에 뒤떨어질 수 밖에 없게 만들어진다.";
                        name = "가브리엘 샤넬";
                        break;
                    case 8:
                    default:
                        book = "낭비한 시간에 대한 후회는\n더 큰 시간 낭비이다.";
                        name = "메이슨 쿨리";
                        break;
                }
                break;
            case 8:
                switch (numb)
                {
                    case 1:
                        book = "마음을 위대한 일로 이끄는 것은\n오직 열정, 위대한 열정뿐이다.";
                        name = "드니 디드로";
                        break;
                    case 2:
                        book = "세상의 중요한 업적 중 대부분은,\n희망이 보이지 않는 상황에서도\n끊임없이 도전한 사람들이 이룬 것이다.";
                        name = "데일 카네기";
                        break;
                    case 3:
                        book = "모든 고귀한 일은 찾기 드문만큼 하기도 어렵다.";
                        name = "바뤼흐 스피노자";
                        break;
                    case 4:
                        book = "가장 큰 위험은 위험없는 삶이다.";
                        name = "스티븐 코비";
                        break;
                    case 5:
                        book = "강한 신념이야말로\n거짓보다 더 위험한 진리의 적이다.";
                        name = "프리드리히 니체";
                        break;
                    case 6:
                        book = "절망은 마약이다.\n절망은 생각을 무관심으로 잠재울 뿐이다.";
                        name = "찰리 채플린";
                        break;
                    case 7:
                        book = "아무런 위험없이 승리하는 것은\n영광없는 승리에 다름 아니다.";
                        name = "피에르 코르네유";
                        break;
                    case 8:
                    default:
                        book = "이 세상에 위대한 사람은 없다.\n단지 평범한 사람들이 일어나 맞서는\n위대한 도전이 있을 뿐이다.";
                        name = "윌리엄 프레데릭 홀시";
                        break;
                }
                break;
            case 0:
            default:
                print("Wrong Categori");
                break;
        }

        if (texts.activeSelf)
        {
            nextBook = book;
            nextName = name;
            texts.GetComponent<FadeOutSystem>().FadeOut(delegate { FadeInText(); });
        }
        else
        {
            texts.transform.Find("Text1").GetComponent<UnityEngine.UI.Text>().text = book;
            texts.transform.Find("Text2").GetComponent<UnityEngine.UI.Text>().text = name;
            ChangeScreen(false);
        }
    }
    public GameObject backBtn, refreshBtn;
    void ChangeScreen(bool categoriTap)
    {
        switch (categoriTap)
        {
            case true:
                currentCategori = 0;
                buttons.GetComponent<FadeOutSystem>().FadeIn();
                texts.GetComponent<FadeOutSystem>().FadeOut();
                backBtn.SetActive(false);
                refreshBtn.SetActive(false);
                break;
            case false:
            default:
                buttons.GetComponent<FadeOutSystem>().FadeOut();
                texts.GetComponent<FadeOutSystem>().FadeIn();
                backBtn.SetActive(true);
                refreshBtn.SetActive(true);
                break;
        }
    }

    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Text scriptText;
    public GameObject chatPanel;
    float scriptProcess;
    string currentScript;
    float faceProcess; void Start()
    {
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
        buttons.SetActive(true);
        texts.SetActive(false);
        texts.GetComponent<FadeOutSystem>().Init(2.0f, false, 1, false);
        buttons.GetComponent<FadeOutSystem>().Init(2.0f, false, 1, false);
        backBtn.SetActive(false);
        refreshBtn.SetActive(false);
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 5);
            if (rand == 1)
            {
                nextScript = "좋은 명언들을\n읽어줄게.";
            }
            else if (rand == 2)
            {
                nextScript = "너의 삶에 보탬이\n됬으면 좋겠어.";
            }
            else if (rand == 3)
            {
                nextScript = "이상한 말도 널리\n알려지면 명언이래.";
            }
            else if (rand == 4)
            {
                nextScript = "다른 명언도\n찾아봐!";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 3.0f * speed; void UpdateScript()
    {
        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            ChangeScript();
        }

        if (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    public Sprite normal, closeEye;
    void UpdateFace()
    {
        if (head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye;
            }
        }
        else if (head.sprite == closeEye)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = normal;
            }
        }
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    void Update()
    {
        UpdateScript();
        UpdateFace();
        BackButtonProc();
    }
}
