﻿using MyDesign;
using MyTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CallMain : MonoBehaviour {
    string inputedCode;
    public UnityEngine.UI.Text code;
    public AudioClip btnSound;
    // Use this for initialization
    void Start()
    {
        inputedCode = "";
        code.text = "";
    }
    public void Call()
    {
        if (!FindObjectOfType<OkModal>())
        {
            if (inputedCode.Length == 0)
            {
                Modal.GetInstance().CreateNewOkModal("번호를 입력해야 합니다.", Modal.OkModalType.Call);
                return;
            }
            string msg;
            switch (inputedCode)
            {
                case "111":
                case "113":
                    msg = "내 친구들 중에 간첩은 없을 거야..";
                    break;
                case "112":
                    msg = "내 마음을 훔쳐간 도둑이 있어요!";
                    break;
                case "114":
                    msg = "아직 요금은 충분하다.";
                    break;
                case "110":
                    msg = "학교,여성,가정폭력은 여기다 신고하자.";
                    break;
                case "119":
                    msg = "내 마음이 불타고 있어요!";
                    break;
                case "1388":
                    msg = "청소년 문제는 여기다 신고하자.";
                    break;
                case "1399":
                    msg = "잘못된 식품은 여기다 신고하자.";
                    break;
                case "1541":
                case "1633":
                case "1677":
                case "1682":
                    msg = "내 요금은 아직 충분하다.";
                    break;
                case "01058931099":
                    msg = "전화를 잘 받지 않는다.";
                    break;
                case "17171771":
                    msg = "I luv u 2";
                    break;
                case "8282":
                    GameMain.GetInstance().SpeedChange(!GameMain.GetInstance().skip);
                    if (GameMain.GetInstance().skip) msg = "스킵모드가 활성화 되었습니다.";
                    else msg = "스킵모드가 비활성화 되었습니다.";
                    break;
                case "6794":
                    Modal.GetInstance().CreateEnding(7);
                    msg = "엔딩 컷씬 샘플이 출력되었습니다.";
                    break;
                case "1099":
                    ToolClass.GetCharacter(1).onGameCompetition = true;
                    msg = ToolClass.GetCharacter(1).name + "의 미니게임 이벤트가 발동되었습니다.";
                    break;
                case "2645":
                    foreach(var i in GameMain.GetInstance().mainData.characters)
                    {
                        if (i.charCode == 1) i.level = 3;
                        else i.level = 6;
                    }
                    msg = "모든 캐릭터의 레벨이 최상이 되었습니다.";
                    break;
                case "7272":
                    for(var i = GameMain.GetInstance().lockedApps.Count-1; i>=0; --i)
                    {
                        GameMain.GetInstance().lockedApps[i].SetEnabled(true);
                        ToolClass.MakeNotify(GameMain.GetInstance().lockedApps[i].appType, "비밀 어플리케이션이 개방되었습니다.");
                        GameMain.GetInstance().lockedApps.Remove(GameMain.GetInstance().lockedApps[i]);
                    }
                    msg = "모든 비밀어플 해금 완료.";
                    break;
                case "1818":
                    SceneManager.LoadScene("DeveloperMenu");
                    msg = "비활성화된 명령어 입니다.";
                    break;
                case "9151":
                    SceneManager.LoadScene("DemoScene");
                    msg = "비활성화된 명령어 입니다.";
                    break;
                case "9999":
                    msg = "업적 점수가 20 추가 되었습니다.";
                    GameMain.GetInstance().mainData.achMainData.score += 20;
                    AchMain.GetInstance().Resort();
                    break;
                case "7777":
                    ToolClass.Reset();
                    ToolClass.Save();
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                    msg = "리셋 성공.";
                    break;
                case "0000":
                    ToolClass.CollectPhoto(2, "ar_02");
                    ToolClass.CollectPhoto(2, "ar_03");
                    ToolClass.CollectPhoto(2, "ar_04");
                    ToolClass.CollectPhoto(2, "ar_05");
                    ToolClass.CollectPhoto(2, "ar_06");
                    ToolClass.CollectPhoto(2, "ar_07");
                    ToolClass.CollectPhoto(2, "ar_08");
                    ToolClass.CollectPhoto(2, "ar_09");
                    ToolClass.CollectPhoto(7, "arin_02");
                    ToolClass.CollectPhoto(7, "arin_03");
                    ToolClass.CollectPhoto(7, "arin_04");
                    ToolClass.CollectPhoto(7, "arin_05");
                    ToolClass.CollectPhoto(7, "arin_06");
                    ToolClass.CollectPhoto(7, "arin_07");
                    ToolClass.CollectPhoto(7, "arin_10");
                    ToolClass.CollectPhoto(1, "hb_02");
                    ToolClass.CollectPhoto(1, "hb_03");
                    ToolClass.CollectPhoto(1, "hb_04");
                    ToolClass.CollectPhoto(1, "hb_05");
                    ToolClass.CollectPhoto(1, "hb_06");
                    ToolClass.CollectPhoto(1, "hb_07");
                    ToolClass.CollectPhoto(1, "hb_08");
                    ToolClass.CollectPhoto(3, "su_02");
                    ToolClass.CollectPhoto(3, "su_03");
                    ToolClass.CollectPhoto(3, "su_04");
                    ToolClass.CollectPhoto(3, "su_05");
                    ToolClass.CollectPhoto(3, "su_06");
                    msg = "아린 캐릭터에 모든 CG가 추가되었습니다.";
                    break;
                default:
                    msg = inputedCode + "는 모르는 번호 입니다.";
                    break;
            }
            Modal.GetInstance().CreateNewOkModal(msg, Modal.OkModalType.Call);
            inputedCode = "";
            code.text = "";
        }
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }

    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
    public void DeleteString()
    {
        ToolClass.PlaySound(btnSound);
        if (inputedCode.Length >= 1)
        {
            inputedCode = inputedCode.Substring(0, inputedCode.Length - 1);
            code.text = inputedCode;
        }
    }
    public void InputString(string str)
    {
        ToolClass.PlaySound(btnSound);
        if (inputedCode.Length >= 11) return;
        inputedCode += str;
        code.text = inputedCode;
    }
}
