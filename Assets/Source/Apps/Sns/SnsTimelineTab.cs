﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;
using UnityEngine.UI;

public class SnsTimelineTab : MonoBehaviour
{
    public GameObject postPrefab;
    public Transform addTargetList;
    [HideInInspector] public List<GameObject> posts;
    public void Resort()
    {
        if (posts == null) posts = new List<GameObject>();
        transform.Find("TimelineViewport").Find("TimelineList").GetComponent<ContentSizeFitter>().enabled = false;

        foreach (GameObject post in posts) Destroy(post);
        posts.Clear();

        GameMain gm = GameMain.GetInstance();
        MainData md = gm.mainData;
        var snsLogs = gm.mainData.snsLogs;

        for (int i = snsLogs.Count - 1; i >= 0; --i)
        {
            SnsLog snsLog = snsLogs[i];
            SnsPostData targetData = ToolClass.GetSnsPost(snsLog.postCode);
            GameObject obj = Instantiate(postPrefab, addTargetList);
            obj.GetComponent<SnsPost>().Init(targetData.postCode, targetData.charCode, targetData.picCode, snsLog.like, targetData.content, snsLog.comments, snsLog.playerLiked);
            posts.Add(obj);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform.Find("TimelineViewport").Find("TimelineList"));
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform.Find("TimelineViewport").Find("TimelineList"));
        transform.Find("TimelineViewport").Find("TimelineList").GetComponent<ContentSizeFitter>().enabled = true;
        //LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform.Find("TimelineViewport").Find("TimelineList"));
        //transform.Find("TimelineViewport").Find("TimelineList").GetComponent<ContentSizeFitter>().enabled = !(transform.Find("TimelineViewport").Find("TimelineList").GetComponent<ContentSizeFitter>().enabled);
        //transform.Find("TimelineViewport").Find("TimelineList").GetComponent<ContentSizeFitter>().enabled = !(transform.Find("TimelineViewport").Find("TimelineList").GetComponent<ContentSizeFitter>().enabled);
        //Invoke("ResetVertical", 0.00001f);
    }
    public void Test()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform.Find("TimelineViewport").Find("TimelineList"));
    }
    void ResetVertical()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform.Find("TimelineViewport").Find("TimelineList"));
    }
}
