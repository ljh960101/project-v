﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using MyDesign;
using MyTool;

public class SnsPost : MonoBehaviour
{
    int charCode;
    int postCode;
    [HideInInspector] public SnsPostData spd;
    [HideInInspector] public SnsLog sl;
    [HideInInspector] public PostScript currentSelectScript;
    public GameObject addComentList, commentPrefab;
    string[] selectList;
    int selectScriptCode;
    public void Init(int postcode, int charcode, string picCode, int like, string content, List<string> comments, bool playerLiked)
    {
        spd = ToolClass.GetSnsPost(postcode);
        sl = ToolClass.GetSnsPostLog(postcode);
        this.charCode = charcode;
        this.postCode = postCode;

        MyCharacter character = ToolClass.GetCharacter(charcode);
        this.name = postcode + "";
        transform.Find("TopBar").Find("MaskImage").Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curSnsPic);
        transform.Find("TopBar").Find("ID").GetComponent<UnityEngine.UI.Text>().text = character.snsId;
        transform.Find("Image").GetComponent<PhotoScript>().SetImageByCode(picCode);
        transform.Find("MiddleBar").Find("text_like").GetComponent<UnityEngine.UI.Text>().text = "<b>좋아요 " + like + "개</b>";
        transform.Find("MiddleBar").Find("text_content").GetComponent<UnityEngine.UI.Text>().text = content;
        transform.Find("TopBar").Find("ID").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { SnsMain.GetInstance().OpenPost(spd.postCode); });

        // 선택지 처리
        transform.Find("MiddleBar").Find("Buttons").Find("Btn_Say").GetComponent<UnityEngine.UI.Button>().interactable = false;
        var postScripts = spd.postScripts;
        foreach (PostScript script in postScripts)
        {
            ScriptParsedData scriptParedData;
            try
            {
                scriptParedData = ToolClass.ParseScript(script.scripts[script.currentPos]);
            }
            catch
            {
                Modal.GetInstance().CreateNewOkModal(spd.postCode + " " + postScripts.IndexOf(script) + " " + script.currentPos + " 스크립트 오류 ");
                return;
            }
            if (scriptParedData.type == ScriptType.SELECT_START)
            {
                currentSelectScript = script;
                transform.Find("MiddleBar").Find("Buttons").Find("Btn_Say").Find("Image").gameObject.SetActive(true);
                transform.Find("MiddleBar").Find("Buttons").Find("Btn_Say").GetComponent<UnityEngine.UI.Button>().interactable = true;
                transform.Find("MiddleBar").Find("Buttons").Find("Btn_Say").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(OnSay);
                selectList = scriptParedData.val.Split('$');
                selectScriptCode = postScripts.IndexOf(script);
                break;
            }
        }
        transform.Find("MiddleBar").Find("Buttons").Find("Btn_Like").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(OnLike);
        if(sl.playerLiked) transform.Find("MiddleBar").Find("Buttons").Find("Btn_Like").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/post/like_act");
        foreach (string comment in comments)
        {
            GameObject newObj = Instantiate(commentPrefab, addComentList.transform);
            newObj.GetComponent<UnityEngine.UI.Text>().text = comment;

            // comment에서 채팅 대상과 태그 대상을 읽어냄.
            string myText = "", targetText = "";
            int i = 0;
            for (; i<comment.Length; ++i)
            {
                if(comment[i] == ' ')
                {
                    myText = comment.Substring(0, i);
                    ++i;
                    break;
                }
            }
            int temp = i;
            if (comment[i] == '@')
            {
                for (; i < comment.Length; ++i)
                {
                    if (comment[i] == ' ')
                    {
                        targetText = comment.Substring(temp, i-temp);
                        break;
                    }
                }
            }
            myText = myText.Replace("<b>", "");
            myText = myText.Replace("</b>", "");
            myText = myText.Replace("@", "");
            targetText = targetText.Replace("<b>", "");
            targetText = targetText.Replace("</b>", "");
            targetText = targetText.Replace("@", "");
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((eventData) => { OnCommentPushed(eventData, myText, targetText); });
            newObj.GetComponent<EventTrigger>().triggers.Add(entry);
        }

        //LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform);
        //gameObject.GetComponent<UnityEngine.UI.ContentSizeFitter>().enabled = false;
        //Invoke("ResetVertical", 0.1f);
    }
    void ResetVertical()
    {
        gameObject.GetComponent<UnityEngine.UI.ContentSizeFitter>().enabled = true;
    }
    const float fontSize = 5.2f;
    const float defalutGab = 10f;
    void OnCommentPushed(BaseEventData eventData, string from, string to)
    {
        PointerEventData ped = eventData as PointerEventData;
        MainData md = GameMain.GetInstance().mainData;
        if ((ped.position.x >= defalutGab && ped.position.x <= defalutGab + fontSize * from.Length))
        {
            foreach (var character in md.characters)
            {
                if(character.snsId == from)
                {
                    SnsMain.GetInstance().OpenProfile(character.charCode);
                    return;
                }
            }
        }
        else if(ped.position.x >= defalutGab + fontSize * (from.Length + 1) && ped.position.x <= defalutGab + fontSize * (from.Length + 1) + fontSize * from.Length)
        {
            foreach (var character in md.characters)
            {
                if (character.snsId == to)
                {
                    SnsMain.GetInstance().OpenProfile(character.charCode);
                    return;
                }
            }
        }
    }
    public void OnSelectPush(string selectName)
    {
        // 해당 리스트로 찾아간다.
        PostScript ps = spd.postScripts[selectScriptCode];
        for (int i = ps.currentPos+ 1; i < ps.scripts.Count; ++i)
        {
            ScriptParsedData scriptData = ToolClass.ParseScript(ps.scripts[i]);
            if (scriptData.type == ScriptType.SELECT_LIST && scriptData.val == selectName)
            {
                ps.currentPos = i + 1;
                ps.currentProcess = 0f;
                SnsMain.GetInstance().Resort();
                break;
            }
        }
    }
    public void OnSay()
    {
        var mainData = GameMain.GetInstance().mainData;
        foreach (var alram in mainData.snsAlrams)
        {
            if (alram.postCode == postCode)
            {
                alram.onAlram = false;
            }
        }

        Modal.GetInstance().CreateSnsSelect(selectList, this);
    }
    public void OnLike()
    {
        var mainData = GameMain.GetInstance().mainData;
        foreach (var alram in mainData.snsAlrams)
        {
            if (alram.postCode == postCode)
            {
                alram.onAlram = false;
            }
        }

        if (sl.playerLiked)
        {
            --sl.like;
            sl.playerLiked = false;
            transform.Find("MiddleBar").Find("Buttons").Find("Btn_Like").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/post/like");
        }
        else
        {
            ++sl.like;
            sl.playerLiked = true;
            transform.Find("MiddleBar").Find("Buttons").Find("Btn_Like").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/post/like_act");
        }
        transform.Find("MiddleBar").Find("text_like").GetComponent<UnityEngine.UI.Text>().text = "<b>좋아요 " + sl.like + "개</b>";
    }
}