﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using MyDesign;
using MyTool;

public class SnsProfileTab : MonoBehaviour {
    public GameObject snsProfilePrefab, postImagePrefab;
    public Transform addTarget;
    int currentCharCode = -1;
    public void Init(int charCode)
    {
        // 리스트에 뭔가 있으면 다 지우셈
        for (int i = addTarget.childCount - 1; i >= 0; --i)
        {
            Destroy(addTarget.GetChild(i).gameObject);
        }
        currentCharCode = charCode;

        MyCharacter character = ToolClass.GetCharacter(charCode);

        // 만들어주긔
        GameObject newObj = Instantiate(snsProfilePrefab, addTarget);
        newObj.name = "SnsProfileContent";
        newObj.transform.Find("Top").Find("MaskImage").Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curSnsPic);
        newObj.transform.Find("Top").Find("Follower").GetComponent<UnityEngine.UI.Text>().text = character.snsFollwer + "";
        newObj.transform.Find("Top").Find("Following").GetComponent<UnityEngine.UI.Text>().text = character.snsFollow + "";
        newObj.transform.Find("Middle").Find("ProfileMsg").GetComponent<UnityEngine.UI.Text>().text = character.snsProfileMsg;
        newObj.transform.Find("Top").Find("Btn_Follow").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(OnClickFollow);
        if(character.bOnSns) newObj.transform.Find("Top").Find("Btn_Follow").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/chara_profile/following");

        GameMain gm = GameMain.GetInstance();
        MainData md = gm.mainData;
        foreach (SnsLog snsLog in md.snsLogs)
        {
            SnsPostData targetData = ToolClass.GetSnsPost(snsLog.postCode);
            if (targetData.charCode == charCode)
            {
                GameObject newImage = Instantiate(postImagePrefab, newObj.transform.Find("PostImages"));
                newImage.transform.Find("MaskImage").GetComponent<PhotoScript>().SetImageByCode(targetData.picCode);
                newImage.transform.Find("MaskImage").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnClickPostImage(targetData.postCode); });
            }
        }

        newObj.GetComponent<UnityEngine.UI.VerticalLayoutGroup>().enabled = false;
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform);
        newObj.GetComponent<UnityEngine.UI.VerticalLayoutGroup>().enabled = true;
        Invoke("SetVerticalGroup", 0.1f);
    }
    public void SetVerticalGroup()
    {
        addTarget.GetComponent<UnityEngine.UI.VerticalLayoutGroup>().enabled = false;
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)addTarget);
        addTarget.GetComponent<UnityEngine.UI.VerticalLayoutGroup>().enabled = true;
    }
    public void OnClickFollow()
    {
        if (!ToolClass.GetCharacter(currentCharCode).bOnSns)
        {
            ToolClass.GetCharacter(currentCharCode).bOnSns = true;
            ToolClass.GetCharacter(currentCharCode).bOpenSns = false;
            Modal.GetInstance().CreateNewOkModal("팔로우 성공!");
            addTarget.Find("SnsProfileContent").Find("Top").Find("Btn_Follow").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/chara_profile/following");
        }
        else
            Modal.GetInstance().CreateNewOkModal("계속 친구로 남고 싶어..");
    }
    public void OnClickPostImage(int postcode)
    {
        SnsMain.GetInstance().OpenPost(postcode);
    }
}
