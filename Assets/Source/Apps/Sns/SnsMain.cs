﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;
using UnityEngine.UI;

public class SnsMain : MonoBehaviour {
    public enum ESnsScreenType
    {
        Main = 0,
        Post,
        Profile
    };
    ESnsScreenType currentScreen;
    [HideInInspector] public SwipeTabs st;
    List<UnityEngine.UI.ScrollRect> fuckingScrolls;
    public GameObject tap_main, tap_post, tap_profile, snsAlramPanel;
    SnsProfileTab spt;
    public Transform alramTargetList;
    public void MakeLog(int postCode)
    {
        MakeLog(ToolClass.GetSnsPost(postCode));
    }
    public void AddComment(int postCode, string comment, string sayChar, string targetChar)
    {
        SnsLog sl = ToolClass.GetSnsPostLog(postCode);
        SnsPostData spd = ToolClass.GetSnsPost(postCode);

        if (targetChar != "") targetChar = "<b>" + targetChar + "</b>";
        if (sayChar != "0")
        {
            // 외부인
            if(sayChar[0] == '@')
            {
                sl.comments.Add("<b>" + sayChar.Substring(1) + "</b> " + targetChar + comment);
            }
            // 친구
            else
            {
                int targetCharCode = int.Parse(sayChar);
                if (targetChar.Contains(GameMain.GetInstance().mainData.snsId))
                {
                    SnsAlram newAlram = new SnsAlram();
                    newAlram.charCode = targetCharCode;
                    newAlram.msg = "<b>" + ToolClass.GetCharacter(targetCharCode).snsId + "</b>님이 덧글을 달았습니다.";
                    newAlram.onAlram = true;
                    newAlram.postCode = postCode;
                    GameMain.GetInstance().mainData.snsAlrams.Add(newAlram);
                    ToolClass.MakeNotify(AppType.Sns, newAlram.msg, postCode);
                }


                sl.comments.Add("<b>" + ToolClass.GetCharacter(targetCharCode).snsId + "</b> " + targetChar + comment);
            }
        }
        // 주인공
        else
            sl.comments.Add("<b>" + GameMain.GetInstance().mainData.snsId + "</b> " + targetChar + comment);

        Resort();
    }
    public void MakeLog(SnsPostData post)
    {
        MyCharacter character = ToolClass.GetCharacter(post.charCode);
        SnsLog newLog = new SnsLog();
        newLog.comments = new List<string>();
        newLog.like = post.defaulatLike;
        newLog.playerLiked = false;
        newLog.postCode = post.postCode;
        post.isOver = false;
        post.isPosted = true;
        GameMain.GetInstance().mainData.snsLogs.Add(newLog);
        SnsAlram newAlram = new SnsAlram();
        newAlram.charCode = post.charCode;
        newAlram.msg = "<b>" + character.snsId + "</b>님이 사진을 올렸습니다.";
        newAlram.onAlram = true;
        newAlram.postCode = post.postCode;
        GameMain.GetInstance().mainData.snsAlrams.Add(newAlram);
        ToolClass.MakeNotify(AppType.Sns, newAlram.msg, post.postCode);
        Resort();
    }
    public void TapChanged(int tabNubmer)
    {
        switch (tabNubmer)
        {
            case 0:
                transform.Find("downBar").Find("UnderMenu").Find("Button1").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/main");
                transform.Find("downBar").Find("UnderMenu").Find("Button2").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/search_disable");
                transform.Find("downBar").Find("UnderMenu").Find("Button3").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/alarm_disable");
                break;
            case 1:
                transform.Find("downBar").Find("UnderMenu").Find("Button1").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/main_disable");
                transform.Find("downBar").Find("UnderMenu").Find("Button2").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/search");
                transform.Find("downBar").Find("UnderMenu").Find("Button3").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/alarm_disable");
                break;
            case 2:
                transform.Find("downBar").Find("UnderMenu").Find("Button1").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/main_disable");
                transform.Find("downBar").Find("UnderMenu").Find("Button2").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/search_disable");
                transform.Find("downBar").Find("UnderMenu").Find("Button3").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("UI/sns/tab_icon/alarm");
                break;
        }
    }
    int currentProfileTabCharcode;
    public void OpenProfile(int charCode)
    {
        currentProfileTabCharcode = charCode;
        if (spt == null)
        {
            spt = tap_profile.GetComponent<SnsProfileTab>();
        }
        spt.Init(charCode);
        if(currentScreen!=ESnsScreenType.Profile) ChangeScreen(ESnsScreenType.Profile);
    }
    List<GameObject> alrams;
    public GameObject alramTabalram;
    int noPushedalramCount = 0;
    public void RefreshAlram()
    {
        MainData md = GameMain.GetInstance().mainData;

        if (alrams == null) alrams = new List<GameObject>();

        for(int i = alrams.Count - 1; i >= 0; --i)
            Destroy(alrams[i]);
        alrams.Clear();

        noPushedalramCount = 0;
        alramTabalram.SetActive(false);
        for(int i=md.snsAlrams.Count-1; i>=0; --i)
        {
            var alram = md.snsAlrams[i];

            GameObject newObj = Instantiate(snsAlramPanel, alramTargetList);
            newObj.transform.Find("MaskImage").Find("CircularPhoto").GetComponent<CircularPhoto>().Init(ToolClass.GetSnsPost(alram.postCode).picCode);
            newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = alram.msg;

            UnityEngine.UI.ColorBlock cb = newObj.GetComponent<UnityEngine.UI.Button>().colors;
            if (alram.onAlram)
            {
                ++noPushedalramCount;
                alramTabalram.SetActive(true);
                cb.normalColor = new Color(1f, 1f, 1f);
                cb.highlightedColor = new Color(1f, 1f, 1f);
                cb.pressedColor = new Color(1f, 1f, 1f);
                cb.disabledColor = new Color(1f, 1f, 1f);
            }
            else
            {
                cb.normalColor = new Color(0.7f, 0.7f, 0.7f);
                cb.highlightedColor = new Color(0.7f, 0.7f, 0.7f);
                cb.pressedColor = new Color(0.7f, 0.7f, 0.7f);
                cb.disabledColor = new Color(0.7f, 0.7f, 0.7f);
            }
            newObj.GetComponent<UnityEngine.UI.Button>().colors = cb;

            newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
                delegate
                {
                    UnityEngine.UI.ColorBlock cb2 = newObj.GetComponent<UnityEngine.UI.Button>().colors;
                    cb2.normalColor = new Color(0.7f, 0.7f, 0.7f);
                    cb2.highlightedColor = new Color(0.7f, 0.7f, 0.7f);
                    cb2.pressedColor = new Color(0.7f, 0.7f, 0.7f);
                    cb.disabledColor = new Color(0.7f, 0.7f, 0.7f);
                    newObj.GetComponent<UnityEngine.UI.Button>().colors = cb2;

                    alram.onAlram = false;
                    OpenPost(alram.postCode);

                    noPushedalramCount = 0;
                    bool chk = false;
                    foreach (SnsAlram myAlram in md.snsAlrams)
                    {
                        if (myAlram.onAlram)
                        {
                            ++noPushedalramCount;
                            chk = true;
                        }
                    }
                    if (!chk) alramTabalram.SetActive(false);
                    am.SetAlram(noPushedalramCount);
                });
            alrams.Add(newObj);
        }
        am.SetAlram(noPushedalramCount);
    }
    public Transform postTarget;
    public GameObject postPrefab;
    int currentPostTabPostCode;
    public void OpenPost(int postCode)
    {
        var mainData = GameMain.GetInstance().mainData;
        foreach (var alram in mainData.snsAlrams)
        {
            if(alram.postCode == postCode)
            {
                alram.onAlram = false;
            }
        }

        postTarget.GetComponent<ContentSizeFitter>().enabled = false;
        currentPostTabPostCode = postCode;
        for(int i=postTarget.childCount-1; i>=0; --i)
        {
            Destroy(postTarget.GetChild(i).gameObject);
        }
        MainData md = GameMain.GetInstance().mainData;
        SnsPostData targetData = ToolClass.GetSnsPost(postCode);
        foreach (SnsLog snsLog in md.snsLogs)
        {
            if(snsLog.postCode == postCode)
            {
                GameObject obj = Instantiate(postPrefab, postTarget);
                obj.GetComponent<SnsPost>().Init(targetData.postCode, targetData.charCode, targetData.picCode, snsLog.like, targetData.content, snsLog.comments, snsLog.playerLiked);
                if (currentScreen != ESnsScreenType.Post) ChangeScreen(ESnsScreenType.Post);
                LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)postTarget);
                LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)postTarget);
                postTarget.GetComponent<ContentSizeFitter>().enabled = true;
                //Invoke("ResetVertical", 0.3f);
                return;
            }
        }
        Modal.GetInstance().CreateNewOkModal("존재하지 않는 포스트 코드");
    }
    void ResetVertical()
    {
        postTarget.GetComponent<UnityEngine.UI.ContentSizeFitter>().enabled = true;
    }
    public void ChangeScreen(int target)
    {
        ChangeScreen((ESnsScreenType)target);
    }
    public void Close()
    {
        foreach (UnityEngine.UI.ScrollRect sr in fuckingScrolls)
            sr.enabled = false;
        am.Close();
    }

    public void ChangeScreen(ESnsScreenType targetScreen)
    {
        switch (targetScreen)
        {
            case ESnsScreenType.Main:
                if (!tap_main.activeSelf)
                {
                    tap_main.GetComponent<FadeOutSystem>().FadeIn();
                }
                if (tap_post.activeSelf)
                {
                    tap_post.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromTop);
                    tap_post.GetComponent<FadeOutSystem>().FadeOut();
                }
                if (tap_profile.activeSelf)
                {
                    tap_profile.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromTop);
                    tap_profile.GetComponent<FadeOutSystem>().FadeOut();
                }
                break;
            case ESnsScreenType.Post:
                if (tap_main.activeSelf)
                {
                    tap_main.GetComponent<FadeOutSystem>().FadeOut();
                }
                if (!tap_post.activeSelf)
                {
                    tap_post.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromTop);
                    tap_post.GetComponent<FadeOutSystem>().FadeIn();
                }
                if (tap_profile.activeSelf)
                {
                    tap_profile.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromTop);
                    tap_profile.GetComponent<FadeOutSystem>().FadeOut();
                }
                break;
            case ESnsScreenType.Profile:
                if (tap_main.activeSelf)
                {
                    tap_main.GetComponent<FadeOutSystem>().FadeOut();
                }
                if (tap_post.activeSelf)
                {
                    tap_main.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromTop);
                    tap_post.GetComponent<FadeOutSystem>().FadeOut();
                }
                if (!tap_profile.activeSelf)
                {
                    tap_profile.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromTop);
                    tap_profile.GetComponent<FadeOutSystem>().FadeIn();
                }
                break;
        }
        if (targetScreen != currentScreen) Resort();
        currentScreen = targetScreen;
    }
    public void Reload()
    {
        Transform cover = gameObject.transform.Find("Cover");
        if (cover)
        {
            cover.gameObject.SetActive(true);
            cover.GetComponent<FadeOutSystem>().Init(2, true, 3f, false);
            cover.GetComponent<FadeOutSystem>().FadeOut();
        }
        gameObject.GetComponent<FadeOutSystem>().FadeIn(() =>
        {
            foreach(UnityEngine.UI.ScrollRect sr in fuckingScrolls)
                sr.enabled = true;
        });
        Resort();
    }
    SnsFindTab sft;
    SnsTimelineTab stt;
    public void Resort()
    {
        if (!sft) sft = transform.Find("Tap_Main").Find("MainMenus").Find("Find").GetComponent<SnsFindTab>();
        if (!stt) stt = transform.Find("Tap_Main").Find("MainMenus").Find("Timeline").GetComponent<SnsTimelineTab>();
        sft.Resort();
        stt.Resort();
        if (currentScreen == ESnsScreenType.Post)
        {
            OpenPost(currentPostTabPostCode);
        }
        else if (currentScreen == ESnsScreenType.Profile)
        {
            OpenProfile(currentProfileTabCharcode);
        }
        RefreshAlram();
    }
    public AppMain am;
    GameMain gm;
    public void FindScrollRects(Transform myTransform)
    {
        UnityEngine.UI.ScrollRect sr = myTransform.GetComponent<UnityEngine.UI.ScrollRect>();
        if (sr)
        {
            fuckingScrolls.Add(sr);
            sr.enabled = false;
        }
        foreach (Transform child in myTransform)
        {
            FindScrollRects(child);
        }
    }
    public void Init()
    {
        am = GetComponent<AppMain>();
        gm = GameMain.GetInstance();

        tap_main.GetComponent<FadeOutSystem>().Init(2f, false, 1f, false, true);
        tap_post.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        tap_profile.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);

        fuckingScrolls = new List<UnityEngine.UI.ScrollRect>();
        foreach (Transform child in transform)
        {
            FindScrollRects(child);
        }
        
        st = tap_main.GetComponent<SwipeTabs>();
        currentScreen = ESnsScreenType.Main;
        ChangeScreen(ESnsScreenType.Main);
        st.transform.Find("Scrollbar").GetComponent<UnityEngine.UI.Scrollbar>().value = 0f;
    }
    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                switch (currentScreen)
                {
                    case ESnsScreenType.Main:
                        Close();
                        break;
                    case ESnsScreenType.Post:
                        ChangeScreen(ESnsScreenType.Main);
                        break;
                    case ESnsScreenType.Profile:
                        ChangeScreen(ESnsScreenType.Main);
                        break;
                }
                backButtonTime = 0.0f;
            }
        }

    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
    private void Update()
    {
        const int SCREEN_HEIGHT = 1500;
        const int SCREEN_UPPER_OFFSET = 1000;
        
        if (currentScreen == ESnsScreenType.Main)
        {
            int screen = st.GetScreen();
            {
                if (screen == -1 || screen == 0)
                {
                    for (int k = 0; k < stt.posts.Count; ++k)
                        stt.posts[k].SetActive(true);
                }
                else
                {
                    for (int k = 0; k < stt.posts.Count; ++k)
                        stt.posts[k].SetActive(false);
                }
            }

            // Alram
            {
                if (screen == 0 || screen == 1)
                {
                    for (int k = 0; k < alrams.Count; ++k)
                        alrams[k].SetActive(true);
                }
                else
                {
                    for (int k = 0; k < alrams.Count; ++k)
                        alrams[k].SetActive(false);
                }
            }
        }
    }
    private static SnsMain instance;
    public static SnsMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("SnsMain").GetComponent<SnsMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
}
