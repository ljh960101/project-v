﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyDesign;

public class SnsFindTab : MonoBehaviour {
    public GameObject addFriendPanel, listTop;
    public Transform addTargetList;
    public GameObject alram;
    [HideInInspector] public List<GameObject> panels;
    public void Resort()
    {
        if (panels == null) panels = new List<GameObject>();

        foreach (GameObject panel in panels) Destroy(panel);
        panels.Clear();

        GameMain gm = GameMain.GetInstance();
        MainData md = gm.mainData;

        bool chk = false;
        foreach(MyCharacter character in md.characters)
        {
            // SNS를 오픈했지만 추가가 안되있음.
            if(character.bOpenSns && !character.bOnSns)
            {
                if (!chk)
                {
                    GameObject newObj2 = Instantiate(listTop, addTargetList);
                    newObj2.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "추천 친구";
                    panels.Add(newObj2);
                    chk = true;
                    alram.SetActive(true);
                }

                GameObject newObj = Instantiate(addFriendPanel, addTargetList);
                newObj.name = character.charCode + "";
                newObj.transform.Find("MaskImage").Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curSnsPic);
                newObj.transform.Find("ID").GetComponent<UnityEngine.UI.Text>().text = character.snsId;
                newObj.transform.Find("Msg").GetComponent<UnityEngine.UI.Text>().text = character.name;
                newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate {SnsMain.GetInstance().OpenProfile(int.Parse(newObj.name)); });
                panels.Add(newObj);
            }
        }
        if (!chk)
        {
            alram.SetActive(false);
        }

        {
            GameObject newObj2 = Instantiate(listTop, addTargetList);
            newObj2.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = "친구 목록";
            panels.Add(newObj2);
        }

        foreach (MyCharacter character in md.characters)
        {
            if (character.bOnSns)
            {
                GameObject newObj = Instantiate(addFriendPanel, addTargetList);
                newObj.name = character.charCode + "";
                newObj.transform.Find("MaskImage").Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curSnsPic);
                newObj.transform.Find("ID").GetComponent<UnityEngine.UI.Text>().text = character.snsId;
                newObj.transform.Find("Msg").GetComponent<UnityEngine.UI.Text>().text = character.snsProfileMsg;
                newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { SnsMain.GetInstance().OpenProfile(int.Parse(newObj.name)); });
                panels.Add(newObj);
            }
        }
    }
}
