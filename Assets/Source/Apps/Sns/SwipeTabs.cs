﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeTabs : MonoBehaviour {
    [HideInInspector] public UnityEngine.UI.Scrollbar sc;
    UnityEngine.UI.ScrollRect sr;
    int screen = 0;
    float swipeTo = -1;

    public int GetScreen() { return screen; }
    void Start()
    {
        sr = GetComponent<UnityEngine.UI.ScrollRect>();
        sc = transform.Find("Scrollbar").GetComponent<UnityEngine.UI.Scrollbar>();
    }
    // 자동 스와이핑 해줌
    public void SwipeTo(int target)
    {
        if (!sc)
        {
            sr = GetComponent<UnityEngine.UI.ScrollRect>();
            sc = transform.Find("Scrollbar").GetComponent<UnityEngine.UI.Scrollbar>();
        }
        float a = (float)target;
        swipeTo = a * 0.5f;
    }
    bool click = false;
    void Update()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        if (Input.touchCount >= 1) click = true;
        else click = false;
#else
        if (Input.GetMouseButtonDown(0)) click = true;
        else if (Input.GetMouseButtonUp(0)) click = false;
#endif

        //if (click) swipeTo = -1f;

        if (swipeTo!=-1 && sr && sr.enabled)
        {
            sc.value += (swipeTo - sc.value) / 4f;
            if (Mathf.Abs(swipeTo - sc.value)<=0.1) swipeTo = -1;
        }

        if (sc.value >= 0.8f)
        {
            screen = 1;
            if(swipeTo==-1) SnsMain.GetInstance().TapChanged(2);
        }
        else if (sc.value <= 0.2f)
        {
            screen = -1;
            if (swipeTo == -1) SnsMain.GetInstance().TapChanged(0);
        }
        else if (sc.value > 0.4f && sc.value < 0.6f)
        {
            screen = 0;
            if (swipeTo == -1) SnsMain.GetInstance().TapChanged(1);
        }
        if (!click && swipeTo==-1)
        {
            switch (screen)
            {
                case -1:
                    if (sc.value <= 0.2f)
                        sc.value += (0f - sc.value) / 5f;
                    else
                        sc.value += (0.5f - sc.value) / 4f;
                    break;
                case 0:
                    if (sc.value > 0.4f && sc.value < 0.6f)
                        sc.value += (0.5f - sc.value) / 4f;
                    else if (sc.value <= 0.4f)
                        sc.value += (0f - sc.value) / 4f;
                    else
                        sc.value += (1f - sc.value) / 4f;
                    break;
                case 1:
                    if (sc.value >= 0.8f)
                        sc.value += (1f - sc.value) / 4f;
                    else
                        sc.value += (0.5f - sc.value) / 4f;

                    break;
            }
        }
    }
}
