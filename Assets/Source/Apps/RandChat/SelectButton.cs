﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectButton : MonoBehaviour {

    // Use this for initialization
    RandChatMain rm;
	void Start () {
        rm = RandChatMain.GetInstance();
        UnityEngine.UI.Button btn = GetComponent<UnityEngine.UI.Button>();
        btn.onClick.AddListener(TaskOnClick);
    }
    void TaskOnClick()
    {
        rm.SelectComplete(this.name);
    }
}
