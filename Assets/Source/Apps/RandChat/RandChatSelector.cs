﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandChatSelector : MonoBehaviour {
    int selectCode;
    UnityEngine.UI.Text text;
    RandChatMain rcm;

    public void Init(string text, int code)
    {
        rcm = RandChatMain.GetInstance();
        this.text = transform.Find("Text").GetComponent<UnityEngine.UI.Text>();
        this.text.text = text;
        selectCode = code;
    }
}
