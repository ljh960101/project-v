﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using MyDesign;
using MyTool;

public class RandChatMain : MonoBehaviour
{
    public GameObject chatPanelPrefab;
    public GameObject targetToChatList;

    [HideInInspector] public AppMain am;
    GameMain gm;
    GameObject Tab_matching, Tab_ChatList, Tab_Chat;
    [HideInInspector] public ERandomChatScreen currentScreen;
    List<RandChat_ChatPanel> chatPanels;
    GameObject lastChatPanel;
    ChatLog clickedChatLog;

    // Chat Move Proc
    bool onMove;
    bool onAutoScroll;
    Vector2 lastTouch;

    public void Reload()
    {
        Transform cover = gameObject.transform.Find("Cover");
        if (cover)
        {
            cover.gameObject.SetActive(true);
            cover.GetComponent<FadeOutSystem>().Init(2, true, 3f, false);
            cover.GetComponent<FadeOutSystem>().FadeOut();
        }
        Resort();
    }
    public void AddToLog(int charCode, string script)
    {
        MainData mainData = GameMain.GetInstance().mainData;
        foreach (ChatLog log in mainData.randomChatLogs)
        {
            if (log.charCode == charCode && !log.isDead)
            {

                BeforeChat bc = new BeforeChat();
                bc.chat = script;
                bc.date = DateTime.Now;
                log.beforeChats.Add(bc);
                string smallScript = ToolClass.ParseToSmallScript(script);
                if (smallScript != "")
                {
                    ToolClass.MakeNotify(AppType.RandomChat, "낮선상대: " + smallScript, charCode);
                    AddAlram(charCode, smallScript);
                }
                break;
            }
        }
        Resort();

    }
    // 채팅 종료
    public void ExitChat()
    {
        // 챗로그 삭제
        foreach(ChatLog cl in gm.mainData.randomChatLogs)
        {
            if(cl == clickedChatLog)
            {
                // 진행중이었던 톡방이면 초기화 시킴
                if (!cl.isDead)
                {
                    // 캐릭터 초기화
                    MyCharacter charcater = ToolClass.GetCharacter(chattingTargetCharCode);
                    if (ToolClass.GetCharacterChatType(chattingTargetCharCode) == ChatType.RANDCHAT)
                    {
                        charcater.currentScriptCode = 0;
                        charcater.currentScriptPosition = 0;
                        charcater.currentScriptProcess = 0;
                        charcater.love = 0;
                        charcater.level = 0;
                        charcater.onOverlapScript = false;
                        charcater.scriptPriority = 1;
                        charcater.scriptCheck.Clear();
                    }
                    charcater.bOnRand = false;
                }
                gm.mainData.randomChatLogs.Remove(cl);
                break;
            }
        }
        ChangeScreen(ERandomChatScreen.CHATLIST);
    }
    // 캐릭터가 채팅을 나감
    public void ExitChatByChar(int charcode)
    {
        foreach (ChatLog cl in gm.mainData.randomChatLogs)
        {
            if (cl.charCode == charcode && !cl.isDead)
            {
                cl.isDead = true;
                break;
            }
        }
        MyCharacter charcater = ToolClass.GetCharacter(charcode);
        if (ToolClass.GetCharacterChatType(charcode) == ChatType.RANDCHAT)
        {
            charcater.currentScriptCode = 0;
            charcater.currentScriptPosition = 0;
            charcater.currentScriptProcess = 0;
            charcater.bOnRand = false;
            charcater.love = 0;
            charcater.level = 0;
            charcater.onOverlapScript = false;
            charcater.scriptPriority = 1;
            charcater.scriptCheck.Clear();
        }
        Resort();
    }

    public void ScrollMoved()
    {
        float val = chat_panel.parent.Find("Scrollbar").GetComponent<Scrollbar>().value;
        if (val <= 0.01) TurnOnAutoScroll(true);
        else onAutoScroll = false;
    }
    public void AddAlram(int charCode, string text)
    {
        // 현재 채팅중이면 걍 씹음
        if (gm.currentApp == gameObject && (chattingTargetCharCode == charCode && currentScreen == ERandomChatScreen.CHAT)) return;
        if (gm.currentApp == gameObject) Modal.GetInstance().CreateRCAlram(text, charCode);

        // beforeLog에 알람 추가.
        foreach (ChatLog log in gm.mainData.randomChatLogs)
        {
            if(log.charCode == charCode)
            {
                ++log.alram;
                break;
            }
        }
        RefreshHomeAlram();
    }

    public enum ERandomChatScreen
    {
        MATCHING = 0,
        CHATLIST = 1,
        CHAT = 2
    }
    public void Init()
    {
        am = GetComponent<AppMain>();
        gm = GameMain.GetInstance();

        backButtonTime = 0.0f;
        Tab_matching = transform.Find("Matching").gameObject;
        Tab_ChatList = transform.Find("ChatList").gameObject;
        Tab_Chat = transform.Find("Chat").gameObject;
        selectPanel = transform.Find("Chat").Find("LowerBar").Find("SelectPanel").gameObject;
        lowerBar = transform.Find("Chat").Find("LowerBar").gameObject;
        typeText = lowerBar.transform.Find("TypeText").GetComponent<UnityEngine.UI.Text>();
        chat_panel = transform.Find("Chat").Find("ChatList").Find("Background").Find("Chats");
        lastChatPanel = lowerBar.transform.Find("LastChatPanel").gameObject;
        TurnOnAutoScroll();

        chatPanels = new List<RandChat_ChatPanel>();
        chats = new List<GameObject>();

        Tab_matching.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, true);
        Tab_ChatList.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, true);
        Tab_Chat.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false, true);

        Tab_matching.SetActive(true);
        Tab_ChatList.SetActive(false);
        Tab_Chat.SetActive(false);
        RefreshChatList();
    }
    void RefreshChatList()
    {
        if (gm.mainData.randomChatLogs.Count >= 2)
        {
            gm.mainData.randomChatLogs.Sort(delegate (ChatLog A, ChatLog B)
            {
                if (A.beforeChats.Count == 0) return 1;
                else if (B.beforeChats.Count == 0) return -1;
                else if (A.beforeChats[A.beforeChats.Count - 1].date < B.beforeChats[B.beforeChats.Count - 1].date) return 1;
                else if (A.beforeChats[A.beforeChats.Count - 1].date > B.beforeChats[B.beforeChats.Count - 1].date) return -1;
                else return 0;
            });
        }

        float firstPos;
        if (chatPanels.Count >= 1) firstPos = ((RectTransform)chatPanels[0].transform).anchoredPosition.y;
        else firstPos = -10f;
        foreach(RandChat_ChatPanel chatPanel in chatPanels)
        {
            Destroy(chatPanel.gameObject);
        }
        chatPanels.Clear();

        for (int i = 0; i < gm.mainData.randomChatLogs.Count; ++i)
        {
            // 마지막 채팅 형식을 찾는다.
            bool haveLastChat = false;
            for(int j = gm.mainData.randomChatLogs[i].beforeChats.Count-1; j >= 0; --j)
            {
                string result = ToolClass.ParseToSmallScript(gm.mainData.randomChatLogs[i].beforeChats[j].chat);
                if (result != "")
                {
                    GameObject newObj = Instantiate(chatPanelPrefab, targetToChatList.transform);
                    newObj.GetComponent<RandChat_ChatPanel>().Init(ToolClass.GetName(gm.mainData.randomChatLogs[i].charCode, ChatType.RANDCHAT),
                    result,
                    gm.mainData.randomChatLogs[i].charCode,
                    1,
                    gm.mainData.randomChatLogs[i].beforeChats[j].date,
                    gm.mainData.randomChatLogs[i].alram,
                    gm.mainData.randomChatLogs[i]);
                    chatPanels.Add(newObj.GetComponent<RandChat_ChatPanel>());
                    Vector2 beforePos = newObj.GetComponent<RectTransform>().anchoredPosition;
                    if (chatPanels.Count>=2) beforePos.y = ((RectTransform)chatPanels[chatPanels.Count - 2].transform).anchoredPosition.y - 75f;
                    else beforePos.y = firstPos;
                    newObj.GetComponent<RectTransform>().anchoredPosition = beforePos;
                    haveLastChat = true;
                    break;
                }
            }
            // 없다면 빈화면 열어줌
            if (!haveLastChat)
            {
                GameObject newObj = Instantiate(chatPanelPrefab, targetToChatList.transform);
                newObj.GetComponent<RandChat_ChatPanel>().Init(ToolClass.GetName(gm.mainData.randomChatLogs[i].charCode, ChatType.RANDCHAT),
                "",
                gm.mainData.randomChatLogs[i].charCode,
                1,
                DateTime.Now,
                gm.mainData.randomChatLogs[i].alram,
                gm.mainData.randomChatLogs[i]);
                chatPanels.Add(newObj.GetComponent<RandChat_ChatPanel>());
                Vector2 beforePos = newObj.GetComponent<RectTransform>().anchoredPosition;
                if (chatPanels.Count>=2) beforePos.y = ((RectTransform)chatPanels[chatPanels.Count - 2].transform).anchoredPosition.y - 75f;
                else beforePos.y = firstPos;
                newObj.GetComponent<RectTransform>().anchoredPosition = beforePos;
            }
        }
    }
    public void Back()
    {
        switch (currentScreen)
        {
            case ERandomChatScreen.MATCHING:
                {
                    am.Close();
                    break;
                }
            case ERandomChatScreen.CHATLIST:
                {
                    ChangeScreen(ERandomChatScreen.MATCHING);
                    break;
                }
            case ERandomChatScreen.CHAT:
                {
                    ChangeScreen(ERandomChatScreen.CHATLIST);
                    break;
                }
        }
    }

    public void ChangeScreen(int type)
    {
        ChangeScreen((ERandomChatScreen)type);
    }
    public void ChangeScreen(ERandomChatScreen type) // Matching = 0, ChatList, Chat
    {
        if(type == ERandomChatScreen.CHATLIST) RefreshChatList();

        if (currentScreen == type) return;
        switch (type)
        {
            case ERandomChatScreen.MATCHING:
                {
                    if (Tab_ChatList.activeSelf)
                    {
                        Tab_ChatList.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        Tab_ChatList.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    if (Tab_Chat.activeSelf)
                    {
                        Tab_Chat.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        Tab_Chat.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    if (!Tab_matching.activeSelf)
                    {
                        Tab_matching.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                        Tab_matching.GetComponent<FadeOutSystem>().FadeIn();
                    }
                    currentScreen = ERandomChatScreen.MATCHING;
                    break;
                }
            case ERandomChatScreen.CHATLIST:
                {
                    if (!Tab_ChatList.activeSelf)
                    {
                        if(currentScreen == ERandomChatScreen.MATCHING) Tab_ChatList.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        else Tab_ChatList.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                        Tab_ChatList.GetComponent<FadeOutSystem>().FadeIn();
                    }
                    if (Tab_Chat.activeSelf)
                    {
                        Tab_Chat.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        Tab_Chat.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    if (Tab_matching.activeSelf)
                    {
                        Tab_matching.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                        Tab_matching.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    currentScreen = ERandomChatScreen.CHATLIST;
                    break;
                }
            case ERandomChatScreen.CHAT:
                {
                    if (Tab_ChatList.activeSelf)
                    {
                        Tab_ChatList.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                        Tab_ChatList.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    if (!Tab_Chat.activeSelf)
                    {
                        Tab_Chat.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                        Tab_Chat.GetComponent<FadeOutSystem>().FadeIn(delegate
                        {
                            ScrollDown();
                        });
                        ScrollDown();
                    }
                    if (Tab_matching.activeSelf)
                    {
                        Tab_matching.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                        Tab_matching.GetComponent<FadeOutSystem>().FadeOut();
                    }
                    currentScreen = ERandomChatScreen.CHAT;
                    break;
                }
        }
    }

    public GameObject Chat_Photo, Chat_MyText, Chat_Image, Chat_Text, Chat_Profile, Chat_Line;
    List<GameObject> chats;
    Transform chat_panel;
    const float fontLineGab = 62.1f; // 폰트 1줄당 갭
    const float textBoxGab = 30f; // 박스의 패딩값
    const int defaultGab = 60; // 기본 간격
    const int firstLineGab = -20; // 첫 줄의 간격
    [HideInInspector] public int chattingTargetCharCode;

    // charcode-알람으로 찾아가기 위한 함수
    public void OpenChat(int charCode)
    {
        foreach (RandChat_ChatPanel panel in chatPanels)
        {
            if (panel.charCode == charCode && !panel.cl.isDead)
            {
                OpenChat(panel);
                break;
            }
        }
        ScrollDown();
    }
    // 갱신해서 찾아가기 위한 함수
    public void OpenChat(ChatLog cl)
    {
        foreach (RandChat_ChatPanel panel in chatPanels)
        {
            if (panel.cl == cl)
            {
                OpenChat(panel);
                break;
            }
        }
    }

    [HideInInspector] public bool lastIsPC = true; // 마지막으로 대화한게 PC인가?
    int lastScriptPos;
    float lastGab;
    bool firstChat;
    GameObject selectPanel;
    GameObject lowerBar;
    public GameObject selectButton, deadImage, chatBackground;
    UnityEngine.UI.Text typeText;
    public void SetTypeText(string text)
    {
        typeText.text = text;
    }
    public void OnClickProfile(string charCode)
    {
        if(ToolClass.GetCharacterChatType(int.Parse(charCode)) == ChatType.CHAT)
        {
            Modal.GetInstance().CreateNewOkModal("이미 등록된 연락처입니다.");
        }
        else
        {
            MyCharacter character = ToolClass.GetCharacter(int.Parse(charCode));
            character.bOnChat = true;
            character.bOnRand = true;
            character.level = 1;
            character.scriptPriority = 1;
            character.currentScriptPosition = 0;
            character.currentScriptProcess = 0f;
            character.love = 0;
            character.onOverlapScript = false;
            ToolClass.CollectPhoto(character.charCode, character.curMsgPic);
            GameMain.GetInstance().GotoNextScript(character);
            MessangerMain.GetInstance().Resort();
            ToolClass.MakeNotify(AppType.Contact, character.name + "의 연락처가 등록되었습니다.");
            ContactMain.GetInstance().GetComponent<AppMain>().AddAlram(1);
            ContactMain.GetInstance().Resort();

            GameObject msg = MessangerMain.GetInstance().gameObject;
            Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
            msg.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
            msg.GetComponent<FadeOutSystem>().FadeIn();
            MessangerMain.GetInstance().Reload();
            gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
            gameObject.GetComponent<FadeOutSystem>().FadeOut();
            MessangerMain.GetInstance().ChangeScreen(0);
            MessangerMain.GetInstance().OnProfileClick(charCode + "");
        }
    }
    public void OpenChat(RandChat_ChatPanel clickedChatPanel)
    {
        SetUnderScreen(false);

        for (int k = 0; k < selectPanel.transform.childCount; ++k)
            Destroy(selectPanel.transform.GetChild(k).gameObject);

        // 다른 창에서 넘어왔다면 리스트 비워줌
        clickedChatLog = clickedChatPanel.cl;

        // 죽은창 체크해서 이미지 띄워줌
        if (clickedChatLog.isDead)
        {
            deadImage.SetActive(true);
        }
        else
        {
            deadImage.SetActive(false);
        }

        bool isRefresh = false;
        if ((currentScreen != ERandomChatScreen.CHAT || chattingTargetCharCode != clickedChatPanel.charCode))
        {
            SetTypeText("");
            isRefresh = true;
            for (int k = chats.Count - 1; k >= 0; --k)
            {
                Destroy(chats[k].gameObject);
                chats.Remove(chats[k]);
            }
        }
        chattingTargetCharCode = clickedChatPanel.charCode;

        // 새로고침이면 먼저 PC체크
        if (isRefresh)
        {
            firstChat = true;
            lastIsPC = true;
        }

        // 먼저 일치하는 로그를 찾는다.
        for (int i=0 ; i < gm.mainData.randomChatLogs.Count; ++i)
        {
            if (gm.mainData.randomChatLogs[i] == clickedChatPanel.cl)
            {
                // 이후 실행함.

                // 새로고침이면 기본값, 아니면 마지막위치를 - gab 가져옴
                float nextPoint = firstLineGab;
                if (!isRefresh)
                {
                    if (chats.Count <= 0) nextPoint = firstLineGab;
                    else nextPoint = ((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y - lastGab;
                }

                lastGab = -9999;
                int j = 0;
                if (!isRefresh) j = lastScriptPos;
                else lastScriptPos = 0;
                if(gm.currentApp.GetComponent<RandChatMain>()) gm.mainData.randomChatLogs[i].alram = 0;
                RefreshHomeAlram();

                for (; j < gm.mainData.randomChatLogs[i].beforeChats.Count; ++j)
                {
                    BeforeChat bc = gm.mainData.randomChatLogs[i].beforeChats[j];
                    GameObject newObj = null;
                    ScriptParsedData chat = ToolClass.ParseScript(bc.chat);
                    // 내 채팅
                    // 상대 채팅
                    if (chat.type != ScriptType.NORMAL) // 특수 채팅
                    {
                        if (chat.type == ScriptType.PHOTO) // 사진
                        {
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Image, chat_panel);
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.GetComponent<PhotoScript>().SetImageByCode(chat.val);
                        }
                        else if (chat.type == ScriptType.PROFILE) // 메신저 프로필
                        {
                            nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Profile, chat_panel);
                            newObj.name = chattingTargetCharCode + "";
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= ((RectTransform)newObj.transform).rect.height;
                            newObj.transform.Find("CircularPhoto").GetComponent<CircularPhoto>().Init(ToolClass.GetCharacter(chattingTargetCharCode).curMsgPic);
                            newObj.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = ToolClass.GetCharacter(chattingTargetCharCode).name;
                            newObj.transform.Find("Save").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnClickProfile(newObj.name); });
                        }
                        else if (chat.type == ScriptType.WAIT) // 대기
                        {
                            continue;
                        }
                        else if (chat.type == ScriptType.SELECT_START) // 선택
                        {
                            continue;
                        }
                        else if (chat.type == ScriptType.LINE) // 라인
                        {
                            firstChat = true;
                            nextPoint -= defaultGab * 2;
                            newObj = Instantiate(Chat_Line, chat_panel);
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;
                            nextPoint -= defaultGab * 2;
                            lastIsPC = true;
                        }
                        else if (chat.type == ScriptType.NPC_START) // NPC (포토올림)
                        {
                            if (lastIsPC)
                            {
                                firstChat = true;
                                lastIsPC = false;
                            }
                            /*nextPoint -= defaultGab;
                            newObj = Instantiate(Chat_Photo, chat_panel);
                            Vector2 pos = newObj.GetComponent<RectTransform>().anchoredPosition;
                            pos.y = nextPoint;
                            newObj.GetComponent<RectTransform>().anchoredPosition = pos;*/
                        }
                        else if (chat.type == ScriptType.PC_START) // PC
                        {
                            if (!lastIsPC) { 
                                firstChat = true;
                                lastIsPC = true;
                            }
                            continue;
                        }
                    }
                    else if (lastIsPC) // PC의 채팅
                    {
                        string prettyChat = ToolClass.MakeStringPretty(chat.val, 16, false);
                        int lineMany = prettyChat.Split('\n').Length;
                        {
                            bool chk = false;
                            for (int k = j - 1; k >= 0; --k)
                            {
                                switch (ToolClass.ParseScript(gm.mainData.randomChatLogs[i].beforeChats[k].chat).type)
                                {
                                    case ScriptType.ADRESS:
                                    case ScriptType.LINE:
                                    case ScriptType.NPC_START:
                                    case ScriptType.PC_START:
                                    case ScriptType.PHOTO:
                                    case ScriptType.PROFILE:
                                        nextPoint -= defaultGab;
                                        chk = true;
                                        break;
                                    case ScriptType.NORMAL:
                                        chk = true;
                                        nextPoint -= defaultGab / 20f;
                                        break;
                                }
                                if (chk) break;
                            }
                        }
                        newObj = Instantiate(Chat_MyText, chat_panel);
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<UnityEngine.UI.Text>().text = ToolClass.MakeStringPretty(prettyChat, 16, false);
                        newObj.transform.Find("Textbox").Find("Text").Find("RealText").GetComponent<UnityEngine.UI.Text>().text = ToolClass.MakeStringPretty(prettyChat, 16, false);
                        Vector2 beforePos = newObj.GetComponent<RectTransform>().anchoredPosition;
                        beforePos.y = nextPoint;
                        newObj.transform.GetComponent<RectTransform>().anchoredPosition = beforePos;

                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").Find("Text").transform);
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").transform);
                        ((RectTransform)newObj.transform).sizeDelta = ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta;

                        if (Mathf.Abs(((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y) <= 40)
                        {
                            if (lineMany * fontLineGab + textBoxGab < 118)
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, 118f);
                                nextPoint -= 118f + defaultGab / 3;
                            }
                            else
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, lineMany * fontLineGab + textBoxGab);
                                nextPoint -= lineMany * fontLineGab + textBoxGab + defaultGab / 3;
                            }
                        }
                        else
                        {
                            nextPoint -= ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y + defaultGab / 3;
                        }
                        if (firstChat)
                        {
                            firstChat = false;
                        }
                        else
                        {
                            newObj.transform.Find("Tail").GetComponent<UnityEngine.UI.Image>().color = new Color(0, 0, 0, 0);
                        }
                    }
                    else
                    {
                        // NPC의 채팅
                        string prettyChat = ToolClass.MakeStringPretty(chat.val, 16, false);
                        int lineMany = prettyChat.Split('\n').Length;
                        {
                            bool chk = false;
                            for (int k = j - 1; k >= 0; --k)
                            {
                                switch (ToolClass.ParseScript(gm.mainData.randomChatLogs[i].beforeChats[k].chat).type)
                                {
                                    case ScriptType.ADRESS:
                                    case ScriptType.LINE:
                                    case ScriptType.NPC_START:
                                    case ScriptType.PC_START:
                                    case ScriptType.PHOTO:
                                    case ScriptType.PROFILE:
                                        nextPoint -= defaultGab;
                                        chk = true;
                                        break;
                                    case ScriptType.NORMAL:
                                        chk = true;
                                        nextPoint -= defaultGab / 20f;
                                        break;
                                }
                                if (chk) break;
                            }
                        }
                        newObj = Instantiate(Chat_Text, chat_panel);
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<UnityEngine.UI.Text>().text = prettyChat;
                        newObj.transform.Find("Textbox").Find("Text").Find("RealText").GetComponent<UnityEngine.UI.Text>().text = prettyChat;
                        Vector2 beforePos = newObj.transform.GetComponent<RectTransform>().anchoredPosition;
                        beforePos.y = nextPoint;
                        newObj.transform.GetComponent<RectTransform>().anchoredPosition = beforePos;

                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").Find("Text").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").Find("Text").transform);
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().enabled = true;
                        newObj.transform.Find("Textbox").GetComponent<ContentSizeFitter>().SetLayoutVertical();
                        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)newObj.transform.Find("Textbox").transform);
                        ((RectTransform)newObj.transform).sizeDelta = ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta;

                        if (Mathf.Abs(((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y) <= 40)
                        {
                            if (lineMany * fontLineGab + textBoxGab < 118)
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, 118f);
                                nextPoint -= 118f + defaultGab / 3;
                            }
                            else
                            {
                                ((RectTransform)newObj.transform).sizeDelta = new Vector2(((RectTransform)newObj.transform).sizeDelta.x, lineMany * fontLineGab + textBoxGab);
                                nextPoint -= lineMany * fontLineGab + textBoxGab + defaultGab / 3;
                            }
                        }
                        else
                        {
                            nextPoint -= ((RectTransform)newObj.transform.Find("Textbox").transform).sizeDelta.y + defaultGab / 3;
                        }
                        if (firstChat)
                        {
                            firstChat = false;
                        }
                        else
                        {
                            newObj.transform.Find("Tail").GetComponent<UnityEngine.UI.Image>().color = new Color(0, 0, 0, 0);
                        }
                    }

                    if (newObj != null) chats.Add(newObj);
                }
                if(gm.mainData.randomChatLogs[i].beforeChats.Count>=1){
                    // 마지막이 셀렉트면 셀렉트하긔
                    ScriptParsedData chat2 = ToolClass.ParseScript(gm.mainData.randomChatLogs[i].beforeChats[gm.mainData.randomChatLogs[i].beforeChats.Count-1].chat);
                    if (chat2.type == ScriptType.SELECT_START) // 선택
                    {
                        // 마지막 대화가 Select이고 진행도가 1. 즉 지금 해야할때라면?
                        if (ToolClass.GetCharacter(chattingTargetCharCode).currentScriptProcess == 1f)
                        {
                            // 먼저 모든 버튼을 삭제
                            for (int k = 0; k < selectPanel.transform.childCount; ++k)
                                Destroy(selectPanel.transform.GetChild(k).gameObject);

                            string[] selects = chat2.val.Split('$');
                            foreach (string select in selects)
                            {
                                GameObject sb = Instantiate(selectButton, selectPanel.transform);
                                sb.name = select;
                                sb.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = select;
                            }
                            SetUnderScreen(true);
                            if (chats.Count <= 0) lastGab = 0;
                            else lastGab = (((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y) - nextPoint;
                        }
                    }
                }

                if (chats.Count >= 2)
                {
                    ((RectTransform)chatBackground.transform).sizeDelta = new Vector2(((RectTransform)chatBackground.transform).sizeDelta.x,
                            (
                                ((RectTransform)chats[0].transform).anchoredPosition.y) -
                                ((((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y -
                                ((RectTransform)chats[chats.Count - 1].transform).sizeDelta.y - 100)
                            )
                        );
                }
                else
                {
                    ((RectTransform)chatBackground.transform).sizeDelta = new Vector2(((RectTransform)chatBackground.transform).sizeDelta.x, 200f);
                }
                lastScriptPos = j;
                // 이미 갱신됬다면 무시(select에서)
                if (lastGab == -9999)
                {
                    if (chats.Count <= 0) lastGab = 0;
                    else lastGab = (((RectTransform)chats[chats.Count - 1].transform).anchoredPosition.y) - nextPoint;
                }

                // 갱신이거나, 자동 스크롤중이라면 화면을 아래로 내려줌
                if (isRefresh || (!isRefresh && onAutoScroll))
                {
                    TurnOnAutoScroll();
                    ScrollDown();
                }
                // 갱신이 아닌데, 자동 스크롤이아니라면 아래에 표시
                if(!isRefresh && !onAutoScroll)
                {
                    string result = ToolClass.ParseToSmallScript(gm.mainData.randomChatLogs[i].beforeChats[gm.mainData.randomChatLogs[i].beforeChats.Count - 1].chat);
                    if(result!="") SetLastChatText(result);
                }
                ChangeScreen(ERandomChatScreen.CHAT);
                return;
            }
        }
    }
    void TurnOnAutoScroll(bool ignoreTouch = false)
    {
        if ((Input.touchCount > 0 || bMLDown) && !ignoreTouch) return;
        onAutoScroll = true;
        SetLastChatText("");
    }
    public void SetLastChatText(string text)
    {
        if (text == "") lastChatPanel.SetActive(false);
        else
        {
            var str = ToolClass.MakeStringPretty(text, 16, true, 1);
            if (str.Length >= 15) str += "...";
            lastChatPanel.SetActive(true);
            lastChatPanel.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = str;
        }
    }
    public void ScrollDown(bool immediately = false)
    {
        if (!immediately)
        {
            if (Input.touchCount > 0 || bMLDown) return;
            if (chats.Count <= 0) return;
        }
        TurnOnAutoScroll();
        chat_panel.parent.Find("Scrollbar").GetComponent<Scrollbar>().value = 1;
        chat_panel.parent.Find("Scrollbar").GetComponent<Scrollbar>().value = 0;
    }
    float saved_under_sizeDelta = -171717;
    void SetUnderScreen(bool isUp)
    {
        const float selectPanelSize = 580f;
        if(saved_under_sizeDelta == -171717)
        {
            saved_under_sizeDelta = ((RectTransform)chat_panel.transform.parent).sizeDelta.y;
        }
        if (isUp)
        {
            Vector2 beforePos = ((RectTransform)lowerBar.transform).anchoredPosition;
            beforePos.y = selectPanelSize;
            ((RectTransform)lowerBar.transform).anchoredPosition = beforePos;
            beforePos = ((RectTransform)chatBackground.transform).offsetMin;
            beforePos.y += selectPanelSize;
            ((RectTransform)chatBackground.transform).offsetMin = beforePos;
            ((RectTransform)chat_panel.transform.parent).sizeDelta = new Vector2(((RectTransform)chat_panel.transform.parent).sizeDelta.x, saved_under_sizeDelta - selectPanelSize);
        }
        else
        {
            Vector2 beforePos = ((RectTransform)lowerBar.transform).anchoredPosition;
            beforePos.y = 0f;
            ((RectTransform)lowerBar.transform).anchoredPosition = beforePos;
            beforePos = ((RectTransform)chatBackground.transform).offsetMin;
            beforePos.y -= selectPanelSize;
            ((RectTransform)chatBackground.transform).offsetMin = beforePos;
            ((RectTransform)chat_panel.transform.parent).sizeDelta = new Vector2(((RectTransform)chat_panel.transform.parent).sizeDelta.x, saved_under_sizeDelta);

        }
    }
    public void Matching()
    {
        // 랜덤 채팅을 하지 않은 사람이 한명도 없다면
        bool noRand = true;
        bool noFirstRand = true;
        for (int i = 0; i < gm.mainData.characters.Count; ++i)
        {
            if (!(gm.mainData.characters[i].bOnRand || gm.mainData.characters[i].bOnChat))
            {
                noRand = false;
                break;
            }
        }
        for (int i = 0; i < gm.mainData.characters.Count; ++i)
        {
            if (!gm.mainData.characters[i].bOnRand && !gm.mainData.characters[i].bOnChat && gm.mainData.characters[i].alreadyRandChated == false)
            {
                noFirstRand = false;
                break;
            }
        }
        if (noRand)
        {
            Modal.GetInstance().CreateNewOkModal("너무 많은 사람을\n만나고 계십니다...");
            return;
        }

        while (true)
        {
            int randomCharNumber = UnityEngine.Random.Range(0, gm.mainData.characters.Count);

            // 랜덤 채팅을 하지 않은 사람을 찾았다면
            if ((!gm.mainData.characters[randomCharNumber].bOnRand && !gm.mainData.characters[randomCharNumber].bOnChat) && (!gm.mainData.characters[randomCharNumber].alreadyRandChated || noFirstRand))
            {
                int randomCharCode = gm.mainData.characters[randomCharNumber].charCode;
                MyCharacter mychar = ToolClass.GetCharacter(randomCharCode);
                mychar.bOnRand = true;
                mychar.currentScriptPosition = 0;
                mychar.currentScriptProcess = 0;
                mychar.scriptPriority = 1;
                mychar.onOverlapScript = false;
                mychar.love = 0;
                mychar.level = 0;
                mychar.scriptCheck.Clear();
                mychar.alreadyRandChated = true;

                // 새로운 채팅을 열어줌
                ChatLog newChat = new ChatLog();
                newChat.charCode = mychar.charCode;
                newChat.beforeChats = new List<BeforeChat>();
                newChat.alram = 0;
                newChat.isDead = false;
                gm.mainData.randomChatLogs.Add(newChat);
                gm.GotoNextScript(mychar);
                Resort();
                OpenChat(newChat);
                break;
            }
        }
    }
    public void SelectComplete(string selectName)
    {
        SetUnderScreen(false);
        ScrollDown(true);

        // 모든 select 버튼을 삭제
        for (int k = 0; k < selectPanel.transform.childCount; ++k)
            Destroy(selectPanel.transform.GetChild(k).gameObject);

        // 해당 리스트로 찾아간다.
        MyCharacter character = ToolClass.GetCharacter(chattingTargetCharCode);
        ChatScript script = ToolClass.GetScript(character.currentScriptCode);
        for (int i = character.currentScriptPosition+1; i<script.scripts.Count; ++i)
        {
            ScriptParsedData scriptData = ToolClass.ParseScript(script.scripts[i]);
            if(scriptData.type == ScriptType.SELECT_LIST && scriptData.val == selectName)
            {
                character.currentScriptPosition = i + 1;
                character.currentScriptProcess = 0f;
                Resort();
                break;
            }
        }
    }
    public void Resort()
    {
        // 챗 리스트 갱신
        RefreshChatList();

        // 채팅 상황이면 갱신
        if(currentScreen == ERandomChatScreen.CHAT)
        {
            OpenChat(clickedChatLog);
        }
    }
    public void RefreshHomeAlram()
    {
        // 홈화면 알람 갯수 설정
        MainData mainData = GameMain.GetInstance().mainData;
        int count = 0;
        foreach (ChatLog chatLog in mainData.randomChatLogs)
        {
            count += chatLog.alram;
        }
        am.SetAlram(count);
    }
    private static RandChatMain instance;
    public static RandChatMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("RandomChatMain").GetComponent<RandChatMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc() 
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        if (backButtonTime>=backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                Back();
                backButtonTime = 0.0f;
            }
        }

    }

    private void FixedUpdate()
    {
        BackButtonProc();
    }
    bool bMLDown;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) bMLDown = true;
        else if (Input.GetMouseButtonUp(0)) bMLDown = false;
        const int SCREEN_HEIGHT = 2000;
        const int SCREEN_UPPER_OFFSET = 500;
        if (currentScreen == ERandomChatScreen.CHAT)
        {
            RectTransform rt_ChatPanel = (RectTransform)(chatBackground.transform);
            for (int k = 0; k < chats.Count; ++k)
            {
                RectTransform rt = (RectTransform)(chats[k].transform);
                if (rt_ChatPanel.anchoredPosition.y - SCREEN_UPPER_OFFSET <= -rt.anchoredPosition.y && -rt.anchoredPosition.y <= rt_ChatPanel.anchoredPosition.y + SCREEN_HEIGHT) chats[k].SetActive(true);
                else chats[k].SetActive(false);
            }
        }
    }
}