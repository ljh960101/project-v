﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoScript : MonoBehaviour
{
    UnityEngine.UI.Image image;
	// Use this for initialization
	void Start ()
    {
        if (transform.Find("Image"))
        {
            image = transform.Find("Image").Find("ImageReal").GetComponent<UnityEngine.UI.Image>();
        }
        else
        {
            image = transform.Find("ImageReal").GetComponent<UnityEngine.UI.Image>();
        }
    }
    public void OnClick()
    {
        Modal.GetInstance().CreateImageModal(image.sprite);
    }
    public void SetImageByCode(string code)
    {
        if(!image)
        {
            if (transform.Find("Image"))
            {
                image = transform.Find("Image").Find("ImageReal").GetComponent<UnityEngine.UI.Image>();
            }
            else
            {
                image = transform.Find("ImageReal").GetComponent<UnityEngine.UI.Image>();
            }
        }

        image.sprite = Resources.Load<Sprite>("Image/" + code);
    }
}
