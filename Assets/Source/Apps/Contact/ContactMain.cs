﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class ContactMain : MonoBehaviour {
    ScreenType currentType;
    int currentCharCode;
    public Transform Tap_ContactList, Tap_Contact, contactList, contact;
    public GameObject contactPrefab;
    MainData mainData;

    public enum ScreenType
    {
        CONTACT_LIST = 0, CONTACT, SIZE
    }

    List<GameObject> contacts;
    void RefreshContactList()
    {
        if (contacts == null) contacts = new List<GameObject>();
        foreach (var obj in contacts) Destroy(obj);
        contacts.Clear();

        foreach(var character in mainData.characters)
        {
            if (character.bOnChat)
            {
                GameObject newObj = Instantiate(contactPrefab, contactList);
                newObj.name = character.charCode + "";
                newObj.transform.Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curMsgPic);
                newObj.transform.Find("Name").GetComponent<UnityEngine.UI.Text>().text = character.name;
                newObj.transform.Find("Btn_Talk").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OpenChat(character.charCode); });
                newObj.transform.Find("Btn_Insta").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OpenInsta(character.charCode); });
                newObj.transform.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OpenContact(character.charCode); });
                contacts.Add(newObj);
            }
        }
    }
    void RefreshContact()
    {
        MyCharacter character = ToolClass.GetCharacter(currentCharCode);
        string job = "", levelExpression = "", level = "", left1 = "", right1 = "", left2 = "", right2 = "", left3 = "", right3 = "", left4 = "", right4 = "";

        if (currentCharCode == 1) // 한빈
        {
            job = "학생";
            levelExpression = "한빈이가 생각하는 나";
            if (character.level == 1) level = "친구";
            else if (character.level == 2) level = "친한 친구";
            left1 = "직업";
            right1 = "학생";
            left2 = "취미";
            right2 = "게임";
        }
        else if (currentCharCode == 2) // 한빈
        {
            job = "고등학생";
            levelExpression = "아람이가 생각하는 나";
            if (character.level == 0) level = "재미있는 누나";
            else  if (character.level == 1) level = "재미있는 누나";
            else if (character.level == 2) level = "친한 누나";
            else if (character.level == 3) level = "친한 누나";
            else if (character.level == 4) level = "친한 누나";
            else if (character.level == 5) level = "친한 누나";
            left1 = "직업";
            right1 = "고등학생";
            left2 = "특기";
            right2 = "축구";
            left3 = "좋아하는 과목";
            right3 = "없음";
        }
        else if (currentCharCode == 3) // 한빈
        {
            job = "학생";
            levelExpression = "한빈이가 생각하는 나";
            if (character.level == 1) level = "친구";
            else if (character.level == 2) level = "친한 친구";
            left1 = "직업";
            right1 = "학생";
            left2 = "취미";
            right2 = "게임";
        }
        else if (currentCharCode == 4) // 한빈
        {
            job = "학생";
            levelExpression = "한빈이가 생각하는 나";
            if (character.level == 1) level = "친구";
            else if (character.level == 2) level = "친한 친구";
            left1 = "직업";
            right1 = "학생";
            left2 = "취미";
            right2 = "게임";
        }

        contact.Find("Name").GetComponent<UnityEngine.UI.Text>().text = "<b>" + character.name + "</b>\n<color=silver>" + job + "</color>";
        contact.Find("CircularPhoto").GetComponent<CircularPhoto>().Init(character.curMsgPic);
        contact.Find("LevelForm").Find("Level").GetComponent<UnityEngine.UI.Text>().text = level;
        contact.Find("LevelForm").Find("LevelFormText").GetComponent<UnityEngine.UI.Text>().text = levelExpression;
        contact.Find("TextForm1").Find("LeftSide").GetComponent<UnityEngine.UI.Text>().text = left1;
        contact.Find("TextForm1").Find("RightSide").GetComponent<UnityEngine.UI.Text>().text = right1;
        contact.Find("TextForm2").Find("LeftSide").GetComponent<UnityEngine.UI.Text>().text = left2;
        contact.Find("TextForm2").Find("RightSide").GetComponent<UnityEngine.UI.Text>().text = right2;
        contact.Find("TextForm3").Find("LeftSide").GetComponent<UnityEngine.UI.Text>().text = left3;
        contact.Find("TextForm3").Find("RightSide").GetComponent<UnityEngine.UI.Text>().text = right3;
        contact.Find("TextForm4").Find("LeftSide").GetComponent<UnityEngine.UI.Text>().text = left4;
        contact.Find("TextForm4").Find("RightSide").GetComponent<UnityEngine.UI.Text>().text = right4;

        if (left1 == "") contact.Find("TextForm1").gameObject.SetActive(false);
        else contact.Find("TextForm1").gameObject.SetActive(true);

        if (left2 == "") contact.Find("TextForm2").gameObject.SetActive(false);
        else contact.Find("TextForm2").gameObject.SetActive(true);

        if (left3 == "") contact.Find("TextForm3").gameObject.SetActive(false);
        else contact.Find("TextForm3").gameObject.SetActive(true);

        if (left4 == "") contact.Find("TextForm4").gameObject.SetActive(false);
        else contact.Find("TextForm4").gameObject.SetActive(true);
    }
    void OpenContact(int charCode)
    {
        currentCharCode = charCode;
        RefreshContact();
        ChangeScreen(ScreenType.CONTACT);
    }
    public void OpenChat(int charCode)
    {
        if (charCode == 0) charCode = currentCharCode;

        GameObject msg = MessangerMain.GetInstance().gameObject;
        Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
        msg.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        msg.GetComponent<FadeOutSystem>().FadeIn();
        MessangerMain.GetInstance().Reload();
        gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        gameObject.GetComponent<FadeOutSystem>().FadeOut();
        MessangerMain.GetInstance().OnProfileClick(charCode + "");
    }
    public void OpenInsta(int charCode)
    {
        if (charCode == 0) charCode = currentCharCode;
        if (!ToolClass.GetCharacter(charCode).bOnSns)
        {
            Modal.GetInstance().CreateNewOkModal("아직 등록되지 않았습니다.");
            return;
        }

        GameObject sns = SnsMain.GetInstance().gameObject;
        Home.GetInstance().GetComponent<FadeOutSystem>().FadeOut();
        sns.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        sns.GetComponent<FadeOutSystem>().FadeIn();
        SnsMain.GetInstance().Reload();
        gameObject.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
        gameObject.GetComponent<FadeOutSystem>().FadeOut();
        SnsMain.GetInstance().OpenProfile(charCode);
    }
    public void ChangeScreen(int type)
    {
        ChangeScreen((ContactMain.ScreenType)type);
    }
    public void ChangeScreen(ScreenType type)
    {
        switch (type)
        {
            case ScreenType.CONTACT_LIST:
                Tap_Contact.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                Tap_Contact.GetComponent<FadeOutSystem>().FadeOut();
                Tap_ContactList.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                Tap_ContactList.GetComponent<FadeOutSystem>().FadeIn();
                break;
            case ScreenType.CONTACT:
                Tap_Contact.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromRight);
                Tap_Contact.GetComponent<FadeOutSystem>().FadeIn();
                Tap_ContactList.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromLeft);
                Tap_ContactList.GetComponent<FadeOutSystem>().FadeOut();
                break;
            default:
                print("Unexpected Type");
                break;
        }
        currentType = type;
    }
    public void Init()
    {
        mainData = GameMain.GetInstance().mainData;
        Tap_ContactList.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        Tap_Contact.GetComponent<FadeOutSystem>().Init(4f, false, 1f, false);
        RefreshContactList();
        ChangeScreen(ScreenType.CONTACT_LIST);
    }
    public void Reload()
    {
        Resort();
        GetComponent<AppMain>().SetAlram(0);
    }
    public void Resort()
    {
        RefreshContactList();
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                switch (currentType)
                {
                    case ScreenType.CONTACT:
                        ChangeScreen(ScreenType.CONTACT_LIST);
                        break;
                    case ScreenType.CONTACT_LIST:
                        GetComponent<AppMain>().Close();
                        break;
                }
                backButtonTime = 0.0f;
            }
        }

    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
    private static ContactMain instance;
    public static ContactMain GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("ContactMain").GetComponent<ContactMain>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }
        return instance;
    }
}
