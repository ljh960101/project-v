﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyTool;
using UnityEngine.UI;

public class CalcMain : MonoBehaviour
{
    public UnityEngine.UI.Image head;
    public UnityEngine.UI.Text scriptText;
    public GameObject chatPanel;
    float scriptProcess;
    string currentScript;
    float faceProcess;
    enum OpertorType
    {
        Plus, Minus, Multiple, Divide, Nothing
    }
    string currentValue, beforeResult;
    OpertorType beforeType;
    UnityEngine.UI.Text text, oper;
    
    string AddCommaString(string val)
    {
        bool afterDot = false;
        if (val.IndexOf(".") == -1) afterDot = true;
        int count = 0;
        for(int i=val.Length-1; i>=1; --i)
        {
            if (val[i] == '.') afterDot = true;
            else if (afterDot)
            {
                ++count;
                if (count >= 3)
                {
                    val = val.Insert(i, ",");
                    count = 0;
                }
            }
        }
        return val;
    }
    void ChangeScript(string nextScript = "")
    {
        if (nextScript == "")
        {
            int rand = UnityEngine.Random.Range(1, 5);
            if (rand == 1)
            {
                nextScript = "수학은 너무 어려워.";
            }
            else if (rand == 2)
            {
                nextScript = "가감승제만 잘해도\n수학은 충분해!";
            }
            else if (rand == 3)
            {
                nextScript = "고등수학을 써볼날이\n있을까?";
            }
            else if (rand == 4)
            {
                nextScript = "계산은 내가\n도와줄게.";
            }
        }
        scriptProcess = 0f;
        currentScript = nextScript;
    }
    const float speed = 7.0f;
    const float nextScriptDelay = 6.0f * speed;
    const float hideChatDelay = 3.0f * speed;
    void UpdateScript()
    {
        scriptProcess += Time.deltaTime * speed;
        if (scriptProcess >= currentScript.Length + nextScriptDelay)
        {
            ChangeScript();
        }

        if (currentScript.Length + hideChatDelay < scriptProcess || scriptProcess < 1) chatPanel.SetActive(false);
        else
        {
            scriptText.text = currentScript.Substring(0, (int)Mathf.Clamp(scriptProcess, 0.0f, currentScript.Length));
            chatPanel.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)scriptText.transform);
        }
    }
    public Sprite normal, closeEye;
    void UpdateFace()
    {
        if (head.sprite == normal)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 3.0f)
            {
                faceProcess -= 3.0f;
                head.sprite = closeEye;
            }
        }
        else if (head.sprite == closeEye)
        {
            faceProcess += Time.deltaTime;
            if (faceProcess >= 0.1f)
            {
                faceProcess -= 0.1f;
                head.sprite = normal;
            }
        }
    }

    void Start()
    {
        faceProcess = 0;
        scriptProcess = 1000f;
        currentScript = "";
        text = transform.Find("Background").Find("Input").GetComponent<UnityEngine.UI.Text>();
        oper = transform.Find("Background").Find("Oper").GetComponent<UnityEngine.UI.Text>();
        Reset();
    }
    public void Reset()
    {
        currentValue = "";
        beforeResult = "";
        beforeType = OpertorType.Nothing;
        text.text = "";
        oper.text = "";
    }
    public void ShowResult()
    {
        text.text = AddCommaString(beforeResult);
        ChangeScript("계산 결과는\n" + text.text + "!");
    }
    public void CalcToBeforeType()
    {
        oper.text = "";
        if (currentValue.Length >= 1)
        {
            switch (beforeType)
            {
                case OpertorType.Plus:
                    beforeResult = Math.Round(double.Parse(beforeResult) + double.Parse(currentValue), 8) + "";
                    currentValue = "";
                    break;
                case OpertorType.Minus:
                    beforeResult = Math.Round(double.Parse(beforeResult) - double.Parse(currentValue), 8) + "";
                    currentValue = "";
                    break;
                case OpertorType.Multiple:
                    beforeResult = Math.Round(double.Parse(beforeResult) * double.Parse(currentValue), 8) + "";
                    currentValue = "";
                    break;
                case OpertorType.Divide:
                    beforeResult = Math.Round(double.Parse(beforeResult) / double.Parse(currentValue), 8) + "";
                    currentValue = "";
                    break;
                default:
                    beforeResult = Math.Round(double.Parse(currentValue), 8) + "";
                    currentValue = "";
                    break;
            }
        }
        ShowResult();
    }
    public void SetOper()
    {
        switch (beforeType)
        {
            case OpertorType.Plus:
                oper.text = "+";
                break;
            case OpertorType.Minus:
                oper.text = "-";
                break;
            case OpertorType.Multiple:
                oper.text = "×";
                break;
            case OpertorType.Divide:
                oper.text = "÷";
                break;
            case OpertorType.Nothing:
                oper.text = "=";
                break;
            default:
                beforeResult = Math.Round(double.Parse(currentValue), 8) + "";
                currentValue = "";
                break;
        }
    }
    public void GetResult()
    {
        if (currentValue == "" && beforeResult == "") return;
        CalcToBeforeType();
        beforeType = OpertorType.Nothing;
        SetOper();
    }
    public void Plus()
    {
        if (currentValue == "" && beforeResult == "") return;
        CalcToBeforeType();
        beforeType = OpertorType.Plus;
        SetOper();
    }
    public void Minus()
    {
        if (currentValue == "" && beforeResult == "") return;
        CalcToBeforeType();
        beforeType = OpertorType.Minus;
        SetOper();
    }
    public void Multiple()
    {
        if (currentValue == "" && beforeResult == "") return;
        CalcToBeforeType();
        beforeType = OpertorType.Multiple;
        SetOper();
    }
    public void Divide()
    {
        if (currentValue == "" && beforeResult == "") return;
        CalcToBeforeType();
        beforeType = OpertorType.Divide;
        SetOper();
    }
    public void DeleteString()
    {
        if (currentValue.Length >= 1)
        {
            currentValue = currentValue.Substring(0, currentValue.Length - 1);
            text.text = AddCommaString(currentValue);
        }
    }
    public void InputString(string _val)
    {
        if (currentValue.Length >= 15) return;
        if(_val == ".")
        {
            if(currentValue.IndexOf(".") == -1)
            {
                if (currentValue.Length <= 0) currentValue += "0.";
                else currentValue += ".";
            }
        }
        else
        {
            currentValue += _val;
        }
        text.text = AddCommaString(currentValue);
    }

    float backButtonTime = 0.0f;
    const float backButtonDelay = 0.3f;
    void BackButtonProc()
    {
        if (backButtonTime <= backButtonDelay) backButtonTime += Time.deltaTime;
        else if (backButtonTime >= backButtonDelay && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<AppMain>().Close();
                backButtonTime = 0.0f;
            }
        }
    }
    private void Update()
    {
        UpdateScript();
        UpdateFace();
        BackButtonProc();
    }
}