﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyDesign;
using MyTool;

public class DockApp : MonoBehaviour {
    [HideInInspector] public AppType appType;

    Home myHome;
    GameObject appText;
    UnityEngine.UI.Image appIcon_image, alarm_image;
    UnityEngine.UI.Text alarm_text;
    [HideInInspector] public bool onEnabled;
    [HideInInspector] public MyCharacter targetCharacter;

    [HideInInspector] public GameObject myApp;

    public void SetNotification(int num)
    {
        if (num > 0)
        {
            alarm_text.text = num + "";
            alarm_image.enabled = true;
        }
        else
        {
            alarm_text.text = "";
            alarm_image.enabled = false;
        }
    }
    void LinkDock()
    {
        myApp = myHome.transform.parent.Find(appType.ToString() + "Main").gameObject;
        myApp.GetComponent<AppMain>().Init(myHome, this);
        if (myApp.GetComponent<RandChatMain>()) myApp.GetComponent<RandChatMain>().Init();
        else if (myApp.GetComponent<MessangerMain>()) myApp.GetComponent<MessangerMain>().Init();
        else if (myApp.GetComponent<SnsMain>()) myApp.GetComponent<SnsMain>().Init();
        else if (myApp.GetComponent<MiniGameMain>()) myApp.GetComponent<MiniGameMain>().Init();
        else if (myApp.GetComponent<PhotoMain>()) myApp.GetComponent<PhotoMain>().Init();
        else if (myApp.GetComponent<ContactMain>()) myApp.GetComponent<ContactMain>().Init();
        else if (myApp.GetComponent<SettingMain>()) myApp.GetComponent<SettingMain>().Init();
        else if (myApp.GetComponent<AchMain>()) myApp.GetComponent<AchMain>().Init();
        myApp.SetActive(false);
    }
    public void Click()
    {
        if (!onEnabled)
        {
            if(targetCharacter == null) Modal.GetInstance().CreateNewOkModal("해당 캐릭터가 없습니다.");
            else Modal.GetInstance().CreateNewOkModal("<b><size=80>조건</size></b>\n\n" + targetCharacter.name + "<color=red>[좋아하는 친구]</color> 달성.");
        }
        else {
            GameMain.GetInstance().mainData.achMainData.lanchAppChecker[appType] = true;
            myApp.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
            myApp.GetComponent<FadeOutSystem>().FadeIn();
            myHome.fos.FadeOut();
            ReloadApp();
        }
    }
    public void ReloadApp()
    {
        if(myApp == RandChatMain.GetInstance().gameObject)
        {
            RandChatMain.GetInstance().Reload();
        }
        else if (myApp == MessangerMain.GetInstance().gameObject)
        {
            MessangerMain.GetInstance().Reload();
        }
        else if (myApp == SnsMain.GetInstance().gameObject)
        {
            SnsMain.GetInstance().Reload();
        }
        else if (myApp == MiniGameMain.GetInstance().gameObject)
        {
            MiniGameMain.GetInstance().Reload();
        }
        else if (myApp == PhotoMain.GetInstance().gameObject)
        {
            PhotoMain.GetInstance().Reload();
        }
        else if (myApp == ContactMain.GetInstance().gameObject)
        {
            ContactMain.GetInstance().Reload();
        }
        else if (myApp == AchMain.GetInstance().gameObject)
        {
            AchMain.GetInstance().Reload();
        }
    }
    public void SetTextActive(bool active)
    {
        appText.SetActive(active);
    }
    public void SetEnabled(bool val)
    {
        if (val)
        {
            appIcon_image.sprite = Resources.Load<Sprite>("UI/icons/"+appType.ToString());
            onEnabled = true;
        }
        else
        {
            appIcon_image.sprite = Resources.Load<Sprite>("UI/icons/" + appType.ToString() + "_h");
            onEnabled = false;
            GameMain.GetInstance().lockedApps.Add(this);
        }
    }
    public void Init(AppType at, Home home)
    {
        appType = at;
        myHome = home;
        appText.GetComponent<UnityEngine.UI.Text>().text = at.ToString();
        LinkDock();

        appIcon_image.sprite = Resources.Load<Sprite>("UI/icons/" + appType.ToString());
        switch (appType)
        {
            case AppType.Ach:
                appText.GetComponent<UnityEngine.UI.Text>().text = "도전과제";
                onEnabled = true;
                break;
            case AppType.Clock:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "시간";
                targetCharacter = ToolClass.GetCharacter(4);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.Timer:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "타이머";
                targetCharacter = ToolClass.GetCharacter(6);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.Figure:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "전시대";
                targetCharacter = ToolClass.GetCharacter(1);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.SelectGame:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "선택봇";
                targetCharacter = ToolClass.GetCharacter(7);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.Calculator:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "계산기";
                targetCharacter = ToolClass.GetCharacter(2);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.Saying:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "명언";
                targetCharacter = ToolClass.GetCharacter(3);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.StarExpress:
                if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()) return;
                appText.GetComponent<UnityEngine.UI.Text>().text = "별자리";
                targetCharacter = ToolClass.GetCharacter(5);
                if (targetCharacter == null || targetCharacter.level < 5)
                {
                    SetEnabled(false);
                }
                else SetEnabled(true);
                break;
            case AppType.Call:
                appText.GetComponent<UnityEngine.UI.Text>().text = "전화";
                onEnabled = true;
                break;
            case AppType.Contact:
                appText.GetComponent<UnityEngine.UI.Text>().text = "연락처";
                onEnabled = true;
                break;
            case AppType.Game:
                appText.GetComponent<UnityEngine.UI.Text>().text = "장조림을 찾아서";
                onEnabled = true;
                break;
            case AppType.Messenger:
                appText.GetComponent<UnityEngine.UI.Text>().text = "메신저";
                onEnabled = true;
                break;
            case AppType.Photo:
                appText.GetComponent<UnityEngine.UI.Text>().text = "앨범";
                onEnabled = true;
                break;
            case AppType.RandomChat:
                appText.GetComponent<UnityEngine.UI.Text>().text = "랜덤채팅";
                onEnabled = true;
                break;
            case AppType.Setting:
                appText.GetComponent<UnityEngine.UI.Text>().text = "설정";
                onEnabled = true;
                break;
            case AppType.Sns:
                appText.GetComponent<UnityEngine.UI.Text>().text = "린스타그램";
                onEnabled = true;
                break;
        }
    }

	// Use this for initialization
	void Awake () {
        appText = transform.Find("AppText").gameObject;
        appIcon_image = GetComponent<UnityEngine.UI.Image>();
        alarm_text = transform.Find("AlarmText").GetComponent<UnityEngine.UI.Text>();
        alarm_image = transform.Find("AlarmIcon").GetComponent<UnityEngine.UI.Image>();
        myApp = null;

        alarm_text.text = "";
        alarm_image.enabled = false;
    }
}
