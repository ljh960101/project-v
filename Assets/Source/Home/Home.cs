﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class Home : MonoBehaviour {
    [HideInInspector] public FadeOutSystem fos;
    [HideInInspector] public GameMain gm;
    public Transform homeBackground, underBar;

    const int docksApp = (int)AppType.Max - 4;

    LowerBar lb;
    GameObject prefab_app;

	void Start()
    {
        gm = FindObjectOfType<GameMain>();
        lb = GetComponent<LowerBar>();
        fos = GetComponent<FadeOutSystem>();
        fos.Init(3.8f, false, 1f, true, true);
        float screenWidth = Screen.width / (Screen.height / 1920f);

        // Init Apps
        prefab_app = Resources.Load<GameObject>("Prefab/Home/App");

        // Dock Apps Spawn
        for(int i=0; i< docksApp; ++i)
        {
            GameObject newApp = Instantiate(prefab_app, homeBackground);
            newApp.name = "Dock" + ((AppType)i).ToString();
            RectTransform rt = newApp.GetComponent<RectTransform>();

            DockApp app = newApp.GetComponent<DockApp>();
            app.Init((AppType)i, this);
        }
        
        // Lower Apps Spawn
        for(int i=0; i<4; ++i)
        {
            GameObject newApp = Instantiate(prefab_app, underBar);
            newApp.name = "Dock" + ((AppType)i).ToString();
            RectTransform rt = newApp.GetComponent<RectTransform>();
            newApp.GetComponent<DockApp>().SetTextActive(false);

            Vector2 temp = rt.anchorMin;
            temp.y = 0f;
            rt.anchorMin = temp;
            temp = rt.anchorMax;
            temp.y = 0f;
            rt.anchorMax = temp;

            temp = rt.pivot;
            temp.y = 0f;
            rt.pivot = temp;

            DockApp app = newApp.GetComponent<DockApp>();
            app.Init((AppType)(docksApp+i), this);
        }

        Dictionary<AppType, int> tmp = new Dictionary<AppType, int>();
        foreach (var i in GameMain.GetInstance().mainData.appAlramLogs)
        {
            tmp[i.Key] = i.Value;
        }
        foreach (var i in tmp)
        {
            GameMain.GetInstance().transform.Find("AppScreen").Find(i.Key.ToString() + "Main").GetComponent<AppMain>().SetAlram(i.Value);
        }
        gameObject.SetActive(false);
    }
    private void FixedUpdate()
    {
        if (GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf && !fos.bOnFade)
        {
            if (ToolClass.BackButtonAbleChecking() && Input.GetKeyUp(KeyCode.Escape))
            {
                if (FindObjectOfType<ExitModal>() == null)
                {
                    Modal.GetInstance().CreateExitModal();
                }
            }
        }
    }
    private static Home instance;
    public static Home GetInstance()
    {
        if (!instance)
        {
            instance = GameMain.GetInstance().transform.Find("AppScreen").transform.Find("Home").GetComponent<Home>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
}