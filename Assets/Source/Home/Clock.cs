﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour {
    UnityEngine.UI.Text myText;

    void Awake() {
        myText= GetComponent<UnityEngine.UI.Text>();
    }

    void Update () {
        // Set text to date time HH:MM AM/PM

        string h, m;
        if (System.DateTime.Now.Hour < 10)
        {
            h = "0" + System.DateTime.Now.Hour + "";
        }
        else
        {
            h = System.DateTime.Now.Hour + "";
        }
        if (System.DateTime.Now.Minute < 10)
        {
            m = "0" + System.DateTime.Now.Minute + "";
        }
        else
        {
            m = System.DateTime.Now.Minute + "";
        }
        myText.text = h + ":" + m;
    }
}