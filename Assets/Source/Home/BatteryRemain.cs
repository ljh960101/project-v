﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryRemain : MonoBehaviour
{
    UnityEngine.UI.Text myText;

    void Awake ()
    {
        myText = GetComponent<UnityEngine.UI.Text>();
    }
	
	void Update () {
        // Set text To battery remain rate
        myText.text = (int)(SystemInfo.batteryLevel * 100f) + "";
	}
}