﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace MyDesign
{
    [Serializable]
    public class AchMainData
    {
        public Dictionary<int, AchData> achDatas;
        public List<AchData> finishList;
        public int score;
        public int loginCount;
        public Dictionary<AppType, bool> lanchAppChecker;
    }
    [Serializable]
    public class AchData
    {
        public int arcCode;
        public int arcValue;
        public AchType achType;
        public int score;
        public string arcName;
        public string arcContent;
    }
    [Serializable]
    public enum AchType
    {
        MiniGameScoreAch,
        RandomChatMany,
        CharacterLevel,
        CollectPic,
        LoginMany,
        EndingMany,
        LanchApp
    }
    [Serializable]
    public class SnsAlram
    {
        public int charCode;
        public int postCode;
        public string msg;
        public bool onAlram;
    }
    [Serializable]
    public class SnsPostData
    {
        public int postCode;
        public int charCode;
        public string picCode;
        public int defaulatLike;
        public string content;
        public int level;
        public int priority;
        public bool isOver;
        public bool isPosted;
        public List<PostScript> postScripts;
    }
    [Serializable]
    public class SnsLog
    {
        public int like;
        public bool playerLiked;
        public List<string> comments;
        public int postCode;
    }
    [Serializable]
    public class PostScript
    {
        public List<string> scripts;
        public int currentPos;
        public float currentProcess;
        public bool isOver;
    }
    [Serializable]
    public class CharaterJsonData
    {
        public MyCharacter[] characters;
    }
    [Serializable]
    public class ScriptJsonData
    {
        public ChatScript[] scripts;
    }
    [Serializable]
    public class SnsJsonData
    {
        public SnsPostData[] posts;
    }
    [Serializable]
    public class SavedData
    {
        public MainData[] maindatas;
    }
    [Serializable]
    public class MainData
    {
        public List<MyCharacter> characters;
        public List<ChatScript> scripts;
        public List<SnsPostData> posts;
        public Dictionary<AppType, int> appAlramLogs;

        public Dictionary<int, bool> trigger;

        public List<ChatLog> randomChatLogs;

        public string msgName;
        public List<ChatLog> chatLogs;

        public string snsId;
        public List<SnsLog> snsLogs;
        public List<SnsAlram> snsAlrams;

        public MiniGameData miniGameData;
        public SettingData settingData;
        public DateTime lastFrameTime;
        public AchMainData achMainData;
    }
    [Serializable]
    public class SettingData
    {
        public bool gameAlram, androidAlram, vibration;
        public float sound;
        public float speed;
    }
    [Serializable]
    public class MiniGameData
    {
        public List<MiniGameScore> scores;
        public List<MiniGameCharacter> characters;
        public int currentCharacterCode;
        public int lastScore;
    }
    [Serializable]
    public class MiniGameCharacter
    {
        public bool onAlram;
        public bool onUnlocked;
        public int miniGameCharacterCode;
        public int charCode;
        public int level;
        public int priority;
        public int love;
    }
    [Serializable]
    public class MiniGameScore
    {
        public int charCode;
        public int score;
        public DateTime scoredDate;
        public int lastScriptCounter; // 최근 스크립트 실행 후 카운트
    }
    [Serializable]
    public class ChatLog
    {
        public bool isDead; // 죽은 채팅인가?
        public int alram;
        public int charCode;
        public List<BeforeChat> beforeChats;
    }
    [Serializable]
    public struct BeforeChat
    {
        public string chat;
        public System.DateTime date;
    }
    [Serializable]
    public class MyCharacter
    {
        public bool afterEnding;
        public bool alreadyRandChated;
        public int snsFollwer, snsFollow;
        public int charCode;
        public string curSnsPic, curMsgPic;
        public int love;
        public int level;
        public int game_defaultRanking;
        public int game_playCount;
        public int game_physical;
        public int game_offset;
        public string name, snsId;
        public string snsProfileMsg;
        public string statusMsg; // 상태 메세지
        public bool bOnRand;
        public bool bOnChat;
        public bool bOnSns;
        public bool bOpenSns;
        public bool onGameCompetition;
        public Dictionary<int, bool> scriptCheck; // 이미 본 스크립트인지 체크.
        public int currentScriptCode;
        public int currentScriptPosition;
        public float currentScriptProcess;
        public bool isGuest;
        public int scriptPriority;
        public bool onOverlapScript; // 중복스크립트 가능 여부
        public List<string> collectedPhotos;
    }
    [Serializable]
    public class OtherLevelLimit
    {
        public int chracterCode;
        public int levelMin;
        public int levelMax;
    }
    [Serializable]
    public class ChatScript
    {
        public int scriptCode;
        public int charCode;
        public bool isMain;
        public bool isOnlyOneTime;
        public int priority;
        public int level;
        public List<OtherLevelLimit> otherLevelLimits;
        public List<string> scripts;
        public ScriptEventType scriptEventType;
    }
    [Serializable]
    public enum ScriptEventType
    {
        NORMAL,
        RANDOMCHAT_END,
        GAME_COMPETITION
    }
    [Serializable]
    public enum AppType
    {
        RandomChat = 0,
        Calculator,
        Game,
        Photo,
        Ach,
        Setting,
        Clock,
        Timer,
        SelectGame,
        Figure,
        Saying,
        StarExpress,
        Call,
        Contact,
        Messenger,
        Sns,
        Max
    };
    [Serializable]
    public struct NotifyInfo
    {
        AppType appCode;
        int notify_many;
    }
    [Serializable]
    public struct NotifyData
    {
        NotifyInfo[] notifyInfos;
    }
    [Serializable]
    public struct Chat
    {
        string name;
        string text;
        Time time;
    }
    [Serializable]
    public enum ChatType {
        RANDCHAT,
        CHAT,
        SNS,
        NO_CHAT
    }
    public class ScriptParsedData
    {
        public ScriptType type;
        public string val;
    }
    [Serializable]
    public enum ScriptType
    {
        PC_START,
        NPC_START,
        NPC_NOREGI,
        WAIT,
        SELECT_START,
        SELECT_LIST,
        SELECT_END,
        LINE,
        PROFILE,
        PHOTO,
        CHANGE_NAME,
        CHANGE_PHOTO,
        ADRESS,
        NORMAL,
        LOVE,
        DEAD,
        CHANGE_STATUS,
        GOTO_LINE,
        TRIGGER_ON,
        TRIGGER_OFF,
        WAIT_TRIGGER,
        IF,
        COMMENT,
        Like,
        GAME_START,
        GAME_NPC,
        GAME_PC,
        GAME_HIGH,
        GAME_SAME,
        GAME_LOW,
        GAME_IGNORE,
        GAME_END,
        ENDING
    }
}