﻿//#define WINDOW
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyDesign;
using UnityEngine.SceneManagement;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using UnityEngine.Windows;
using System.IO;
#endif

public class CharacterMaker : MonoBehaviour {

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    public GameObject charButton;

    MyCharacter currentCharacter;
    List<MyCharacter> characters;
    List<GameObject> buttons;
    
    float counter;
    void OnApplicationQuit()
    {
        Create();
    }
    private void Update()
    {
        counter += Time.deltaTime;
        if(counter >= 30f)
        {
            counter -= 30f;
            Create();
        }
    }
    public void Status(string val)
    {
        currentCharacter.statusMsg = val;
    }
    public void Following(string val)
    {
        currentCharacter.snsFollow = int.Parse(val);
    }
    public void Follower(string val)
    {
        currentCharacter.snsFollwer = int.Parse(val);
    }
    public void snsProfileMsg(string val)
    {
        currentCharacter.snsProfileMsg = val;
    }
    public void OnMsg(Int32 val)
    {
        switch (val)
        {
            case 0:
                currentCharacter.bOnChat = false;
                currentCharacter.bOnRand = false;
                currentCharacter.bOnSns = false;
                currentCharacter.isGuest = true;
                break;
            case 1:
                currentCharacter.bOnChat = false;
                currentCharacter.bOnRand = false;
                currentCharacter.bOnSns = false;
                currentCharacter.isGuest = false;
                break;
            case 2:
                currentCharacter.bOnChat = true;
                currentCharacter.bOnRand = true;
                currentCharacter.bOnSns = false;
                currentCharacter.isGuest = false;
                break;
            case 3:
                currentCharacter.bOnChat = true;
                currentCharacter.bOnRand = true;
                currentCharacter.bOnSns = true;
                currentCharacter.isGuest = false;
                break;
        }
    }
    void LoadFile()
    {
        if (System.IO.File.Exists("characterData.txt"))
        {
            Load();
        }
        else InitFile();
    }
    void SaveFile()
    {
        CharaterJsonData obj = new CharaterJsonData();
        obj.characters = this.characters.ToArray();
        System.IO.File.WriteAllText("characterData.txt", JsonUtility.ToJson(obj));
    }
    void InitFile()
    {
        New();
        Create();
        CharaterJsonData obj = new CharaterJsonData();
        obj.characters = this.characters.ToArray();
        System.IO.File.WriteAllText("characterData.txt", JsonUtility.ToJson(obj));
        LoadFile();
    }
    public void StartScript(string val)
    {
        currentCharacter.currentScriptCode = int.Parse(val);
    }
    private void Start()
    {
        buttons = new List<GameObject>();
        characters = new List<MyCharacter>();
        LoadFile();
    }
    public void New()
    {
        currentCharacter = RenewCurrentCharacter();
        characters.Add(currentCharacter);
        MakeButtons();
        ResetToCurrentChar();
    }
    public void OnButton(string index)
    {
        currentCharacter = characters[int.Parse(index)];
        MakeButtons();
        ResetToCurrentChar();
    }
    void ResetToCurrentChar()
    {
        transform.Find("StartScriptCode").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.currentScriptCode + "";
        transform.Find("CharCode").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.charCode + "";
        transform.Find("CharName").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.name;
        transform.Find("SnsName").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.snsId;
        transform.Find("PicCode").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.curMsgPic + "";
        transform.Find("SnsPicCode").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.curSnsPic + "";
        transform.Find("Status").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.statusMsg;
        transform.Find("SnsProfileMsg").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.snsProfileMsg;
        transform.Find("GamePlayCount").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.game_playCount + "";
        transform.Find("GameRanking").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.game_defaultRanking + "";
        transform.Find("GameOffset").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.game_offset + "";
        transform.Find("GamePhysical").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.game_physical + "";
        transform.Find("Following").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.snsFollow + "";
        transform.Find("Follower").GetComponent<UnityEngine.UI.InputField>().text = currentCharacter.snsFollwer + "";
        if (currentCharacter.bOnSns)
            transform.Find("CharType").GetComponent<UnityEngine.UI.Dropdown>().value = 3;
        else if (currentCharacter.bOnChat)
            transform.Find("CharType").GetComponent<UnityEngine.UI.Dropdown>().value = 2;
        else if (currentCharacter.isGuest)
            transform.Find("CharType").GetComponent<UnityEngine.UI.Dropdown>().value = 0;
        else
            transform.Find("CharType").GetComponent<UnityEngine.UI.Dropdown>().value = 1;
    }
    void MakeButtons()
    {
        foreach(GameObject button in buttons)
        {
            Destroy(button);
        }
        buttons.Clear();

        characters.Sort(delegate (MyCharacter A, MyCharacter B)
        {
            if (A.charCode > B.charCode) return 1;
            else if (A.charCode < B.charCode) return -1;
            else return 0;
        });

        foreach(MyCharacter character in characters)
        {
            GameObject newObj = Instantiate(charButton, transform.Find("List").Find("GirdWithElements"));
            newObj.name = characters.IndexOf(character) + "";
            newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = character.charCode + "";
            newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnButton(newObj.name); });
            buttons.Add(newObj);
        }
    }
    public void Delete()
    {
        characters.Remove(currentCharacter);
        if (characters.Count > 0) currentCharacter = characters[0];
        else currentCharacter = RenewCurrentCharacter();
        MakeButtons();
        ResetToCurrentChar();
    }
    public void Create()
    {
        CharaterJsonData obj = new CharaterJsonData();
        obj.characters = this.characters.ToArray();
        //transform.Find("Code").GetComponent<UnityEngine.UI.InputField>().text = JsonUtility.ToJson(obj);
        //GUIUtility.systemCopyBuffer = transform.Find("Code").GetComponent<UnityEngine.UI.InputField>().text;
        SaveFile();
    }
    public void Load()
    {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        try
        {
            characters.Clear();
            characters.AddRange(JsonUtility.FromJson<CharaterJsonData>(System.IO.File.ReadAllText("characterData.txt")).characters);
            if (characters.Count > 0) currentCharacter = characters[0];
            MakeButtons();
            ResetToCurrentChar();
        }
        catch
        {
            Modal.GetInstance().CreateNewOkModal("잘못된 코드.");
        }
#endif
    }
    public void CharName(string name)
    {
        currentCharacter.name = name;
    }
    public void CharCode(string code)
    {
        currentCharacter.charCode = int.Parse(code);
        MakeButtons();
    }
    public void SnsName(string name)
    {
        currentCharacter.snsId = name;
    }
    public void GameCount(string code)
    {
        currentCharacter.game_playCount = int.Parse(code);
    }
    public void GameRank(string code)
    {
        currentCharacter.game_defaultRanking = int.Parse(code);
    }
    public void GamePhysical(string code)
    {
        currentCharacter.game_physical = int.Parse(code);
    }
    public void GameOffset(string code)
    {
        currentCharacter.game_offset = int.Parse(code);
    }
    public void PicCode(string code)
    {
        currentCharacter.curMsgPic = code;
    }
    public void SnsPicCode(string code)
    {
        currentCharacter.curSnsPic = code;
    }
    public void Goto_ScriptMaker()
    {
        Create();
        SceneManager.LoadScene("DeveloperMenu");
    }
    public void GotoMerger()
    {
        Create();
        SceneManager.LoadScene("Merger");
    }
    MyCharacter RenewCurrentCharacter()
    {
        MyCharacter newCharacter;
        newCharacter = new MyCharacter();
        newCharacter.charCode = 0;
        newCharacter.curMsgPic = "1";
        newCharacter.curSnsPic = "1";
        newCharacter.love = 0;
        newCharacter.level = 0;
        newCharacter.name = "0";
        newCharacter.snsId = "0";
        newCharacter.bOnRand = false;
        newCharacter.bOnChat = false;
        newCharacter.bOnSns = false;
        newCharacter.scriptCheck = new Dictionary<int, bool>();
        newCharacter.currentScriptCode = 0;
        newCharacter.currentScriptPosition = 0;
        newCharacter.currentScriptProcess = 0f;
        newCharacter.isGuest = true;
        newCharacter.scriptPriority = 1;
        newCharacter.onOverlapScript = false;
        newCharacter.statusMsg = "카톡메세지!";
        newCharacter.snsProfileMsg = "인스타메세지!";
        return newCharacter;
    }
#endif
}