﻿using System.Collections.Generic;
using UnityEngine;
using System;
using MyDesign;
using UnityEngine.SceneManagement;

public class Merger : MonoBehaviour
{
    UnityEngine.UI.InputField in1, in2, result;
    Int32 option;
    private void Start()
    {
        in1 = transform.Find("Input1").GetComponent<UnityEngine.UI.InputField>();
        in2 = transform.Find("Input2").GetComponent<UnityEngine.UI.InputField>();
        result = transform.Find("Result").GetComponent<UnityEngine.UI.InputField>();
        option = 0;
    }
    public void GotoScriptMaker()
    {
        SceneManager.LoadScene("DeveloperMenu");
    }
    public void GotoCharacterMaker()
    {
        SceneManager.LoadScene("Character Maker");
    }
    public void Create()
    {
        string val1 = in1.text, val2 = in2.text;

        switch (option)
        {
            case 0:
                try
                {
                    List<MyCharacter> v1 = new List<MyCharacter>();
                    List<MyCharacter> v2 = new List<MyCharacter>();
                    v1.AddRange(JsonUtility.FromJson<CharaterJsonData>(val1).characters);
                    v2.AddRange(JsonUtility.FromJson<CharaterJsonData>(val2).characters);

                    foreach(MyCharacter character in v2)
                    {
                        v1.Add(character);
                    }

                    CharaterJsonData obj = new CharaterJsonData();
                    obj.characters = v1.ToArray();
                    result.text = JsonUtility.ToJson(obj);
                }
                catch
                {
                    Modal.GetInstance().CreateNewOkModal("데이터가 이상합니다.");
                    return;
                }
                break;
            case 1:
                try
                {
                    List<ChatScript> v1 = new List<ChatScript>();
                    List<ChatScript> v2 = new List<ChatScript>();
                    v1.AddRange(JsonUtility.FromJson<ScriptJsonData>(val1).scripts);
                    v2.AddRange(JsonUtility.FromJson<ScriptJsonData>(val2).scripts);

                    foreach (ChatScript script in v2)
                    {
                        v1.Add(script);
                    }

                    ScriptJsonData obj = new ScriptJsonData();
                    obj.scripts = v1.ToArray();
                    result.text = JsonUtility.ToJson(obj);
                }
                catch
                {
                    Modal.GetInstance().CreateNewOkModal("데이터가 이상합니다.");
                    return;
                }
                break;
            case 2:
                try
                {
                    List<SnsPostData> v1 = new List<SnsPostData>();
                    List<SnsPostData> v2 = new List<SnsPostData>();
                    v1.AddRange(JsonUtility.FromJson<SnsJsonData>(val1).posts);
                    v2.AddRange(JsonUtility.FromJson<SnsJsonData>(val2).posts);

                    foreach (SnsPostData script in v2)
                    {
                        v1.Add(script);
                    }

                    SnsJsonData obj = new SnsJsonData();
                    obj.posts = v1.ToArray();
                    result.text = JsonUtility.ToJson(obj);
                }
                catch
                {
                    Modal.GetInstance().CreateNewOkModal("데이터가 이상합니다.");
                    return;
                }
                return;
        }
        GUIUtility.systemCopyBuffer = result.text;
        Modal.GetInstance().CreateNewOkModal("복사가 된거시애오...");
    }
    public void OptionChanged(Int32 val)
    {
        option = val;
    }
}
