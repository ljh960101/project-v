﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using System;
using UnityEngine.SceneManagement;
using MyTool;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using UnityEngine.Windows;
using System.IO;
#endif

public class SnsMaker : MonoBehaviour
{
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    public GameObject scriptButton;
    public Transform panel_scriptMaker, panel_editor, panel_sns;

    SnsPostData currentData;
    PostScript currentPostScript;
    List<PostScript> scripts;
    List<SnsPostData> datas;
    List<GameObject> sm_buttons;
    List<GameObject> ol_buttons;

    ScreenType currentScreen;
    float autoDelayValue;

    float counter;
    private void Update()
    {
        counter += Time.deltaTime;
        if (counter >= 30f)
        {
            counter -= 30f;
            Create();
        }
    }
    void LoadFile()
    {
        if (System.IO.File.Exists("postData.txt"))
        {
            panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text = System.IO.File.ReadAllText("postData.txt");
            Load();
        }
        else InitFile();
    }
    public void GotoGame()
    {
        Create();
        SceneManager.LoadScene("DeveloperMenu");
    }
    void SaveFile()
    {
        panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text = panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text.Replace("\r", "");
        System.IO.File.WriteAllText("postData.txt", panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text);
    }
    void InitFile()
    {
        New();
        Create();
        System.IO.File.WriteAllText("postData.txt", panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text);
        LoadFile();
    }
    enum ScreenType
    {
        ScriptMaker = 0,
        ScriptEditor,
        SIMULATE
    }
    void OnApplicationQuit()
    {
        Create();
    }
    public void Simulate()
    {
        GotoMenu(2);
        Create();
    }
    public void ScriptChange(string script)
    {
        currentPostScript.scripts.Clear();
        currentPostScript.scripts.AddRange(script.Split('\n'));
        for (int i = currentPostScript.scripts.Count-1; i >= 0; --i)
        {
            if (currentPostScript.scripts[i] == "" || currentPostScript.scripts[i] == "\n")
                currentPostScript.scripts.Remove(currentPostScript.scripts[i]);
        }
    }
    public void Exit(int type)
    {
        GotoMenu(type);
    }
    public void Copy()
    {
        string newString = "";
        foreach (string str in currentPostScript.scripts)
        {
            newString += str + '\n';
        }
        GUIUtility.systemCopyBuffer = newString;
    }
    public UnityEngine.UI.Toggle waitCansleToggle;
    public void GotoMenu(int screenType)
    {
        currentScreen = (ScreenType)screenType;
        panel_scriptMaker.gameObject.SetActive(false);
        panel_editor.gameObject.SetActive(false);
        panel_sns.gameObject.SetActive(false);
        switch (currentScreen)
        {
            case ScreenType.ScriptMaker:
                ResetToCurrentData();
                MakeButtons();
                panel_scriptMaker.gameObject.SetActive(true);
                break;
            case ScreenType.ScriptEditor:
                scripts = currentData.postScripts;
                gameObject.GetComponent<UnityEngine.UI.CanvasScaler>().referenceResolution = new Vector2(320, 480);
                ResetToCurrentData();
                MakeButtons();
                panel_editor.gameObject.SetActive(true);
                break;
            case ScreenType.SIMULATE:
                GameMain gm = GameMain.GetInstance();
                gm.mainData.characters = new List<MyCharacter>();
                gm.gameStart = true;
                MyCharacter testCharacter = new MyCharacter();
                testCharacter.charCode = 1;
                testCharacter.curMsgPic = "hs_01";
                testCharacter.love = 0;
                testCharacter.level = 1;
                testCharacter.name = "이름";
                testCharacter.snsId = "인스타아이디";
                testCharacter.bOnRand = true;
                testCharacter.bOnChat = true;
                testCharacter.bOpenSns = true;
                testCharacter.bOnSns = true;
                testCharacter.scriptCheck = new Dictionary<int, bool>();
                testCharacter.currentScriptCode = 0;
                testCharacter.currentScriptPosition = 0;
                testCharacter.currentScriptProcess = 0;
                testCharacter.collectedPhotos = new List<string>();
                testCharacter.isGuest = true;
                testCharacter.scriptPriority = 1;
                testCharacter.onOverlapScript = false;
                gm.mainData.characters.Add(testCharacter);

                bool noWait = waitCansleToggle.isOn;
                gm.mainData.snsAlrams = new List<SnsAlram>();
                gm.mainData.snsLogs = new List<SnsLog>();

                SnsPostData testScript = new SnsPostData();
                testScript.charCode = 1;
                testScript.level = 0;
                testScript.priority = 1;
                testScript.content = currentData.content;
                testScript.defaulatLike = currentData.defaulatLike;
                testScript.picCode = currentData.picCode;
                testScript.postCode = 999;
                testScript.postScripts = new List<PostScript>();
                PostScript testScript2 = new PostScript();
                testScript2.currentPos = 0;
                testScript2.currentProcess = 0f;
                testScript2.scripts = new List<string>();
                int test = 0;
                try
                {
                    foreach (string script in currentPostScript.scripts)
                    {
                        test = currentPostScript.scripts.IndexOf(script);
                        string str = script.Trim('\r');
                        if (noWait)
                        {
                            ScriptParsedData spd = ToolClass.ParseScript(str);
                            if (spd.type == ScriptType.WAIT)
                            {
                                str = "[W 0.2]";
                            }
                        }
                        testScript2.scripts.Add(str);
                    }
                }
                catch
                {
                    Modal.GetInstance().CreateNewOkModal((test+1) + "번째줄 에서 오류 발생");
                }
                testScript.postScripts.Add(testScript2);
                gm.mainData.posts = new List<SnsPostData>();
                gm.mainData.posts.Add(testScript);

                panel_sns.gameObject.SetActive(true);
                gameObject.GetComponent<UnityEngine.UI.CanvasScaler>().referenceResolution = new Vector2(1080, 1920);
                gm.currentApp = MessangerMain.GetInstance().gameObject;
                if (!SnsMain.GetInstance().am)
                {
                    SnsMain.GetInstance().Init();
                }
                SnsMain.GetInstance().Resort();
                SnsMain.GetInstance().Reload();
                if (SnsMain.GetInstance().transform.Find("Cover")) Destroy(MessangerMain.GetInstance().transform.Find("Cover").gameObject);
                break;
        }
        Create();
    }
    private void Start ()
    {
        autoDelayValue = 0;
        sm_buttons = new List<GameObject>();
        ol_buttons = new List<GameObject>();
        scripts = new List<PostScript>();
        datas = new List<SnsPostData>();
        LoadFile();
        GotoMenu(0);
    }
    public void New()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            currentData = RenewCurrentData();
            datas.Add(currentData);
            MakeButtons();
            ResetToCurrentData();
        }
        else
        {
            currentPostScript = RenewScript();
            scripts.Add(currentPostScript);
            MakeButtons();
            ResetToCurrentData();
        }
    }
    public void Delete()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            datas.Remove(currentData);
            if (datas.Count > 0) currentData = datas[0];
            else currentData = RenewCurrentData();
            MakeButtons();
            ResetToCurrentData();
        }
        else
        {
            scripts.Remove(currentPostScript);
            if (scripts.Count > 0) currentPostScript = scripts[0];
            else currentPostScript = null;
            MakeButtons();
            ResetToCurrentData();
        }
    }
    public void Create()
    {
        SnsJsonData obj = new SnsJsonData();
        obj.posts = this.datas.ToArray();
        panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text = JsonUtility.ToJson(obj);
        //GUIUtility.systemCopyBuffer = panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text;
        SaveFile();
    }
    public void Load()
    {
        try
        {
            datas.Clear();
            datas.AddRange(JsonUtility.FromJson<SnsJsonData>(panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text).posts);
            if (datas.Count > 0) currentData = datas[0];
            else New();
            MakeButtons();
            ResetToCurrentData();
        }
        catch
        {
            Modal.GetInstance().CreateNewOkModal("잘못된 코드.");
        }
    }
    public void OnButton(string index)
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            currentData = datas[int.Parse(index)];
            MakeButtons();
            ResetToCurrentData();
        }
        else
        {
            currentPostScript = scripts[int.Parse(index)];
            MakeButtons();
            ResetToCurrentData();
        }
    }
    void MakeButtons()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            foreach (GameObject button in sm_buttons)
            {
                Destroy(button);
            }
            sm_buttons.Clear();

            datas.Sort(delegate (SnsPostData A, SnsPostData B)
            {
                if (A.postCode > B.postCode) return 1;
                else if (A.postCode < B.postCode) return -1;
                else return 0;
            });
            foreach (SnsPostData data in datas)
            {
                GameObject newObj = Instantiate(scriptButton, panel_scriptMaker.Find("List").Find("GirdWithElements"));
                newObj.name = datas.IndexOf(data) + "";
                newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = data.postCode + "";
                newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnButton(newObj.name); });
                sm_buttons.Add(newObj);
            }
        }
        else
        {
            foreach (GameObject button in ol_buttons)
            {
                Destroy(button);
            }
            ol_buttons.Clear();
            foreach (PostScript script in scripts)
            {
                GameObject newObj = Instantiate(scriptButton, panel_editor.Find("List").Find("GirdWithElements"));
                newObj.name = scripts.IndexOf(script) + "";
                newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = newObj.name + "";
                newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnButton(newObj.name); });
                ol_buttons.Add(newObj);
            }
        }
    }
    void ResetToCurrentData()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            panel_scriptMaker.Find("CharCode").GetComponent<UnityEngine.UI.InputField>().text = currentData.charCode + "";
            panel_scriptMaker.Find("PostCode").GetComponent<UnityEngine.UI.InputField>().text = currentData.postCode + "";
            panel_scriptMaker.Find("DefaultLike").GetComponent<UnityEngine.UI.InputField>().text = currentData.defaulatLike + "";
            panel_scriptMaker.Find("Level").GetComponent<UnityEngine.UI.InputField>().text = currentData.level + "";
            panel_scriptMaker.Find("Priority").GetComponent<UnityEngine.UI.InputField>().text = currentData.priority + "";
            panel_scriptMaker.Find("PicCode").GetComponent<UnityEngine.UI.InputField>().text = currentData.picCode + "";
            panel_scriptMaker.Find("Content").GetComponent<UnityEngine.UI.InputField>().text = currentData.content + "";
        }
        else if(currentScreen == ScreenType.ScriptEditor)
        {
            if (currentPostScript == null)
            {
                panel_editor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().interactable = false;
                return;
            }

            panel_editor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().interactable = true;
            string myScript = "";
            foreach(string script in currentPostScript.scripts)
            {
                myScript += script + '\n';
            }
            panel_editor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().text = myScript;
        }
    }
    PostScript RenewScript()
    {
        PostScript newLimit = new PostScript();
        newLimit.currentPos = 0;
        newLimit.currentProcess = 0f;
        newLimit.isOver = false;
        newLimit.scripts = new List<string>();
        return newLimit;
    }
    SnsPostData RenewCurrentData()
    {
        SnsPostData newScript = new SnsPostData();
        newScript.charCode = 1;
        newScript.content = "";
        newScript.defaulatLike = 0;
        newScript.isOver = false;
        newScript.isPosted = false;
        newScript.level = 0;
        newScript.picCode = "1";
        newScript.postCode = 1;
        newScript.postScripts = new List<PostScript>();
        newScript.priority = 1;
        return newScript;
    }
    public void PostCode(string val)
    {
        currentData.postCode = int.Parse(val);
        MakeButtons();
    }
    public void CharCode(string val)
    {
        currentData.charCode = int.Parse(val);
    }
    public void PicCode(string val)
    {
        currentData.picCode = val;
    }
    public void DefaultLike(string val)
    {
        currentData.defaulatLike = int.Parse(val);
    }
    public void CharLevel(string val)
    {
        currentData.level = int.Parse(val);
    }
    public void CharPriority(string val)
    {
        currentData.priority = int.Parse(val);
    }
    public void Content(string val)
    {
        currentData.content = val;
    }
#endif
}
