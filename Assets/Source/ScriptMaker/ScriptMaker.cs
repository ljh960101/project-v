﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using System;
using UnityEngine.SceneManagement;
using MyTool;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using UnityEngine.Windows;
using System.IO;
#endif

public class ScriptMaker : MonoBehaviour
{
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    public GameObject scriptButton;
    public Transform panel_scriptMaker, panel_scriptEditor, panel_otherLimit, panel_chat;
    public GameMain gameMain;

    ChatScript currentScript;
    List<ChatScript> scripts;
    List<GameObject> sm_buttons, ol_buttons;

    OtherLevelLimit currentLevelLimit;
    List<OtherLevelLimit> limits;

    ScreenType currentScreen;
    float autoDelayValue;
    bool bCharCodeSort = true, bLevelSort = true, bPrioritySort = true;
    public void CharCodeSort(bool val)
    {
        bCharCodeSort = val;
        MakeButtons();
    }
    public void LevelSort(bool val)
    {
        bLevelSort = val;
        MakeButtons();
    }
    public void PrioritySort(bool val)
    {
        bPrioritySort = val;
        MakeButtons();
    }

    float counter;
    private void Update()
    {
        counter += Time.deltaTime;
        if (counter >= 30f)
        {
            counter -= 30f;
            Create();
        }
    }
    void LoadFile()
    {
        if (System.IO.File.Exists("scriptData.txt"))
        {
            
            Load();
        }
        else InitFile();
    }
    public void GotoGame()
    {
        Create();
        SceneManager.LoadScene("DeveloperMenu");
    }
    void SaveFile()
    {
        ScriptJsonData obj = new ScriptJsonData();
        obj.scripts = this.scripts.ToArray();
        System.IO.File.WriteAllText("scriptData.txt", JsonUtility.ToJson(obj));
    }
    void InitFile()
    {
        New();
        Create();
        ScriptJsonData obj = new ScriptJsonData();
        obj.scripts = this.scripts.ToArray();
        System.IO.File.WriteAllText("scriptData.txt", JsonUtility.ToJson(obj));
        LoadFile();
    }
    enum ScreenType
    {
        ScriptMaker = 0,
        ScriptEditor,
        OtherLevelEditor,
        SIMULATE
    }
    void OnApplicationQuit()
    {
        Create();
    }
    public void ScriptEditorButton(int sort)
    {
        int currentPosition = panel_scriptEditor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().caretPosition;
        string currentScript = panel_scriptEditor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().text;

        if (currentPosition < currentScript.Length && currentScript[currentPosition] != '\n')
        {
            for (int i = currentPosition; i < currentScript.Length; ++i)
            {
                if (currentScript[i] == '\n')
                {
                    currentPosition = i;
                    break;
                }
            }
            currentScript = currentScript.Insert(currentPosition, "\n");
            ++currentPosition;
        }

        switch (sort)
        {
            case 0:
                currentScript = currentScript.Insert(currentPosition, "[PC]");
                break;
            case 1:
                currentScript = currentScript.Insert(currentPosition, "[NPC]");
                break;
            case 2:
                currentScript = currentScript.Insert(currentPosition, "[L]");
                break;
            case 3:
                currentScript = currentScript.Insert(currentPosition, "[Ph 코드]");
                break;
            case 4:
                currentScript = currentScript.Insert(currentPosition, "[Pr]");
                break;
            case 5:
                currentScript = currentScript.Insert(currentPosition, "[Ad]");
                break;
            case 6:
                currentScript = currentScript.Insert(currentPosition, "[SS 안녕?$뭐해?$심심해!]\n[S 안녕?]\n[S 뭐해?]\n[S 심심해!]\n[SE]");
                break;
            case 7:
                currentScript = currentScript.Insert(currentPosition, "[SS 선택지$선택지$선택지]");
                break;
            case 8:
                currentScript = currentScript.Insert(currentPosition, "[S 선택지]");
                break;
            case 9:
                currentScript = currentScript.Insert(currentPosition, "[SE]");
                break;
            case 10:
                currentScript = currentScript.Insert(currentPosition, "[CN 이름]");
                break;
            case 11:
                currentScript = currentScript.Insert(currentPosition, "[CP 코드]");
                break;
            case 12:
                currentScript = currentScript.Insert(currentPosition, "[W 초]");
                break;
            case 13:
                int count = 0;
                int i = currentPosition;
                for (; i < currentScript.Length; i++)
                    if (currentScript[i] == '\n') break;

                if (i >= currentScript.Length) i = currentScript.Length - 1;
                for (; i>=0; --i)
                {
                    ++count;
                    if (currentScript[i] == '[') count = 0;
                    else if (currentScript[i] == '\n' && count >= 2) break;
                    else if (currentScript[i] == '\n' && count < 2) count = 0;
                }
                currentScript = currentScript.Insert(currentPosition, "[W " + (count*autoDelayValue) + "]");
                break;
        }
        panel_scriptEditor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().text = currentScript;
    }
    public void Simulate()
    {
        GotoMenu(3);
        Create();
    }
    public void ScriptChange(string script)
    {
        currentScript.scripts.Clear();
        currentScript.scripts.AddRange(script.Split('\n'));
        for (int i = currentScript.scripts.Count-1; i >= 0; --i)
        {
            if (currentScript.scripts[i] == "" || currentScript.scripts[i] == "\n")
                currentScript.scripts.Remove(currentScript.scripts[i]);
        }
    }
    public void LoveMin(string love)
    {
        currentLevelLimit.levelMin = int.Parse(love);
    }
    public void LoveMax(string love)
    {
        currentLevelLimit.levelMax = int.Parse(love);
    }
    public void Exit(int type)
    {
        GotoMenu(type);
    }
    public void Copy()
    {
        string newString = "";
        foreach (string str in currentScript.scripts)
        {
            newString += str + '\n';
        }
        GUIUtility.systemCopyBuffer = newString;
    }
    public UnityEngine.UI.Toggle waitCansleToggle;
    public void GotoMenu(int screenType)
    {
        currentScreen = (ScreenType)screenType;
        panel_otherLimit.gameObject.SetActive(false);
        panel_scriptEditor.gameObject.SetActive(false);
        panel_scriptMaker.gameObject.SetActive(false);
        panel_chat.gameObject.SetActive(false);
        switch (currentScreen)
        {
            case ScreenType.ScriptMaker:
                ResetToCurrentData();
                MakeButtons();
                panel_scriptMaker.gameObject.SetActive(true);
                break;
            case ScreenType.ScriptEditor:
                if(MessangerMain.GetInstance().am) MessangerMain.GetInstance().ChangeScreen(0);
                gameObject.GetComponent<UnityEngine.UI.CanvasScaler>().referenceResolution = new Vector2(320, 480);
                ResetToCurrentData();
                panel_scriptEditor.gameObject.SetActive(true);
                break;
            case ScreenType.OtherLevelEditor:
                limits = currentScript.otherLevelLimits;
                if (limits.Count > 0)
                {
                    ResetToCurrentData();
                }
                MakeButtons();
                panel_otherLimit.gameObject.SetActive(true);
                break;
            case ScreenType.SIMULATE:
                GameMain gm = GameMain.GetInstance();
                gm.gameStart = true;
                gm.mainData.characters = new List<MyCharacter>();
                gm.mainData.scripts = new List<ChatScript>();
                gm.mainData.chatLogs = new List<ChatLog>();
                MyCharacter testCharacter = new MyCharacter();
                testCharacter.charCode = 1;
                testCharacter.curMsgPic = "hs_01";
                testCharacter.love = 0;
                testCharacter.level = 1;
                testCharacter.name = "이름";
                testCharacter.snsId = "인스타아이디";
                testCharacter.bOnRand = true;
                testCharacter.bOnChat = true;
                testCharacter.bOpenSns = false;
                testCharacter.scriptCheck = new Dictionary<int, bool>();
                testCharacter.currentScriptCode = 1;
                testCharacter.currentScriptPosition = 0;
                testCharacter.currentScriptProcess = 0;
                testCharacter.isGuest = true;
                testCharacter.scriptPriority = 1;
                testCharacter.onOverlapScript = false;
                testCharacter.collectedPhotos = new List<string>();
                gm.mainData.characters.Add(testCharacter);

                bool noWait = waitCansleToggle.isOn;
                if (waitCansleToggle.isOn)
                {
                    gm.SetTypingSpeed(80f);
                }
                else
                {
                    gm.SetTypingSpeed(8f);
                }
                ChatScript testScript = new ChatScript();
                testScript.charCode = 1;
                testScript.scriptCode = 1;
                testScript.isMain = false;
                testScript.isOnlyOneTime = false;
                testScript.priority = 1;
                testScript.level = 1;
                testScript.otherLevelLimits = new List<OtherLevelLimit>();
                testScript.scripts = new List<string>();
                int test = 0;
                try
                {
                    foreach (string script in currentScript.scripts)
                    {
                        test = currentScript.scripts.IndexOf(script);
                        string str = script.Trim('\r');
                        if (noWait)
                        {
                            ScriptParsedData spd = ToolClass.ParseScript(str);
                            if (spd.type == ScriptType.WAIT)
                            {
                                str = "[W 0.2]";
                            }
                        }
                        testScript.scripts.Add(str);
                    }
                }
                catch
                {
                    Modal.GetInstance().CreateNewOkModal((test+1) + "번째줄 에서 오류 발생");
                }
                testScript.scriptEventType = ScriptEventType.NORMAL;
                gm.mainData.scripts.Add(testScript);

                gameMain.enabled = true;
                panel_chat.gameObject.SetActive(true);
                gameObject.GetComponent<UnityEngine.UI.CanvasScaler>().referenceResolution = new Vector2(1080, 1920);
                gm.currentApp = MessangerMain.GetInstance().gameObject;
                if (!MessangerMain.GetInstance().am)
                {
                    MessangerMain.GetInstance().Init();
                }
                MessangerMain.GetInstance().Resort();
                MessangerMain.GetInstance().OpenChat(1);
                MessangerMain.GetInstance().Reload();
                if (MessangerMain.GetInstance().transform.Find("Cover")) Destroy(MessangerMain.GetInstance().transform.Find("Cover").gameObject);
                break;
        }
        Create();
    }
    private void Start ()
    {
        gameMain.enabled = false;
        autoDelayValue = 0;
        sm_buttons = new List<GameObject>();
        ol_buttons = new List<GameObject>();
        scripts = new List<ChatScript>();
        LoadFile();
        GotoMenu(0);
    }
    public void AutoDelayVal(string val)
    {
        autoDelayValue = float.Parse(val);
    }
    public void New()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            currentScript = RenewCurrentScript();
            scripts.Add(currentScript);
            MakeButtons();
            ResetToCurrentData();
        }
        else
        {
            transform.Find("OtherLevel").Find("CharCode").GetComponent<UnityEngine.UI.InputField>().interactable = true;
            transform.Find("OtherLevel").Find("LoveMin").GetComponent<UnityEngine.UI.InputField>().interactable = true;
            transform.Find("OtherLevel").Find("LoveMax").GetComponent<UnityEngine.UI.InputField>().interactable = true;

            currentLevelLimit = RenewCurrentLimit();
            limits.Add(currentLevelLimit);
            MakeButtons();
            ResetToCurrentData();
        }
    }
    public void Delete()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            scripts.Remove(currentScript);
            if (scripts.Count > 0) currentScript = scripts[0];
            else currentScript = RenewCurrentScript();
            MakeButtons();
            ResetToCurrentData();
        }
        else
        {
            if (limits.Count <= 0) return;
            limits.Remove(currentLevelLimit);
            if (limits.Count > 0) currentLevelLimit = limits[0];
            else currentLevelLimit = null;

            if (currentLevelLimit != null)
            {
                transform.Find("OtherLevel").Find("CharCode").GetComponent<UnityEngine.UI.InputField>().interactable = false;
                transform.Find("OtherLevel").Find("LoveMin").GetComponent<UnityEngine.UI.InputField>().interactable = false;
                transform.Find("OtherLevel").Find("LoveMax").GetComponent<UnityEngine.UI.InputField>().interactable = false;
            }
            MakeButtons();
            ResetToCurrentData();
        }
    }
    public void Create()
    {
        /*ScriptJsonData obj = new ScriptJsonData();
        obj.scripts = this.scripts.ToArray();
        panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text = JsonUtility.ToJson(obj);*/
        //GUIUtility.systemCopyBuffer = panel_scriptMaker.Find("Code").GetComponent<UnityEngine.UI.InputField>().text;
        SaveFile();
    }
    public void Load()
    {
        try
        {
            scripts.Clear();
            scripts.AddRange(JsonUtility.FromJson<ScriptJsonData>(System.IO.File.ReadAllText("scriptData.txt")).scripts);
            if (scripts.Count > 0) currentScript = scripts[0];
            else New();
            MakeButtons();
            ResetToCurrentData();
        }
        catch
        {
            Modal.GetInstance().CreateNewOkModal("잘못된 코드.");
        }
    }
    public void OnlyOneTime(bool onlyOne)
    {
        currentScript.isOnlyOneTime = onlyOne;
    }
    public void MainQuest(bool mainQuest)
    {
        currentScript.isMain = mainQuest;
    }
    public void CharCode(string code)
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            currentScript.charCode = int.Parse(code);
            MakeButtons();
        }
        else
        {
            currentLevelLimit.chracterCode = int.Parse(code);
            MakeButtons();
        }
    }
    public void ScriptCode(string code)
    {
        currentScript.scriptCode = int.Parse(code);
        MakeButtons();
    }
    public void Priority(string priority)
    {
        currentScript.priority = int.Parse(priority);
        MakeButtons();
    }
    public void Level(string level)
    {
        currentScript.level = int.Parse(level);
        MakeButtons();
    }
    public void ScriptTypeCG(Int32 type)
    {
        currentScript.scriptEventType = (ScriptEventType)type;
    }
    public void OnButton(string index)
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            currentScript = scripts[int.Parse(index)];
            MakeButtons();
            ResetToCurrentData();
        }
        else
        {
            currentLevelLimit = limits[int.Parse(index)];
            MakeButtons();
            ResetToCurrentData();
        }
    }
    public GameObject indexAcker;
    void MakeButtons()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            foreach (GameObject button in sm_buttons)
            {
                Destroy(button);
            }
            sm_buttons.Clear();

            ChatScript tmp;
            if (bPrioritySort)
            {
                for (int i = 0; i < scripts.Count - 1; ++i)
                {
                    for (int j = 0; j < scripts.Count - i - 1; ++j)
                    {
                        if (scripts[j].priority > scripts[j + 1].priority)
                        {
                            tmp = scripts[j];
                            scripts[j] = scripts[j + 1];
                            scripts[j + 1] = tmp;
                        }
                    }
                }
            }
            if (bLevelSort)
            {
                for (int i = 0; i < scripts.Count - 1; ++i)
                {
                    for (int j = 0; j < scripts.Count - i - 1; ++j)
                    {
                        if (scripts[j].level > scripts[j + 1].level)
                        {
                            tmp = scripts[j];
                            scripts[j] = scripts[j + 1];
                            scripts[j + 1] = tmp;
                        }
                    }
                }
            }
            if (bCharCodeSort)
            {
                for (int i = 0; i < scripts.Count - 1; ++i)
                {
                    for (int j = 0; j < scripts.Count - i - 1; ++j)
                    {
                        if (scripts[j].charCode > scripts[j + 1].charCode)
                        {
                            tmp = scripts[j];
                            scripts[j] = scripts[j + 1];
                            scripts[j + 1] = tmp;
                        }
                    }
                }
            }

            int lastCharCode = -999, lastLevel = -999, lastPriority = -999;
            foreach (ChatScript script in scripts)
            {
                if (lastCharCode != script.charCode && bCharCodeSort)
                {
                    lastCharCode = script.charCode;
                    GameObject obj = Instantiate(indexAcker, panel_scriptMaker.Find("List").Find("GirdWithElements"));
                    obj.name = scripts.IndexOf(script) + "Index";
                    obj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = script.charCode + "";
                    obj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().color = new Color(1f, 0f, 0f);
                    sm_buttons.Add(obj);
                }
                if (lastLevel != script.level && bLevelSort)
                {
                    lastLevel = script.level;
                    GameObject obj = Instantiate(indexAcker, panel_scriptMaker.Find("List").Find("GirdWithElements"));
                    obj.name = scripts.IndexOf(script) + "Index";
                    obj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = script.level + "";
                    obj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().color = new Color(0f, 1f, 0f);
                    sm_buttons.Add(obj);
                }
                if (lastPriority != script.priority && bPrioritySort)
                {
                    lastPriority = script.priority;
                    GameObject obj = Instantiate(indexAcker, panel_scriptMaker.Find("List").Find("GirdWithElements"));
                    obj.name = scripts.IndexOf(script) + "Index";
                    obj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = script.priority + "";
                    obj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().color = new Color(0f, 0f, 1f);
                    sm_buttons.Add(obj);
                }
                GameObject newObj = Instantiate(scriptButton, panel_scriptMaker.Find("List").Find("GirdWithElements"));
                newObj.name = scripts.IndexOf(script) + "";
                newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = script.scriptCode + "";
                newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnButton(newObj.name); });
                sm_buttons.Add(newObj);
            }
        }
        else
        {
            foreach (GameObject button in ol_buttons)
            {
                Destroy(button);
            }
            ol_buttons.Clear();

            limits.Sort(delegate (OtherLevelLimit A, OtherLevelLimit B)
            {
                if (A.chracterCode > B.chracterCode) return 1;
                else if (A.chracterCode < B.chracterCode) return -1;
                else return 0;
            });
            foreach (OtherLevelLimit limit in limits)
            {
                GameObject newObj = Instantiate(scriptButton, panel_otherLimit.Find("List").Find("GirdWithElements"));
                newObj.name = limits.IndexOf(limit) + "";
                newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = limit.chracterCode + "";
                newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnButton(newObj.name); });
                ol_buttons.Add(newObj);
            }
        }
    }
    void ResetToCurrentData()
    {
        if (currentScreen == ScreenType.ScriptMaker)
        {
            panel_scriptMaker.Find("CharCode").GetComponent<UnityEngine.UI.InputField>().text = currentScript.charCode + "";
            panel_scriptMaker.Find("ScriptCode").GetComponent<UnityEngine.UI.InputField>().text = currentScript.scriptCode + "";
            panel_scriptMaker.Find("Priority").GetComponent<UnityEngine.UI.InputField>().text = currentScript.priority + "";
            panel_scriptMaker.Find("Level").GetComponent<UnityEngine.UI.InputField>().text = currentScript.level + "";
            panel_scriptMaker.Find("MainQuest").GetComponent<UnityEngine.UI.Toggle>().isOn = currentScript.isMain;
            panel_scriptMaker.Find("ScriptType").GetComponent<UnityEngine.UI.Dropdown>().value = (Int32)currentScript.scriptEventType;
            panel_scriptMaker.Find("OnlyOne").GetComponent<UnityEngine.UI.Toggle>().isOn = currentScript.isOnlyOneTime;
        }
        else if(currentScreen == ScreenType.OtherLevelEditor)
        {
            if (limits.Count <= 0) return;

            panel_otherLimit.Find("CharCode").GetComponent<UnityEngine.UI.InputField>().text = currentLevelLimit.chracterCode + "";
            panel_otherLimit.Find("LoveMin").GetComponent<UnityEngine.UI.InputField>().text = currentLevelLimit.levelMin + "";
            panel_otherLimit.Find("LoveMax").GetComponent<UnityEngine.UI.InputField>().text = currentLevelLimit.levelMax + "";
        }
        else if(currentScreen == ScreenType.ScriptEditor)
        {
            string myScript = "";
            foreach(string script in currentScript.scripts)
            {
                myScript += script + '\n';
            }
            panel_scriptEditor.Find("ScriptField").GetComponent<UnityEngine.UI.InputField>().text = myScript;
        }
    }
    // Change Screen
    public void OtherCharLevel()
    {
        GotoMenu(2);
    }
    public void Goto_CharacterMaker()
    {
        Create();
        SceneManager.LoadScene("Character Maker");
    }
    public void Script()
    {
        GotoMenu(1);
    }
    OtherLevelLimit RenewCurrentLimit()
    {
        OtherLevelLimit newLimit = new OtherLevelLimit();
        newLimit.chracterCode = 0;
        newLimit.levelMax = 100;
        newLimit.levelMin = 1;
        return newLimit;
    }
    ChatScript RenewCurrentScript()
    {
        ChatScript newScript = new ChatScript();
        newScript.scriptCode = 0;
        newScript.charCode = 0;
        newScript.isMain = false;
        newScript.isOnlyOneTime = false;
        newScript.priority = 1;
        newScript.level = 0;
        newScript.otherLevelLimits = new List<OtherLevelLimit>();
        newScript.scripts = new List<string>();
        newScript.scriptEventType = ScriptEventType.NORMAL;
        return newScript;
    }
    public void GotoMerger()
    {
        Create();
        SceneManager.LoadScene("Merger");
    }
#endif
}
