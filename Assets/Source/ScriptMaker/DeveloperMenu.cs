﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeveloperMenu : MonoBehaviour
{
    public void GotoCharacterMaker()
    {
        SceneManager.LoadScene("Character Maker");
    }
    public void GotoScriptMaker()
    {
        SceneManager.LoadScene("Script Maker");
    }
    public void GotoSnsMaker()
    {
        SceneManager.LoadScene("Sns Maker");
    }
    public void GotoMerger()
    {
        SceneManager.LoadScene("Merger");
    }
    public void GotoGame()
    {
        SceneManager.LoadScene("Main");
    }
}
