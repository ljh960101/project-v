﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace MyTool
{
    public static class ToolClass
    {
        static public bool BackButtonAbleChecking()
        {
            return (GameObject.FindObjectOfType<OkModal>() == null && GameObject.FindObjectOfType<ExitModal>() == null && GameObject.FindObjectOfType<EndingModal>() == null && GameObject.FindObjectOfType<ImageModal>() == null);
        }
        static public void MakeVibration()
        {
            if (GameMain.GetInstance().mainData.settingData.vibration)
            {
#if UNITY_ANDROID
                Handheld.Vibrate();
#endif
            }
        }
        static public void Save()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/data.dat");
            MainData fileData = GameMain.GetInstance().mainData;
            bf.Serialize(file, fileData);
            file.Close();
        }
        static public void Load()
        {
            if (!GameMain.GetInstance().resetGameOnPlay && File.Exists(Application.persistentDataPath + "/data.dat") && !(GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/data.dat", FileMode.Open);
                MainData fileData = (MainData)bf.Deserialize(file);
                GameMain.GetInstance().mainData = fileData;
                file.Close();
            }
            else
            {
                Reset();
            }
        }
        static public void Reset()
        {
            MainData mainData = new MainData();
            GameMain.GetInstance().mainData = mainData;

            mainData.snsId = "[Default]";
            mainData.msgName = "[Default]";
            //mainData.snsId = "ljh960101";
            //mainData.msgName = "횽이";
            mainData.trigger = new Dictionary<int, bool>();
            ///////// RANDOM CHAT TEST
            mainData.characters = new List<MyCharacter>();
            mainData.scripts = new List<ChatScript>();
            mainData.snsLogs = new List<SnsLog>();
            mainData.snsAlrams = new List<SnsAlram>();
            mainData.randomChatLogs = new List<ChatLog>();
            mainData.chatLogs = new List<ChatLog>();
            mainData.appAlramLogs = new Dictionary<AppType, int>();

            mainData.miniGameData = new MiniGameData();
            mainData.miniGameData.characters = new List<MiniGameCharacter>();
            mainData.miniGameData.scores = new List<MiniGameScore>();
            mainData.posts = new List<SnsPostData>();
            mainData.settingData = new SettingData();
            mainData.settingData.androidAlram = true;
            mainData.settingData.gameAlram = true;
            mainData.settingData.sound = 1.0f;
            mainData.settingData.vibration = true;
            mainData.settingData.speed = 1.0f;

            mainData.achMainData = new AchMainData();
            mainData.achMainData.achDatas = new Dictionary<int, AchData>();
            mainData.achMainData.finishList = new List<AchData>();
            mainData.achMainData.lanchAppChecker = new Dictionary<AppType, bool>();

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
            if (GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>())
            {
                if (File.Exists("scriptData.txt") && File.Exists("characterData.txt"))
                {
                    mainData.scripts.AddRange(JsonUtility.FromJson<ScriptJsonData>(File.ReadAllText("scriptData.txt")).scripts);
                    mainData.characters.AddRange(JsonUtility.FromJson<CharaterJsonData>(File.ReadAllText("characterData.txt")).characters);
                    mainData.posts.AddRange(JsonUtility.FromJson<SnsJsonData>(File.ReadAllText("postData.txt")).posts);
                }

                for (int i = 1; i <= 8; ++i)
                {
                    MiniGameCharacter newCharacter = new MiniGameCharacter();
                    newCharacter.charCode = i;
                    newCharacter.level = 4;
                    newCharacter.love = 0;
                    newCharacter.priority = 1;
                    newCharacter.miniGameCharacterCode = i;
                    newCharacter.onAlram = false;
                    newCharacter.onUnlocked = false;
                    mainData.miniGameData.characters.Add(newCharacter);
                    //if (i == 1) newCharacter.onUnlocked = true;
                }
                mainData.miniGameData.currentCharacterCode = 1;
                {
                    MiniGameScore t = new MiniGameScore();
                    t.charCode = 0;
                    t.lastScriptCounter = 0;
                    t.score = 5;
                    t.scoredDate = DateTime.Now;
                    mainData.miniGameData.scores.Add(t);
                }
                {
                    MiniGameScore t = new MiniGameScore();
                    t.charCode = 1;
                    t.lastScriptCounter = 0;
                    t.score = 6;
                    t.scoredDate = DateTime.Now;
                    mainData.miniGameData.scores.Add(t);
                }
            }
            else
            {
                if (File.Exists("scriptData.txt") && File.Exists("characterData.txt"))
                {
                    mainData.scripts.AddRange(JsonUtility.FromJson<ScriptJsonData>(File.ReadAllText("scriptData.txt")).scripts);
                    mainData.characters.AddRange(JsonUtility.FromJson<CharaterJsonData>(File.ReadAllText("characterData.txt")).characters);
                    mainData.posts.AddRange(JsonUtility.FromJson<SnsJsonData>(File.ReadAllText("postData.txt")).posts);
                }
                else
                {
                    mainData.scripts.AddRange(JsonUtility.FromJson<ScriptJsonData>(Resources.Load<TextAsset>("Data/scriptData").text).scripts);
                    mainData.characters.AddRange(JsonUtility.FromJson<CharaterJsonData>(Resources.Load<TextAsset>("Data/characterData").text).characters);
                    mainData.posts.AddRange(JsonUtility.FromJson<SnsJsonData>(Resources.Load<TextAsset>("Data/postData").text).posts);
                }
            }
#else
                mainData.scripts.AddRange(JsonUtility.FromJson<ScriptJsonData>(Resources.Load<TextAsset>("Data/scriptData").text).scripts);
                mainData.characters.AddRange(JsonUtility.FromJson<CharaterJsonData>(Resources.Load<TextAsset>("Data/characterData").text).characters);
                mainData.posts.AddRange(JsonUtility.FromJson<SnsJsonData>(Resources.Load<TextAsset>("Data/postData").text).posts);
#endif

            foreach (MyCharacter character in mainData.characters)
            {
                character.scriptCheck = new Dictionary<int, bool>();
                character.collectedPhotos = new List<string>();
            }
            foreach (MyCharacter character in mainData.characters)
            {
                if (character.currentScriptCode != 0)
                {
                    ChatScript myScript = ToolClass.GetScript(character.currentScriptCode);
                    character.level = myScript.level;
                    character.scriptPriority = myScript.priority;
                    character.scriptCheck[character.currentScriptCode] = true;
                    ToolClass.CollectPhoto(character.charCode, character.curMsgPic);
                    ToolClass.CollectPhoto(character.charCode, character.curSnsPic);
                }
            }
            if (!(GameMain.GetInstance().GetComponent<ScriptMaker>() || GameMain.GetInstance().GetComponent<SnsMaker>()))
            {
                for (int i = 1; i <= 8; ++i)
                {
                    if(i==2 || i==3 || i==5) continue;
                    MiniGameCharacter newCharacter = new MiniGameCharacter();
                    newCharacter.charCode = i;
                    newCharacter.level = 4;
                    newCharacter.love = 0;
                    newCharacter.priority = 1;
                    newCharacter.miniGameCharacterCode = i;
                    newCharacter.onAlram = false;
                    newCharacter.onUnlocked = false;
                    mainData.miniGameData.characters.Add(newCharacter);
                    if (i == 1) newCharacter.onUnlocked = true;
                }
                mainData.miniGameData.currentCharacterCode = 1;
                {
                    MiniGameScore t = new MiniGameScore();
                    t.charCode = 0;
                    t.lastScriptCounter = 0;
                    t.score = 10;
                    t.scoredDate = DateTime.Now;
                    mainData.miniGameData.scores.Add(t);
                }

                for (int i = 1; i <= 0; ++i)
                {
                    SnsPostData spd = new SnsPostData();
                    spd.content = "테스트\n너모 싫다\n시밤바";
                    spd.defaulatLike = 5;
                    spd.level = 0;
                    spd.picCode = "sw_02";
                    spd.postCode = i;
                    spd.charCode = 1;
                    spd.postScripts = new List<PostScript>();
                    PostScript test = new PostScript();
                    test.currentPos = 0;
                    test.currentProcess = 0f;
                    test.isOver = false;
                    test.scripts = new List<string>();
                    test.scripts.Add("[SS 안녕$하세$요?]");
                    test.scripts.Add("[S 안녕]");
                    test.scripts.Add("[COMMENT @ljh960101$S1]");
                    test.scripts.Add("[S 하세]");
                    test.scripts.Add("[COMMENT @lol123$S2]");
                    test.scripts.Add("[S 요?]");
                    test.scripts.Add("[COMMENT @DA$S3]");
                    test.scripts.Add("[SE]");
                    test.scripts.Add("[COMMENT @lol123$끝]");
                    spd.postScripts.Add(test);
                    spd.priority = 1;
                    mainData.posts.Add(spd);
                }
            }
            InitAch();
            mainData.lastFrameTime = DateTime.Now;
        }
        static public void InitAch()
        {
            var mainData = GameMain.GetInstance().mainData;
            int count = 1;
            {
                AchData ad = new AchData();
                ad.achType = AchType.LoginMany;
                ad.arcValue = 1;
                ad.score = 1;
                ad.arcName = "널 만나러 왔어";
                ad.arcContent = "내폰속로맨스 첫 시작";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.LoginMany;
                ad.arcValue = 20;
                ad.score = 3;
                ad.arcName = "또 만나러 왔어";
                ad.arcContent = "내폰속로맨스 20회 플레이";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.LoginMany;
                ad.arcValue = 60;
                ad.score = 7;
                ad.arcName = "생각나서 또 왔어";
                ad.arcContent = "내폰속로맨스 60회 플레이";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.LoginMany;
                ad.arcValue = 100;
                ad.score = 10;
                ad.arcName = "내폰로가 없으면 안돼!";
                ad.arcContent = "내폰속로맨스 100회 플레이";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.LanchApp;
                ad.arcValue = (int)AppType.Max - 4;
                ad.score = 10;
                ad.arcName = "모두 다 눌러보겠어!";
                ad.arcContent = "비밀 어플을 제외한 모든 어플을 실행";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.RandomChatMany;
                ad.arcValue = 10;
                ad.score = 5;
                ad.arcName = "랜덤채팅의 시작";
                ad.arcContent = "랜덤채팅 10명 매칭";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.RandomChatMany;
                ad.arcValue = 20;
                ad.score = 10;
                ad.arcName = "랜덤채팅 마스터";
                ad.arcContent = "랜덤채팅 20명 매칭";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.MiniGameScoreAch;
                ad.arcValue = 100;
                ad.score = 3;
                ad.arcName = "이정도는 간단하지";
                ad.arcContent = "미니게임 100점 달성";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.MiniGameScoreAch;
                ad.arcValue = 250;
                ad.score = 5;
                ad.arcName = "이것이 내 실력이다!";
                ad.arcContent = "미니게임 250점 달성";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.CharacterLevel;
                ad.arcValue = 3;
                ad.score = 2;
                ad.arcName = "우리는 모두 친구";
                ad.arcContent = "모든 캐릭터 [친구] 달성";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.CharacterLevel;
                ad.arcValue = 5;
                ad.score = 3;
                ad.arcName = "이건 무슨 기분일까";
                ad.arcContent = "모든 캐릭터 [썸] 달성";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.CharacterLevel;
                ad.arcValue = 6;
                ad.score = 5;
                ad.arcName = "오늘부터 우리는..?";
                ad.arcContent = "모든 캐릭터 [연인] 달성";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.CollectPic;
                ad.arcValue = 10;
                ad.score = 3;
                ad.arcName = "수집가";
                ad.arcContent = "CG 50% 수집";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.CollectPic;
                ad.arcValue = 20;
                ad.score = 5;
                ad.arcName = "달리는 수집가";
                ad.arcContent = "CG 70% 수집";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.CollectPic;
                ad.arcValue = 30;
                ad.score = 7;
                ad.arcName = "완벽해져가는 수집가";
                ad.arcContent = "CG 90% 수집";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.EndingMany;
                ad.arcValue = 1;
                ad.score = 1;
                ad.arcName = "사랑의 시작";
                ad.arcContent = "엔딩 1회";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.EndingMany;
                ad.arcValue = 3;
                ad.score = 3;
                ad.arcName = "이번엔 너로 정했다!";
                ad.arcContent = "엔딩 3회";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.EndingMany;
                ad.arcValue = 5;
                ad.score = 7;
                ad.arcName = "사랑은 갈등의 반복";
                ad.arcContent = "엔딩 5회";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            {
                AchData ad = new AchData();
                ad.achType = AchType.EndingMany;
                ad.arcValue = 8;
                ad.score = 10;
                ad.arcName = "내 사랑은..!";
                ad.arcContent = "모든 캐릭터 엔딩";
                ad.arcCode = count++;
                mainData.achMainData.achDatas[ad.arcCode] = ad;
            }
            mainData.achMainData.loginCount += 1;
        }
        static public void PlaySound(AudioClip audioClip)
        {
            var soundVolume = GameMain.GetInstance().mainData.settingData.sound;
            if (soundVolume > 0.0f)
            {
                GameMain.GetInstance().GetComponent<AudioSource>().PlayOneShot(audioClip, soundVolume);
            }
        }
        static public void CollectPhoto(int charCode, string picCode)
        {
            if (picCode == "0" || picCode == "1") return;
            MyCharacter character = GetCharacter(charCode);
            if (character.collectedPhotos!=null && character.collectedPhotos.IndexOf(picCode) == -1)
            {
                character.collectedPhotos.Add(picCode);
            }
            PhotoMain.GetInstance().Reload();
        }
        static public int GetMiniGameScore(int charCode = 0)
        {
            int val = -1;
            var scores = GameMain.GetInstance().mainData.miniGameData.scores;
            foreach(var score in scores)
            {
                if(score.charCode == charCode)
                {
                    val = score.score;
                }
            }
            if(val == -1)
            {
                Debug.Log("미진행된 캐릭터 코드 : " + charCode);
            }
            return val;
        }
        static public string MakeStringPretty(string orginString, int lineLimit = 9999, bool bOnRemove = true, int rowLimit = 9999)
        {
            string newText = orginString;
            int count = 0;
            
            // 한줄당 N개의 글자수로 제한
            for (int i = 0; i < newText.Length; ++i)
            {
                ++count;
                if (newText[i] == '\n')
                {
                    count = 0;
                }
                if (count >= lineLimit)
                {
                    // 지우기
                    if (bOnRemove)
                    {
                        newText = newText.Remove(i, 1);
                        --i;
                    }
                    // 줄바꾸기
                    else if(i != newText.Length-1)
                    {
                        if (i == newText.Length - 1) break;
                        newText = newText.Insert(i, "\n");
                        count = 0;
                    }
                    continue;
                }
            }
            count = 0;

            // rowLimit 줄로 자름
            for (int i = 0; i < newText.Length; ++i)
            {
                if (newText[i] == '\n')
                {
                    ++count;
                    if (count >= rowLimit)
                    {
                        newText = newText.Substring(0, i);
                        break;
                    }
                }
            }

            return newText;
        }
        static public void MakeNotify(AppType appCode, string text, int code = -1)
        {
            GameMain gm = GameMain.GetInstance();
            if (gm.currentApp && (gm.currentApp.GetComponent<Home>() || (gm.currentApp.GetComponent<AppMain>().appType != appCode || gm.currentApp.GetComponent<AppMain>().appType == AppType.Timer || gm.currentApp.GetComponent<AppMain>().appType == AppType.Sns)))
            {
                gm.notify.ShowNotify(appCode, text, code);
            }
        }
        static public String ParseToSmallScript(string script)
        {
            ScriptParsedData spd = ParseScript(script);
            switch (spd.type)
            {
                case ScriptType.NORMAL:
                    return spd.val;
                case ScriptType.PHOTO:
                    return "(사진)";
                case ScriptType.PROFILE:
                    return "(프로필)";
                default:
                    return "";
            }
        }
        static public ScriptParsedData ParseScript(string script)
        {
            string upperString = script.ToUpper();
            ScriptParsedData returnData = new ScriptParsedData();
            // 특수 채팅
            if (upperString[0] == '[')
            {
                if (upperString == "[PC]")
                {
                    returnData.type = ScriptType.PC_START;
                }
                else if (upperString == "[NPC]")
                {
                    returnData.type = ScriptType.NPC_START;
                }
                else if (upperString == "[SE]")
                {
                    returnData.type = ScriptType.SELECT_END;
                }
                else if (upperString == "[L]")
                {
                    returnData.type = ScriptType.LINE;
                }
                else if (upperString == "[D]")
                {
                    returnData.type = ScriptType.DEAD;
                }
                else if (upperString == "[PR]")
                {
                    returnData.type = ScriptType.PROFILE;
                }
                else if (upperString == "[AD]")
                {
                    returnData.type = ScriptType.ADRESS;
                }
                else if (upperString == "[GAME_START]")
                {
                    returnData.type = ScriptType.GAME_START;
                }
                else if (upperString == "[GAME_HIGH]")
                {
                    returnData.type = ScriptType.GAME_HIGH;
                }
                else if (upperString == "[GAME_SAME]")
                {
                    returnData.type = ScriptType.GAME_SAME;
                }
                else if (upperString == "[ENDING]")
                {
                    returnData.type = ScriptType.ENDING;
                }
                else if (upperString == "[GAME_LOW]")
                {
                    returnData.type = ScriptType.GAME_LOW;
                }
                else if (upperString == "[GAME_IGNORE]")
                {
                    returnData.type = ScriptType.GAME_IGNORE;
                }
                else if (upperString == "[GAME_END]")
                {
                    returnData.type = ScriptType.GAME_END;
                }
                else if (upperString[1] == 'N' && upperString[2] == 'P' && upperString[3] == 'C' && upperString[4] == ' ' )
                {
                    string removeNeedString = "[NPC ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.NPC_NOREGI;
                    returnData.val = script;
                }
                else if (upperString[1] == 'I' && upperString[2] == 'F')
                {
                    string removeNeedString = "[IF ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.IF;
                    returnData.val = script;
                }
                else if (upperString[1] == 'L' && upperString[2] == 'I')
                {
                    string removeNeedString = "[LIKE ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.Like;
                    returnData.val = script;
                }
                else if (upperString[1] == 'C' && upperString[2] == 'O')
                {
                    string removeNeedString = "[COMMENT ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.COMMENT;
                    returnData.val = script;
                }
                else if (upperString[1] == 'W')
                {
                    string removeNeedString = "[W ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.WAIT;
                    returnData.val = script;
                }
                else if (upperString[1] == 'L' && upperString[2] == ' ')
                {
                    string removeNeedString = "[L ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.LOVE;
                    returnData.val = script;
                }
                else if (upperString[1] == 'S' && upperString[2] == ' ')
                {
                    string removeNeedString = "[S ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.SELECT_LIST;
                    returnData.val = script;
                }
                else if (upperString[1] == 'S' && upperString[2] == 'S')
                {
                    string removeNeedString = "[SS ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.SELECT_START;
                    returnData.val = script;
                }
                else if (upperString[1] == 'P' && upperString[2] == 'H')
                {
                    string removeNeedString = "[PH ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.PHOTO;
                    returnData.val = script;
                }
                else if (upperString[1] == 'C' && upperString[2] == 'N')
                {
                    string removeNeedString = "[CN ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.CHANGE_NAME;
                    returnData.val = script;
                }
                else if (upperString[1] == 'C' && upperString[2] == 'P')
                {
                    string removeNeedString = "[CP ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.CHANGE_PHOTO;
                    returnData.val = script;
                }
                else if (upperString[1] == 'C' && upperString[2] == 'S')
                {
                    string removeNeedString = "[CS ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.CHANGE_STATUS;
                    returnData.val = script;
                }
                else if (upperString[1] == 'G' && upperString[2] == 'L')
                {
                    string removeNeedString = "[GL ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.GOTO_LINE;
                    returnData.val = script;
                }
                else if (upperString[1] == 'T' && upperString[2] == 'O')
                {
                    string removeNeedString = "[TO ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.TRIGGER_ON;
                    returnData.val = script;
                }
                else if (upperString[1] == 'T' && upperString[2] == 'X')
                {
                    string removeNeedString = "[TX ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.TRIGGER_OFF;
                    returnData.val = script;
                }
                else if (upperString[1] == 'W' && upperString[2] == 'T')
                {
                    string removeNeedString = "[WT ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.WAIT_TRIGGER;
                    returnData.val = script;
                }
                else if (upperString == "[GAME_NPC]")
                {
                    returnData.type = ScriptType.GAME_NPC;
                }
                else if (upperString == "[GAME_PC]")
                {
                    returnData.type = ScriptType.GAME_PC;
                }
                else if (upperString[1] == 'G' && upperString[2] == 'A' && upperString[6] == 'N')
                {
                    string removeNeedString = "[GAME_NPC ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.GAME_NPC;
                    returnData.val = script;
                }
                else if (upperString[1] == 'G' && upperString[2] == 'A' && upperString[6] == 'P')
                {
                    string removeNeedString = "[GAME_PC ";
                    int i = upperString.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);
                    removeNeedString = "]";
                    i = script.IndexOf(removeNeedString);
                    script = script.Remove(i, removeNeedString.Length);

                    returnData.type = ScriptType.GAME_PC;
                    returnData.val = script;
                }
            }
            else
            {
                string newString = script.Replace("$NAME", GameMain.GetInstance().mainData.msgName);
                newString = newString.Replace("$ID", GameMain.GetInstance().mainData.snsId);

                returnData.type = ScriptType.NORMAL;
                returnData.val = newString;
            }

            return returnData;
        }
        static public ChatType GetCharacterChatType(int charCode)
        {
            MainData mainData = GameMain.GetInstance().mainData;
            foreach (MyCharacter character in mainData.characters)
            {
                if (character.charCode == charCode)
                {
                    if (character.bOnChat)
                    {
                        return ChatType.CHAT;
                    }
                    else if (character.bOnRand && !character.bOnChat)
                    {
                        return ChatType.RANDCHAT;
                    }
                    else if (!character.bOnRand && !character.bOnChat)
                    {
                        return ChatType.NO_CHAT;
                    }
                }
            }
            throw new Exception("There's no matched charcode.");
        }
        static public ChatType GetCharacterChatType(MyCharacter character)
        {
            if (character.bOnChat)
            {
                return ChatType.CHAT;
            }
            else if (character.bOnRand && !character.bOnChat)
            {
                return ChatType.RANDCHAT;
            }
            else if (!character.bOnRand && !character.bOnChat)
            {
                return ChatType.NO_CHAT;
            }
            throw new Exception("There's no matched charcode.");
        }
        public static string GetName(int charCode, ChatType chatType)
        {
            MainData mainData = GameMain.GetInstance().mainData;
            for (int i = 0; i < mainData.characters.Count; ++i)
            {
                if (charCode == mainData.characters[i].charCode)
                {
                    switch (chatType)
                    {
                        case ChatType.RANDCHAT:
                            return "낯선 사람";
                        //return mainData.characters[i].randName;
                        case ChatType.CHAT:
                            return mainData.characters[i].name;
                        case ChatType.SNS:
                            return mainData.characters[i].snsId;
                    }
                }
            }
            return "Wrong Code";
        }
        static public ChatScript GetScript(int scriptCode)
        {
            MainData mainData = GameMain.GetInstance().mainData;
            foreach (ChatScript script in mainData.scripts)
            {
                if (script.scriptCode == scriptCode)
                    return script;
            }
            return null;
        }
        static public MyCharacter GetCharacter(int charCode)
        {
            MainData mainData = GameMain.GetInstance().mainData;
            foreach (MyCharacter ch in mainData.characters)
            {
                if (ch.charCode == charCode)
                    return ch;
            }
            return null;
        }
        static public SnsPostData GetSnsPost(int postCode)
        {
            MainData mainData = GameMain.GetInstance().mainData;
            foreach (SnsPostData spd in mainData.posts)
            {
                if (spd.postCode == postCode)
                    return spd;
            }
            return null;
        }
        static public SnsLog GetSnsPostLog(int postCode)
        {
            MainData mainData = GameMain.GetInstance().mainData;
            foreach (SnsLog spd in mainData.snsLogs)
            {
                if (spd.postCode == postCode)
                    return spd;
            }
            return null;
        }
    }
}
