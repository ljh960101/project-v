﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInOutSystem : MonoBehaviour {
    FadeOutSystem fos;
    bool onFadeOut;
	void Start () {
        fos = GetComponent<FadeOutSystem>();
        fos.Init(1f, false, 1.0f, false);
        fos.FadeIn( delegate { gameObject.SetActive(true); });
        onFadeOut = false;
    }
    private void Update()
    {
        if (!fos.bOnFade)
        {
            if (onFadeOut)
            {
                onFadeOut = !onFadeOut;
                fos.FadeIn(delegate { gameObject.SetActive(true); });
            }
            else
            {
                onFadeOut = !onFadeOut;
                fos.FadeOut(delegate { gameObject.SetActive(true); });
            }
        }
    }
}