﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchToStart : MonoBehaviour {
    public FadeOutSystem fos;
    public UnityEngine.UI.Scrollbar sl;
    public UnityEngine.UI.Image rndImage;
	void Start () {
        GetComponent<FadeOutSystem>().Init(1.0f, true, 1.0f, false);
        SetRandImage();
        fos.Init(1.0f, true, 1.0f, false);
        fos.FadeIn();
    }
    void SetRandImage()
    {
        var characters = GameMain.GetInstance().mainData.characters;
        List<string> photos = new List<string>();
        foreach(var character in characters)
        {
            foreach(var photo in character.collectedPhotos)
            {
                photos.Add(photo);
            }
        }
        if (photos.Count == 0) rndImage.transform.parent.gameObject.SetActive(false);
        else rndImage.sprite = Resources.Load<Sprite>("Image/" + photos[UnityEngine.Random.Range(0, photos.Count)]);
    }
    public void SliderChanged(float val)
    {
        if (val >= 1f)
        {
            Close();
        }
    }
    bool onDown = false;
    public void Update()
    {
        if (Input.GetMouseButtonDown(0)) onDown = true;
        if (Input.GetMouseButtonUp(0)) onDown = false;

        if (!fos.bOnFade && Input.touchCount==0 && !onDown)
        {
            sl.value = Mathf.Lerp(sl.value, 0, 0.5f);
        }
    }
    public void Close()
    {
        GetComponent<FadeOutSystem>().Init(2.0f, true, 1.0f, false);
        GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.MoveFromDown);
        GetComponent<FadeOutSystem>().FadeOut(delegate {
            if(GameMain.GetInstance().mainData.msgName != "[Default]") GameMain.GetInstance().gameStart = true; } );
        Home.GetInstance().GetComponent<FadeOutSystem>().FadeIn();
    }
}
