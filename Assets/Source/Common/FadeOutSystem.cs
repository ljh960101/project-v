﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutSystem : MonoBehaviour
{
    const float globalSpeed = 1f;

    bool isFirst = true;
    Home myHome;
    Vector2 basicScale;
    float speed;
    ChangeType currentType;
    float fadeOutLimit;
    [HideInInspector] public bool bOnFade;
    CanvasGroup trans;
    float currentOpaicty;
    bool bOndestory;
    bool noOpacity;
    [HideInInspector] public bool bFadeOut;

    Vector2 basicPos, basicOffset1, basicOffset2;
    Vector2 beforePos, beforeOffset1, beforeOffset2;

    public delegate void VoidFunction();
    public List<VoidFunction> after;

    public enum ChangeType
    {
        Fade,
        Zoom,
        MoveFromLeft,
        MoveFromRight,
        MoveFromTop,
        MoveFromDown,
    };
    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }
    public void Reset()
    {
        GetComponent<CanvasGroup>().alpha = 1f;
        ((RectTransform)transform).anchoredPosition = basicPos;
    }
    public void SetType(ChangeType type)
    {
        currentType = type;
    }
    public void FadeOut(VoidFunction callBackFunc = null)
    {
        if (callBackFunc != null) after.Add(callBackFunc);
        trans.interactable = false;
        if (currentType == ChangeType.Zoom)
        {
            ((RectTransform)transform).localScale = basicScale;
        }
        float width = 1080;
        float height = (Screen.height * 1080f / Screen.width);
        if (currentType == ChangeType.Zoom)
        {
            ((RectTransform)transform).localScale = Vector2.zero;
        }
        else if (currentType == ChangeType.MoveFromDown)
        {
            beforePos = basicPos + new Vector2(0, height);
            beforeOffset1 = basicPos + new Vector2(0, height);
            beforeOffset2 = basicPos + new Vector2(0, height);
        }
        else if (currentType == ChangeType.MoveFromTop)
        {
            beforePos = basicPos - new Vector2(0, height);
            beforeOffset1 = basicPos - new Vector2(0, height);
            beforeOffset2 = basicPos - new Vector2(0, height);
        }
        else if (currentType == ChangeType.MoveFromLeft)
        {
            beforePos = basicPos - new Vector2(width, 0);
            beforeOffset1 = basicPos - new Vector2(width, 0);
            beforeOffset2 = basicPos - new Vector2(width, 0);
        }
        else if (currentType == ChangeType.MoveFromRight)
        {
            beforePos = basicPos + new Vector2(width, 0);
            beforeOffset1 = basicPos + new Vector2(width, 0);
            beforeOffset2 = basicPos + new Vector2(width, 0);
        }

        bFadeOut = true;
        bOnFade = true;
        currentOpaicty = fadeOutLimit;
        gameObject.SetActive(true);
    }

    public void FadeIn(VoidFunction callBackFunc = null)
    {
        if (callBackFunc != null) after.Add(callBackFunc);
        if (myHome) myHome.gm.currentApp = gameObject;
        float width = 1080;
        float height = (Screen.height * 1080f / Screen.width);
        if (currentType == ChangeType.Zoom)
        {
            ((RectTransform)transform).localScale = Vector2.zero;
        }
        else if (currentType == ChangeType.MoveFromDown)
        {
            beforePos = basicPos + new Vector2(0, height);
            beforeOffset1 = basicPos + new Vector2(0, height);
            beforeOffset2 = basicPos + new Vector2(0, height);
        }
        else if (currentType == ChangeType.MoveFromTop)
        {
            beforePos = basicPos - new Vector2(0, height);
            beforeOffset1 = basicPos - new Vector2(0, height);
            beforeOffset2 = basicPos - new Vector2(0, height);
        }
        else if (currentType == ChangeType.MoveFromLeft)
        {
            beforePos = basicPos - new Vector2(width, 0);
            beforeOffset1 = basicPos - new Vector2(width, 0);
            beforeOffset2 = basicPos - new Vector2(width, 0);
        }
        else if (currentType == ChangeType.MoveFromRight)
        {
            beforePos = basicPos + new Vector2(width, 0);
            beforeOffset1 = basicPos + new Vector2(width, 0);
            beforeOffset2 = basicPos + new Vector2(width, 0);
        }

        trans.interactable = false;

        bFadeOut = false;
        bOnFade = true;
        currentOpaicty = 0f;

        if(!noOpacity) trans.alpha = currentOpaicty;
        gameObject.SetActive(true);
    }
    
	public void Init(float speed = 2f, bool bOndestory = false, float fadeOutLimit = 1f, bool isHomeApp = true, bool noOpacity = false, ChangeType type = ChangeType.Fade)
    {
        after = new List<VoidFunction>();
        this.noOpacity = noOpacity;
        basicPos = ((RectTransform)transform).anchoredPosition;
        basicOffset1 = ((RectTransform)transform).offsetMax;
        basicOffset2 = ((RectTransform)transform).offsetMin;

        currentType = type;
        if (isFirst)
        {
            basicScale = ((RectTransform)transform).localScale;
            isFirst = false;
        }
        this.fadeOutLimit = fadeOutLimit;
        this.speed = speed;
        this.bOndestory = bOndestory;
        trans = GetComponent<CanvasGroup>();
        bFadeOut = false;
        bOnFade = false;
        if(isHomeApp) myHome = transform.parent.Find("Home").GetComponent<Home>();
    }
	
	void Update () {
        if(!trans) trans = GetComponent<CanvasGroup>();
        if (bOnFade)
        {
            if(bFadeOut)
            {
                currentOpaicty -= Time.deltaTime * speed * globalSpeed;

                if (!noOpacity) trans.alpha = currentOpaicty;
                if (currentType == ChangeType.Zoom)
                {
                    ((RectTransform)transform).localScale = Vector2.Lerp(new Vector2(0, 0), basicScale, Mathf.Clamp(currentOpaicty,0f, 1f));
                }
                else if(currentType == ChangeType.MoveFromDown || currentType == ChangeType.MoveFromTop || currentType == ChangeType.MoveFromRight || currentType == ChangeType.MoveFromLeft)
                {
                    ((RectTransform)transform).anchoredPosition = Vector2.Lerp(beforePos, basicPos, Mathf.Clamp(currentOpaicty, 0f, 1f));
                    //((RectTransform)transform).offsetMax = Vector2.Lerp(beforeOffset1, basicOffset1, Mathf.Clamp(currentOpaicty, 0f, 1f));
                    //((RectTransform)transform).offsetMin = Vector2.Lerp(beforeOffset2, basicOffset2, Mathf.Clamp(currentOpaicty, 0f, 1f));
                }

                if (currentOpaicty <= 0f)
                {
                    if (!noOpacity) trans.alpha = 0f;
                    bFadeOut = false;
                    bOnFade = false;
                    trans.interactable = true;
                    if (bOndestory) Destroy(gameObject);
                    else gameObject.SetActive(false);
                    if (after.Count > 0)
                    {
                        List<VoidFunction> sv_after = new List<VoidFunction>();
                        foreach (VoidFunction func in after)
                        {
                            sv_after.Add(func);
                        }
                        after.Clear();
                        foreach(VoidFunction func in sv_after)
                        {
                            func();
                        }
                        sv_after.Clear();
                    }
                }
            }
            else
            {
                currentOpaicty += Time.deltaTime * speed * globalSpeed;

                if (!noOpacity) trans.alpha = currentOpaicty;
                if (currentType == ChangeType.Zoom)
                {
                    ((RectTransform)transform).localScale = Vector2.Lerp(new Vector2(0, 0), basicScale, Mathf.Clamp(currentOpaicty, 0f, 1f));
                }
                else if (currentType == ChangeType.MoveFromDown || currentType == ChangeType.MoveFromTop || currentType == ChangeType.MoveFromRight || currentType == ChangeType.MoveFromLeft)
                {
                    ((RectTransform)transform).anchoredPosition = Vector2.Lerp(beforePos, basicPos, Mathf.Clamp(currentOpaicty, 0f, 1f));
                    //((RectTransform)transform).offsetMax = Vector2.Lerp(beforeOffset1, basicOffset1, Mathf.Clamp(currentOpaicty, 0f, 1f));
                    //((RectTransform)transform).offsetMin = Vector2.Lerp(beforeOffset2, basicOffset2, Mathf.Clamp(currentOpaicty, 0f, 1f));
                }

                if (currentOpaicty >= fadeOutLimit)
                {
                    if (!noOpacity) trans.alpha = 1f;
                    bFadeOut = false;
                    bOnFade = false;
                    trans.interactable = true;
                    if (after.Count > 0)
                    {
                        List<VoidFunction> sv_after = new List<VoidFunction>();
                        foreach (VoidFunction func in after)
                        {
                            sv_after.Add(func);
                        }
                        after.Clear();
                        foreach (VoidFunction func in sv_after)
                        {
                            func();
                        }
                        sv_after.Clear();
                    }
                }
            }
        }
	}
}
