﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class Notify : MonoBehaviour {
    GameMain gm;

    UnityEngine.UI.Image notify_icon;
    UnityEngine.UI.Text notify_name, notify_script;
    AppType curAppType;

    Notify_State curState;
    float timer;
    RectTransform rt;
    Next_Notify nextNotify;
    int saved_code; // 특별 수행을 위한 코드
    int next_code;

    //320 -> -50
    // Use this for initialization
    void Start () {
        gm = transform.parent.GetComponent<GameMain>();
        rt = GetComponent<RectTransform>();

        curState = Notify_State.Hide;
        nextNotify.text = "NoAlarm";

        notify_icon = transform.Find("AppIcon").GetComponent<UnityEngine.UI.Image>();
        notify_name = transform.Find("AppIcon").Find("AppName").GetComponent<UnityEngine.UI.Text>();
        notify_script = transform.Find("Script").GetComponent<UnityEngine.UI.Text>();
    }

    public void Click()
    {
        curState = Notify_State.Up;

        Transform app = transform.parent.Find("AppScreen").Find(curAppType.ToString() + "Main");

        // Exist
        if (app)
        {
            bool isSnsWithoutFade = gm.currentApp.name == app.gameObject.name && app.gameObject.name == "SnsMain";
            if (gm.currentApp.name != app.gameObject.name || isSnsWithoutFade)
            {
                if(!isSnsWithoutFade) gm.currentApp.GetComponent<FadeOutSystem>().FadeOut();
                if (saved_code != -1 && app.GetComponent<RandChatMain>())
                {
                    app.GetComponent<RandChatMain>().Reload();
                    app.GetComponent<RandChatMain>().ScrollDown();
                    app.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                    app.GetComponent<FadeOutSystem>().FadeIn(() => { app.GetComponent<RandChatMain>().OpenChat(saved_code); });
                }
                else if (saved_code != -1 && app.GetComponent<MessangerMain>())
                {
                    app.GetComponent<MessangerMain>().Reload();
                    app.GetComponent<MessangerMain>().ScrollDown();
                    app.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                    app.GetComponent<FadeOutSystem>().FadeIn(() => { app.GetComponent<MessangerMain>().OpenChat(saved_code); });
                }
                else if (app.GetComponent<SnsMain>())
                {
                    if (!isSnsWithoutFade) app.GetComponent<SnsMain>().Reload();
                    else app.GetComponent<SnsMain>().Resort();
                    if (!isSnsWithoutFade) app.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                    if (!isSnsWithoutFade) app.GetComponent<FadeOutSystem>().FadeIn(() => { app.GetComponent<SnsMain>().OpenPost(saved_code); });
                    else app.GetComponent<SnsMain>().OpenPost(saved_code);
                }
                else if (app.GetComponent<AchMain>())
                {
                    app.GetComponent<AchMain>().Reload();
                    app.GetComponent<FadeOutSystem>().FadeIn();
                }
                else if (app.GetComponent<ContactMain>())
                {
                    app.GetComponent<ContactMain>().Reload();
                    app.GetComponent<FadeOutSystem>().FadeIn();
                }
                else
                {
                    app.GetComponent<FadeOutSystem>().SetType(FadeOutSystem.ChangeType.Zoom);
                    app.GetComponent<FadeOutSystem>().FadeIn();
                }
            }
        }
        else
        {
            print("None Exist App Error");
        }
    }
    public void CloseNotify()
    {
        curState = Notify_State.Up;
    }

    public AudioClip alram;
    public void ShowNotify (AppType appCode, string text, int code = -1)
    {
        if (!GameMain.GetInstance().mainData.settingData.gameAlram) return;

        ToolClass.PlaySound(alram);
        ToolClass.MakeVibration();
        if (curState != Notify_State.Hide)
        {
            next_code = code;
            nextNotify.appCode = appCode;
            nextNotify.text = text;
        }
        else
        {
            saved_code = code;
            curState = Notify_State.Down;
            timer = 0f;

            notify_icon.sprite = Resources.Load<Sprite>("UI/icons/" + appCode.ToString());
            switch (appCode)
            {
                case AppType.Sns:
                    notify_name.text = "린스타그램";
                    break;
                case AppType.RandomChat:
                    notify_name.text = "유리병";
                    break;
                case AppType.Messenger:
                    notify_name.text = "토크";
                    break;
                case AppType.Clock:
                    notify_name.text = "시간";
                    break;
                case AppType.Timer:
                    notify_name.text = "타이머";
                    break;
                case AppType.SelectGame:
                    notify_name.text = "선택봇";
                    break;
                case AppType.Figure:
                    notify_name.text = "전시대";
                    break;
                case AppType.Game:
                    notify_name.text = "장조림을 찾아서";
                    break;
                case AppType.Ach:
                    notify_name.text = "도전과제";
                    break;
                default:
                    notify_name.text = appCode.ToString();
                    break;
            }
            curAppType = appCode;
            notify_script.text = text;
        }
    }

	// Update is called once per frame
	void Update () {
        Vector2 nextPos;
        switch (curState)
        {
            case Notify_State.Down:
                nextPos = rt.anchoredPosition;
                nextPos.y -= Time.deltaTime * 1000f;
                rt.anchoredPosition = nextPos;
                if (nextPos.y <= -50f)
                {
                    nextPos.y = -50f;
                    rt.anchoredPosition = nextPos;

                    curState = Notify_State.Stay;
                    timer = 0f;
                }
                break;
            case Notify_State.Stay:
                timer += Time.deltaTime;
                if(timer >= 3f || nextNotify.text != "NoAlarm")
                {
                    curState = Notify_State.Up;
                    timer = 0f;
                }
                break;
            case Notify_State.Up:
                nextPos = rt.anchoredPosition;
                nextPos.y += Time.deltaTime * 1000f;
                rt.anchoredPosition = nextPos;
                if (nextPos.y >= 320f)
                {
                    nextPos.y = 320f;
                    rt.anchoredPosition = nextPos;

                    // If have next notify
                    if (nextNotify.text != "NoAlarm")
                    {
                        notify_icon.sprite = Resources.Load<Sprite>("UI/icons/" + nextNotify.appCode.ToString());
                        switch (nextNotify.appCode)
                        {
                            case AppType.Sns:
                                notify_name.text = "린스타그램";
                                break;
                            case AppType.RandomChat:
                                notify_name.text = "유리병";
                                break;
                            case AppType.Messenger:
                                notify_name.text = "토크";
                                break;
                            case AppType.Clock:
                                notify_name.text = "시간";
                                break;
                            case AppType.Timer:
                                notify_name.text = "타이머";
                                break;
                            case AppType.SelectGame:
                                notify_name.text = "선택봇";
                                break;
                            case AppType.Figure:
                                notify_name.text = "전시대";
                                break;
                            case AppType.Game:
                                notify_name.text = "장조림을 찾아서";
                                break;
                            default:
                                notify_name.text = nextNotify.appCode.ToString();
                                break;
                        }
                        curAppType = nextNotify.appCode;
                        notify_script.text = nextNotify.text;
                        nextNotify.text = "NoAlarm";
                        saved_code = next_code;
                        next_code = -1;

                        curState = Notify_State.Down;
                    }
                    else curState = Notify_State.Hide;
                }
                break;
        }
	}

    struct Next_Notify
    {
        public AppType appCode;
        public string text;
    }
    enum Notify_State
    {
        Down,
        Up,
        Stay,
        Hide
    }
}
