﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MSGAlram : MonoBehaviour, IPointerClickHandler
{
    UnityEngine.UI.Image image;
    int charCode;
    bool onFadeIn;
    float timer;
    public void Init(string text, int charcode)
    {
        image = GetComponent<UnityEngine.UI.Image>();
        image.color = new Color(1, 1, 1, 0f);
        transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = text;
        this.charCode = charcode;
        onFadeIn = true;
        timer = 0f;
    }
    public void FadeOut()
    {
        onFadeIn = false;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        MessangerMain.GetInstance().ScrollDown();
        MessangerMain.GetInstance().OpenChat(charCode);
    }
    private void Update()
    {
        if (onFadeIn)
        {
            timer += Time.deltaTime * 10f;
            if (timer >= 15f) onFadeIn = false;
            image.color = new Color(1, 1, 1, timer);
        }
        else
        {
            timer -= Time.deltaTime * 10f;
            if (timer <= 0f)
            {
                Destroy(gameObject);
            }
            else
            {
                image.color = new Color(1, 1, 1, timer);
            }
        }
    }
}
