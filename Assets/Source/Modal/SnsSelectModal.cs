﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyDesign;
using MyTool;

public class SnsSelectModal : MonoBehaviour {
    public GameObject selectButton;
    public Transform listTarget;
    SnsPost targetPost;
    string[] selects;
    public void Init(string[] selects, SnsPost targetPost)
    {
        // 대화내용을 읽는다. 대화내용이 없으면 포스트 내용으로
        string lastCharacterId = "";
        for (int i = targetPost.currentSelectScript.currentPos - 1; i >= 0; --i)
        {
            if (ToolClass.ParseScript(targetPost.currentSelectScript.scripts[i]).type == ScriptType.COMMENT)
            {
                ScriptParsedData spd = ToolClass.ParseScript(targetPost.currentSelectScript.scripts[i]);
                string charId = spd.val.Split('$')[0];
                if (charId == "0")
                {
                    lastCharacterId = GameMain.GetInstance().mainData.snsId + " " + spd.val.Split('$')[1];
                    break;
                }
                else if (charId[0] == '@')
                {
                    lastCharacterId = charId.Substring(1) + " " + spd.val.Split('$')[1];
                }
                else
                {
                    lastCharacterId = ToolClass.GetCharacter(int.Parse(charId)).snsId + " " + spd.val.Split('$')[1];
                }
            }
        }
        if(lastCharacterId == "")
        {
            lastCharacterId = targetPost.spd.content;
        }
        transform.Find("Topbar").Find("Text").GetComponent<UnityEngine.UI.Text>().text = lastCharacterId;

        this.selects = selects;
        this.targetPost = targetPost;
        for (int i= 0; i < selects.Length; ++i)
        { 
            string selectString = selects[i];
            GameObject newObj = Instantiate(selectButton, listTarget);
            newObj.name = i + "";
            newObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = selectString;
            newObj.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { OnClick(int.Parse(newObj.name)); });
        }
        GetComponent<FadeOutSystem>().Init(4f, true, 1f, false, false); ;
        GetComponent<FadeOutSystem>().FadeIn();
    }
    public void OnClick(int code)
    {
        targetPost.OnSelectPush(selects[code]);
        GetComponent<FadeOutSystem>().FadeOut();
    }
    public void Close()
    {
        GetComponent<FadeOutSystem>().FadeOut();
    }
    void BackButtonProc()
    {
        if (!GetComponent<FadeOutSystem>().bFadeOut && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<FadeOutSystem>().FadeOut();
            }
        }
    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
}
