﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Modal : MonoBehaviour
{
    public GameObject okModal;
    public GameObject rcAlram;
    public GameObject msgAlram;
    public GameObject imageModal;
    public GameObject exitModal;
    public GameObject snsSelect;
    public GameObject ending;
    public GameObject setting;
    public GameObject okModal_Call;
    private static Modal instance;
    public static Modal GetInstance()
    {
        if (!instance)
        {
            instance = GameObject.FindObjectOfType<Modal>();
            if (!instance)
                Debug.LogError("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
    public enum OkModalType
    {
        Normal,
        Call
    }
    public void CreateNewOkModal(string text, OkModalType type = OkModalType.Normal)
    {
        GameObject newOkModal;
        switch (type)
        {
            case OkModalType.Call:
                newOkModal = okModal_Call;
                break;
            default:
                newOkModal = okModal;
                break;
        }
        if (!newOkModal) print("No okModal");
        else
        {
            Instantiate(newOkModal, transform).GetComponent<OkModal>().Init(text);
        }
    }
    public void CreateExitModal()
    {
        if (!exitModal) print("No exitModal");
        else
        {
            Instantiate(exitModal, transform).GetComponent<ExitModal>().Init();
        }
    }
    public void CreateRCAlram(string text, int charCode)
    {
        if (!rcAlram) print("No RCAlram");
        else
        {
            var rcAlrams = FindObjectOfType<RCAlram>();
            if(rcAlrams) rcAlrams.GetComponent<RCAlram>().FadeOut();
            Instantiate(rcAlram, transform).GetComponent<RCAlram>().Init(text, charCode);
        }
    }
    public void CreateMSGAlram(string text, int charCode)
    {
        if (!msgAlram) print("No MSGAlram");
        else
        {
            var msgAlrams = FindObjectOfType<MSGAlram>();
            if (msgAlrams) msgAlrams.GetComponent<MSGAlram>().FadeOut();
            Instantiate(msgAlram, transform).GetComponent<MSGAlram>().Init(text, charCode);
        }
    }
    public void CreateEnding(int charCode)
    {
        if (!ending) print("No Ending");
        else
        {
            Instantiate(ending, transform).GetComponent<EndingModal>().Init(charCode);
        }
    }
    public void CreateSettingModal()
    {
        if (!setting) print("No Setting");
        else
        {
            Instantiate(setting, transform).GetComponent<SettingModal>().Init();
        }
    }
    public void CreateImageModal(string imagePath)
    {
        if (!imageModal) print("No imageModal");
        else
        {
            Instantiate(imageModal, transform).GetComponent<ImageModal>().Init(imagePath);
        }
    }
    public void CreateImageModal(Sprite sp)
    {
        if (!imageModal) print("No RCAlram");
        else
        {
            Instantiate(imageModal, transform).GetComponent<ImageModal>().Init(sp);
        }
    }
    public void CreateSnsSelect(string[] selects, SnsPost targetPost)
    {
        if (!snsSelect) print("No RCAlram");
        else
        {
            Instantiate(snsSelect, transform).GetComponent<SnsSelectModal>().Init(selects, targetPost);
        }
    }
}