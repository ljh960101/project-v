﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageModal : MonoBehaviour
{
    public void Init(Sprite sp)
    {
        transform.Find("Background").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = sp;
        transform.Find("Btn_Back").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(Click);
        GetComponent<FadeOutSystem>().Init(4f, true, 1f, false);
        GetComponent<FadeOutSystem>().FadeIn();
    }
    public void Init(string text)
    {
        transform.Find("Background").Find("Image").GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("Image/" + text);
        transform.Find("Btn_Back").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(Click);
        GetComponent<FadeOutSystem>().Init(4f, true, 1f, false);
        GetComponent<FadeOutSystem>().FadeIn();
    }
    public void Click()
    {
        GetComponent<FadeOutSystem>().FadeOut();
    }

    public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
    public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.
    void BackButtonProc()
    {
        if (!GetComponent<FadeOutSystem>().bFadeOut && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<FadeOutSystem>().FadeOut();
            }
        }
    }
    void Update()
    {
        BackButtonProc();
        if (Input.touchCount == 2)
        {
            onZoom = true;
            transform.Find("Background").GetComponent<UnityEngine.UI.ScrollRect>().enabled = false;

            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            RectTransform photo_transform = (RectTransform)transform.Find("Background").Find("Image").transform;
            zoom += deltaMagnitudeDiff * orthoZoomSpeed * -0.01f;
            zoom = Mathf.Clamp(zoom, 1f, 4f);
            photo_transform.sizeDelta = new Vector2(1080 * zoom, 1450 * zoom);
        }
        else
        {
            onZoom = false;
            transform.Find("Background").GetComponent<UnityEngine.UI.ScrollRect>().enabled = true;
        }
    }
    float zoom;
    bool onZoom;
}
