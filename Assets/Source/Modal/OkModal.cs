﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OkModal : MonoBehaviour
{
    public UnityEngine.UI.Text myText;
    public UnityEngine.UI.Button myButton;
    public void Init(string text)
    {
        myText.text = text;
        myButton.onClick.AddListener(Click);
        GetComponent<FadeOutSystem>().Init(4f, true, 1f, false);
        GetComponent<FadeOutSystem>().FadeIn();
    }
    void BackButtonProc()
    {
        if (GameMain.GetInstance() && !GetComponent<FadeOutSystem>().bFadeOut && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<FadeOutSystem>().FadeOut();
            }
        }
    }
    public void Update()
    {
        BackButtonProc();
    }
    public void Click()
    {
        GetComponent<FadeOutSystem>().FadeOut();
    }
}
