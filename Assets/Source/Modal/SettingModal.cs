﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingModal : MonoBehaviour {
    public UnityEngine.UI.InputField msg, id;
    public void Init()
    {
        GetComponent<FadeOutSystem>().Init(4f, true, 1.0f, false);
    }
    public void Ok()
    {
        if(msg.text.Length<2 || id.text.Length < 2)
        {
            Modal.GetInstance().CreateNewOkModal("두글자 이상의 이름으로 입력해주세요.");
        }
        else {
            GetComponent<FadeOutSystem>().FadeOut(delegate { GameMain.GetInstance().gameStart = true; });
            GameMain.GetInstance().mainData.msgName = msg.text;
            GameMain.GetInstance().mainData.snsId = id.text;
        }
    }
}
