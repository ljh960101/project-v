﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitModal : MonoBehaviour
{
    public void Init()
    {
        transform.Find("Image").Find("Btn_Yes").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(ButtonYes);
        transform.Find("Image").Find("Btn_No").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(ButtonNo);
    }
    public void ButtonYes()
    {
        Application.Quit();
    }
    public void ButtonNo()
    {
        Destroy(gameObject);
    }
    private void FixedUpdate()
    {
        BackButtonProc();
    }
    void BackButtonProc()
    {
        if (!GetComponent<FadeOutSystem>().bFadeOut && GameMain.GetInstance().currentApp == gameObject && Application.platform == RuntimePlatform.Android && gameObject.activeSelf)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                GetComponent<FadeOutSystem>().FadeOut();
            }
        }
    }
}
