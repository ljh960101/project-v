﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyTool;

public class EndingModal : MonoBehaviour {
    List<string> scripts;
    public Transform tap_cg, tap_chat, tap_ending;
    public GameObject namePanel;
    public UnityEngine.UI.Text content, name, ending;
    public UnityEngine.UI.Image background, character, cg;
    int currentPos;
    float currentChatProgress;
    int currentCharCode;
    float chatSpeed = 10.0f;
    public void Init(int charCode = 1)
    {
        GameMain.GetInstance().gameStart = false;
        tap_cg.gameObject.SetActive(false);
        tap_chat.gameObject.SetActive(false);
        tap_ending.gameObject.SetActive(false);
        currentPos = -1;
        currentChatProgress = 0f;
        scripts = new List<string>();
        FillScriptByCharCode(charCode);
        GetComponent<FadeOutSystem>().Init(0.5f, true, 1.0f, false); ;
        tap_cg.gameObject.GetComponent<FadeOutSystem>().Init(0.5f, false, 1.0f, false);
        tap_chat.gameObject.GetComponent<FadeOutSystem>().Init(0.5f, false, 1.0f, false);
        tap_ending.gameObject.GetComponent<FadeOutSystem>().Init(0.5f, false, 1.0f, false);
        GetComponent<FadeOutSystem>().FadeIn();
        tap_cg.GetComponent<FadeOutSystem>().FadeIn();
        ending.text = ToolClass.GetCharacter(charCode).name + " 엔딩을 달성하셨습니다.";
    }
    void FillScriptByCharCode(int charCode)
    {
        currentCharCode = charCode;
        ToolClass.GetCharacter(charCode).afterEnding = true;
        if(charCode == 1)
        {
            scripts.Add("$arin_s4");
            scripts.Add("+그러고 보니 나한테 할 말 있다고 하지 않았어?");
            scripts.Add("$arin_s6");
            scripts.Add("응?");
            scripts.Add("아, 그거...!?");
            scripts.Add("+..?");
            scripts.Add("$arin_s4");
            scripts.Add("아직은 마음의 준비가 안됐는데...");
            scripts.Add("+뭔데 그래?");
            scripts.Add("-아린이는 쑥스러운지 땅만 쳐다보다 고개를 들어 나와 눈을 마주하였다.");
            scripts.Add("$arin_s2");
            scripts.Add("있잖아, 언니가 너.. 좋아해도 돼?");
        }
        else
        {
            scripts.Add("$arin_s4");
            scripts.Add("+그러고 보니 나한테 할 말 있다고 하지 않았어?");
            scripts.Add("$arin_s6");
            scripts.Add("응?");
            scripts.Add("아, 그거...!?");
            scripts.Add("+..?");
            scripts.Add("$arin_s4");
            scripts.Add("아직은 마음의 준비가 안됐는데...");
            scripts.Add("+뭔데 그래?");
            scripts.Add("-아린이는 쑥스러운지 땅만 쳐다보다 고개를 들어 나와 눈을 마주하였다.");
            scripts.Add("*30"); // 크기
            scripts.Add("/20"); // 속도
            scripts.Add("사실 진짜인지는  나도 잘 모르겠는데, 그러니까... 음, 막 내가 생각하기에는 조금 그런 거 같기도 하고, 이러면 네가 좋아할지도 잘 모르겠지만...");
            scripts.Add("/10");
            scripts.Add("*50");
            scripts.Add("$arin_s2");
            scripts.Add("있잖아, 언니가 너.. 좋아해도 돼?");
        }
    }
    public void Back()
    {
        if (currentPos >= 1)
        {
            --currentPos;
            currentChatProgress = 0f;
        }
    }
    void Update()
    {
        if (currentPos != -1 && currentPos < scripts.Count)
        {
            currentChatProgress += Time.deltaTime * chatSpeed;
            if (scripts[currentPos][0] == '-')
            {
                content.text = scripts[currentPos].Substring(1, Mathf.Clamp((int)currentChatProgress, 0, scripts[currentPos].Length - 1));
                namePanel.SetActive(false);
            }
            else if (scripts[currentPos][0] == '+')
            {
                content.text = scripts[currentPos].Substring(1, Mathf.Clamp((int)currentChatProgress, 0, scripts[currentPos].Length - 1));
                name.text = GameMain.GetInstance().mainData.msgName;
                namePanel.SetActive(true);
            }
            else if (scripts[currentPos][0] == '$')
            {
                character.sprite = Resources.Load<Sprite>("Image/" + scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                ++currentPos;
                currentChatProgress = 0f;
                content.text = "";
            }
            else if (scripts[currentPos][0] == '*') // 크기
            {
                content.fontSize = int.Parse(scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                ++currentPos;
                currentChatProgress = 0f;
                content.text = "";
            }
            else if (scripts[currentPos][0] == '/') // 속도
            {
                chatSpeed = float.Parse(scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                ++currentPos;
                currentChatProgress = 0f;
                content.text = "";
            }
            else
            {
                content.text = scripts[currentPos].Substring(0, Mathf.Clamp((int)currentChatProgress, 0, scripts[currentPos].Length));
                name.text = ToolClass.GetCharacter(currentCharCode).name;
                namePanel.SetActive(true);
            }
        }
    }
    public void OnClick()
    {
        if(currentPos == -1)
        {
            ++currentPos;
            currentChatProgress = 10000f;
            tap_cg.GetComponent<FadeOutSystem>().FadeOut();
            tap_chat.GetComponent<FadeOutSystem>().FadeIn();

            if (scripts[currentPos][0] == '$')
            {
                character.sprite = Resources.Load<Sprite>("Image/" + scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                ++currentPos;
                currentChatProgress = 0f;
            }
        }
        else if (currentPos != -1 && currentPos<scripts.Count)
        {
            if(scripts[currentPos].Length + 1 <= currentChatProgress)
            {
                ++currentPos;
                currentChatProgress = 0f;
                if(currentPos >= scripts.Count)
                {
                    tap_chat.GetComponent<FadeOutSystem>().FadeOut();
                    tap_ending.GetComponent<FadeOutSystem>().FadeIn();
                }
                else if (scripts[currentPos][0] == '$')
                {
                    character.sprite = Resources.Load<Sprite>("Image/" + scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                    ++currentPos;
                    currentChatProgress = 0f;
                }
                else if (scripts[currentPos][0] == '*') // 크기
                {
                    content.fontSize = int.Parse(scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                    ++currentPos;
                    currentChatProgress = 0f;
                }
                else if (scripts[currentPos][0] == '/') // 속도
                {
                    chatSpeed = float.Parse(scripts[currentPos].Substring(1, scripts[currentPos].Length - 1));
                    ++currentPos;
                    currentChatProgress = 0f;
                }
            }
            else
            {
                currentChatProgress = 10000f;
            }
        }
        else
        {
            GetComponent<FadeOutSystem>().FadeOut(delegate
            {
                GameMain.GetInstance().gameStart = true;
            });
        }
    }
}