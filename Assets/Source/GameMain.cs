﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyDesign;
using MyTool;
using Assets.SimpleAndroidNotifications;
using UnityEngine.SceneManagement;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using UnityEngine.Windows;
using System.IO;
#endif

public class GameMain : MonoBehaviour {
    const float touchEffect_respawnTime = 0.05f;
    [HideInInspector] public bool gameStart;

    [HideInInspector] public GameObject currentApp;
    [HideInInspector] public MainData mainData;
    [HideInInspector] public Notify notify;

    public bool skipMode, resetGameOnPlay, unlockScretApps;

    bool bOnTouch;
    Int32 myTouchid;
    Vector2 nowPoint;
    GameObject pt_move, pt_touchEnd;
    float touchEffect_respawn;
    RandChatMain rcm;
    MessangerMain msg;

    void Awake() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        ApplicationChrome.navigationBarState = ApplicationChrome.States.Hidden;
        ApplicationChrome.statusBarState = ApplicationChrome.States.Visible;
        
        if(skipMode) SpeedChange(!GameMain.GetInstance().skip);

        ToolClass.Load();

        bOnTouch = false;
        myTouchid = -1;
        touchEffect_respawn = 0f;
        lockedApps = new List<DockApp>();
        notify = transform.Find("Notify").GetComponent<Notify>();
        pt_move = Resources.Load<GameObject>("Prefab/Main/PT_Move");
        pt_touchEnd = Resources.Load<GameObject>("Prefab/Main/PT_TouchEnd");
        if (transform.Find("AppScreen")) rcm = transform.Find("AppScreen").Find("RandomChatMain").GetComponent<RandChatMain>();
        else rcm = transform.Find("RandomChatMain").GetComponent<RandChatMain>();
        msg = MessangerMain.GetInstance();
        // 기본 정보 입력 처리
        if(transform.parent == null || transform.parent.Find("TouchToStart") == null)
        {
            gameStart = true;
        }
        else {
            transform.parent.Find("TouchToStart").gameObject.SetActive(true);
            if (mainData.msgName == "[Default]")
            {
                Modal.GetInstance().CreateSettingModal();
            }
        }

    }
    void FixedUpdate()
    {
        TouchEffectProc();
        GameProc();
    }
    void OnApplicationQuit()
    {
        ToolClass.Save();
    }

    float typingSpeed = 6f;
    public void SetTypingSpeed(float val)
    {
        typingSpeed = val;
    }
    void OnApplicationPause(bool pauseStatus)
    {
        TimerMain.GetInstance().OnPause(pauseStatus);

        if (pauseStatus)
        {
            ToolClass.Save();
            // 인스타 알람넣기

            // SNS 스크립트 진행
            foreach (var postLog in mainData.snsLogs)
            {
                SnsPostData spd = ToolClass.GetSnsPost(postLog.postCode);
                if (!spd.isPosted || spd.isOver) continue;

                foreach (var postScript in spd.postScripts)
                {
                    if (postScript.isOver) continue;

                    string currentScript = postScript.scripts[postScript.currentPos];
                    ScriptParsedData parsedScript = ToolClass.ParseScript(currentScript);
                    if(parsedScript.type == ScriptType.WAIT)
                    {
                        for(int i= postScript.currentPos+1; i<postScript.scripts.Count; ++i)
                        {
                            var parsedScript2 = ToolClass.ParseScript(postScript.scripts[i]);
                            if (parsedScript2.type == ScriptType.WAIT || parsedScript2.type == ScriptType.IF || parsedScript2.type == ScriptType.SELECT_START || parsedScript2.type == ScriptType.SELECT_LIST)
                            {
                                break;
                            }
                            else if(parsedScript2.type == ScriptType.COMMENT)
                            {
                                SnsMain sm = SnsMain.GetInstance();
                                string[] data = parsedScript.val.Split('$');

                                // Get Lasted Comment
                                string lastCharacterId = "";
                                // 마지막 채팅 타겟이 캐릭터 인것만 올려준다.
                                for (int k = postScript.currentPos - 1; k >= 0; --k)
                                {
                                    if (ToolClass.ParseScript(postScript.scripts[k]).type == ScriptType.COMMENT)
                                    {
                                        string charId = ToolClass.ParseScript(postScript.scripts[k]).val.Split('$')[0];
                                        if (charId == "0")
                                        {
                                            string msg;
                                            if (parsedScript2.val.Split('$')[0] == "0") break;
                                            else if (parsedScript2.val.Split('$')[0][0] == '@') msg = parsedScript2.val.Split('$')[0].Substring(1) + " : " + parsedScript2.val.Split('$')[1];
                                            else msg = ToolClass.GetCharacter(int.Parse(parsedScript2.val.Split('$')[0])).snsId + " : " + parsedScript2.val.Split('$')[1];
                                            NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(float.Parse(parsedScript.val) - postScript.currentProcess),
                                                "내 폰 속 로맨스",
                                                msg, new Color(1, 0.3f, 0.15f));
                                            break;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            // 채팅 알람넣기
            foreach (var character in mainData.characters)
            {
                if(character.currentScriptCode != 0)
                {
                    var scripts = ToolClass.GetScript(character.currentScriptCode).scripts;
                    var currentScript = ToolClass.ParseScript(scripts[character.currentScriptPosition]);
                    if(currentScript.type == ScriptType.WAIT)
                    {
                        bool isNpc = false;
                        // NPC의 채팅인지 조사.
                        for(int i = character.currentScriptPosition-1; i>=0; --i)
                        {
                            if(ToolClass.ParseScript(scripts[i]).type == ScriptType.NPC_START || ToolClass.ParseScript(scripts[i]).type == ScriptType.NPC_NOREGI)
                            {
                                isNpc = true;
                                break;
                            }
                            else if (ToolClass.ParseScript(scripts[i]).type == ScriptType.PC_START)
                            {
                                isNpc = false;
                                break;
                            }
                            else if (ToolClass.ParseScript(scripts[i]).type == ScriptType.SELECT_END || ToolClass.ParseScript(scripts[i]).type == ScriptType.SELECT_LIST || ToolClass.ParseScript(scripts[i]).type == ScriptType.SELECT_START)
                            {
                                isNpc = false;
                                break;
                            }
                        }
                        
                        for(int i=character.currentScriptPosition+1; i<scripts.Count; ++i)
                        {
                                if (ToolClass.ParseScript(scripts[i]).type == ScriptType.WAIT || ToolClass.ParseScript(scripts[i]).type == ScriptType.SELECT_START || ToolClass.ParseScript(scripts[i]).type == ScriptType.SELECT_LIST)
                                {
                                    break;
                                }
                                else if (ToolClass.ParseScript(scripts[i]).type == ScriptType.NPC_START || ToolClass.ParseScript(scripts[i]).type == ScriptType.NPC_NOREGI) isNpc = true;
                                else if (ToolClass.ParseScript(scripts[i]).type == ScriptType.PC_START) isNpc = false;
                                else if (ToolClass.ParseToSmallScript(scripts[i]) != "")
                                {
                                    if (!isNpc) break;
                                    string msg;
                                    if (character.bOnChat) msg = character.name + " : " + ToolClass.ParseToSmallScript(scripts[i]);
                                    else msg = "낯선 사람 : " + ToolClass.ParseToSmallScript(scripts[i]);
                                    NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(float.Parse(currentScript.val) - character.currentScriptProcess),
                                                "내 폰 속 로맨스",
                                                msg, new Color(1, 0.3f, 0.15f));
                            }
                        }
                    }
                }
            }
        }
        else
        {
            NotificationManager.CancelAll();
        }
    }
    [HideInInspector] public bool skip = false;
    [HideInInspector] public List<DockApp> lockedApps;
    public AudioClip heartSound;
    // 채팅을 진행한다.
    void GameProc()
    {
        if (!gameStart) return;

        float deltaTime;
        if ((DateTime.Now - mainData.lastFrameTime).TotalSeconds >= 100) deltaTime = 100f;
        else deltaTime = (float)(DateTime.Now - mainData.lastFrameTime).TotalMilliseconds / 1000f;
        mainData.lastFrameTime = DateTime.Now;
        // 타이머 업데이트
        TimerMain.GetInstance().UpdateTimer(deltaTime);
        deltaTime *= mainData.settingData.speed;

        AchMain.GetInstance().UpdateAch();

        // SNS 스크립트 진행
        foreach (var postLog in mainData.snsLogs)
        {
            SnsPostData spd = ToolClass.GetSnsPost(postLog.postCode);
            if(!spd.isPosted || spd.isOver) continue;

            bool isOver = true;
            foreach(var postScript in spd.postScripts)
            {
                if(postScript.isOver) continue;
                isOver = false;

                string currentScript = postScript.scripts[postScript.currentPos];
                ScriptParsedData parsedScript = ToolClass.ParseScript(currentScript);
                switch (parsedScript.type)
                {
                    case ScriptType.LOVE:
                        ToolClass.GetCharacter(spd.charCode).love += int.Parse(parsedScript.val);
                        ++postScript.currentPos;
                        postScript.currentProcess = 0f;
                        Instantiate(Resources.Load<GameObject>("Prefab/Main/pt_heart"));
                        ToolClass.PlaySound(heartSound);
                        break;
                    case ScriptType.IF:
                        {
                            string[] condition = parsedScript.val.Split('$');
                            MyCharacter targetCharacter = ToolClass.GetCharacter(int.Parse(condition[0]));
                            if (targetCharacter.level >= int.Parse(condition[1]) &&
                                targetCharacter.scriptPriority >= int.Parse(condition[2]) &&
                                targetCharacter.love >= int.Parse(condition[3]))
                            {
                                ++postScript.currentPos;
                                postScript.currentProcess = 0f;
                            }
                            break;
                        }
                    case ScriptType.WAIT:
                        {
                            {
                                postScript.currentProcess += deltaTime;
                                if (postScript.currentProcess >= float.Parse(parsedScript.val) || skip)
                                {
                                    ++postScript.currentPos;
                                    postScript.currentProcess = 0f;
                                }
                            }
                            break;
                        }
                    case ScriptType.COMMENT:
                        {
                            SnsMain sm = SnsMain.GetInstance();
                            string[] data = parsedScript.val.Split('$');

                            // Get Lasted Comment
                            string lastCharacterId = "";
                            for(int i=postScript.currentPos-1; i>=0; --i)
                            {
                                if(ToolClass.ParseScript(postScript.scripts[i]).type == ScriptType.COMMENT)
                                {
                                    string charId = ToolClass.ParseScript(postScript.scripts[i]).val.Split('$')[0];
                                    if(charId == "0")
                                    {
                                        lastCharacterId = "@" + mainData.snsId + " ";
                                        break;
                                    }
                                    else if (charId[0] == '@')
                                    {
                                        lastCharacterId = charId + " ";
                                        break;
                                    }
                                    else
                                    {
                                        lastCharacterId = '@' + ToolClass.GetCharacter(int.Parse(charId)).snsId + " ";
                                        break;
                                    }
                                }
                            }

                            SnsMain.GetInstance().AddComment(postLog.postCode, data[1], data[0], lastCharacterId);
                            ++postScript.currentPos;
                            postScript.currentProcess = 0f;
                            break;
                        }
                    case ScriptType.SELECT_START:
                        {
                            if (postScript.currentProcess == 0f)
                            {
                                postScript.currentProcess = 1f;
                                SnsMain.GetInstance().Resort();
                            }
                            break;  
                        }
                    case ScriptType.SELECT_LIST:
                        {
                            // SE를 만날떄까지 이동
                            for (int k = postScript.currentPos + 1; k < postScript.scripts.Count; ++k)
                            {
                                ScriptParsedData scriptData = ToolClass.ParseScript(postScript.scripts[k]);
                                if (scriptData.type == ScriptType.SELECT_END)
                                {
                                    postScript.currentPos = k + 1;
                                    postScript.currentProcess = 0f;
                                    break;
                                }
                            }
                            break;
                        }
                    case ScriptType.SELECT_END:
                        {
                            ++postScript.currentPos;
                            postScript.currentProcess = 0f;
                            break;
                        }
                    case ScriptType.Like:
                        {
                            postLog.like += int.Parse(parsedScript.val);
                            ++postScript.currentPos;
                            postScript.currentProcess = 0f;
                            break;
                        }
                }
                if (postScript.currentPos >= postScript.scripts.Count)
                {
                    --postScript.currentPos;
                    postScript.isOver = true;
                }
            }

            if (isOver) spd.isOver = true;
        }

        // 채팅 스크립트 진행
        for (int charI = 0; charI < mainData.characters.Count; ++charI)
        {
            MyCharacter character = mainData.characters[charI];
            // 스크립트 진행중
            if (character.currentScriptCode != 0)
            {
                ChatScript currentScript = ToolClass.GetScript(character.currentScriptCode);
                // 스크립트 종료
                if (character.currentScriptPosition >= currentScript.scripts.Count)
                {
                    // 게임 성적 갱신
                    bool onScore = false;
                    bool alreadyChanged = false;
                    foreach (var score in mainData.miniGameData.scores)
                    {
                        ++(score.lastScriptCounter);
                        if (score.charCode == character.charCode)
                        {
                            onScore = true;
                            if (score.lastScriptCounter >= character.game_playCount && UnityEngine.Random.Range(0,4) == 1)
                            {
                                int myScore = 0;
                                foreach (var score2 in mainData.miniGameData.scores)
                                {
                                    if (score2.charCode == 0)
                                    {
                                        myScore = score2.score;
                                        break;
                                    }
                                }
                                int newScore = (int)(
                                    (float)myScore * ((float)character.game_physical - ((float)UnityEngine.Random.Range(0, character.game_offset * 2) - (float)character.game_offset))/100f
                                    ) + UnityEngine.Random.Range(0, 5);
                                if(newScore > score.score)
                                {
                                    score.score = newScore;
                                    MiniGameMain.GetInstance().RefreshRanking();
                                    bool alreadyCompetition = false;
                                    foreach (var _character in mainData.characters)
                                    {
                                        if (_character.onGameCompetition)
                                        {
                                            alreadyCompetition = true;
                                            break;
                                        }
                                    }

                                    // 게임중인 캐릭터가 없고, 랭킹을 갱신했으면
                                    if(!alreadyCompetition && newScore > myScore)
                                    {
                                        character.onGameCompetition = true;
                                        if (ToolClass.GetCharacterChatType(character) != ChatType.RANDCHAT) SendToChatLog("[L]", character.charCode);
                                        SendToChatLog("[W 7]", character.charCode);
                                        GotoNextScript(character);
                                        alreadyChanged = true;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    // 스코어가 존재하지 않는다면, 등록해줌.
                    if (!onScore)
                    {
                        MiniGameScore newScore = new MiniGameScore();
                        newScore.charCode = character.charCode;
                        newScore.lastScriptCounter = 0;
                        newScore.scoredDate = DateTime.Now;
                        newScore.score = character.game_defaultRanking;
                        mainData.miniGameData.scores.Add(newScore);
                        MiniGameMain.GetInstance().RefreshRanking();
                    }

                    // 스크립트 갱신
                    if (!alreadyChanged)
                    {
                        if (ToolClass.GetCharacterChatType(character) != ChatType.RANDCHAT) SendToChatLog("[L]", character.charCode);
                        SendToChatLog("[W 7]", character.charCode);
                        GotoNextScript(character);
                    }
                    continue;
                }

                // 스크립트 진행
                ScriptParsedData script = ToolClass.ParseScript(currentScript.scripts[character.currentScriptPosition]);
                //try
                {
                    switch (script.type)
                    {
                        case ScriptType.LOVE:
                            character.love += int.Parse(script.val);
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            Instantiate(Resources.Load<GameObject>("Prefab/Main/pt_heart"));
                            ToolClass.PlaySound(heartSound);
                            break;
                        case ScriptType.WAIT:
                            {
                                // 처음이라면 알림을 등록해준다.
                                character.currentScriptProcess += deltaTime;
                                if (character.currentScriptProcess >= float.Parse(script.val) || skip)
                                {
                                    ++character.currentScriptPosition;
                                    character.currentScriptProcess = 0f;
                                }
                            }
                            break;
                        case ScriptType.ENDING:
                            mainData.achMainData.loginCount += 1;
                            Modal.GetInstance().CreateEnding(character.charCode);
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.PC_START:
                        case ScriptType.NPC_START:
                            if (script.type == ScriptType.PHOTO) ToolClass.CollectPhoto(character.charCode, script.val);
                            SendToChatLog(currentScript.scripts[character.currentScriptPosition], character.charCode);
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            --charI;
                            continue;
                        case ScriptType.NPC_NOREGI:
                        case ScriptType.PHOTO:
                        case ScriptType.PROFILE:
                        case ScriptType.ADRESS:
                        case ScriptType.LINE:
                            if (script.type == ScriptType.PHOTO) ToolClass.CollectPhoto(character.charCode, script.val);
                            SendToChatLog(currentScript.scripts[character.currentScriptPosition], character.charCode);
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.GAME_NPC:
                            SendToChatLog("[GAME_NPC " + ToolClass.GetMiniGameScore(character.charCode) + "]", character.charCode);
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.GAME_PC:
                            SendToChatLog("[GAME_PC " + mainData.miniGameData.lastScore + "]", character.charCode);
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.NORMAL:
                            {
                                bool lastIsPC = false;
                                for (int i = character.currentScriptPosition - 1; i >= 0; i--)
                                {
                                    if (ToolClass.ParseScript(currentScript.scripts[i]).type == ScriptType.PC_START)
                                    {
                                        lastIsPC = true;
                                        break;
                                    }
                                    if (ToolClass.ParseScript(currentScript.scripts[i]).type == ScriptType.NPC_START || ToolClass.ParseScript(currentScript.scripts[i]).type == ScriptType.NPC_NOREGI)
                                    {
                                        lastIsPC = false;
                                        break;
                                    }
                                }
                                // PC의 대화 상태
                                if (lastIsPC)
                                {
                                    // 사진을 보고 있는 상태에서는 진행하지 않는다.
                                    if (FindObjectOfType<ImageModal>() != null)
                                    {
                                        continue;
                                    }

                                    if (ToolClass.GetCharacterChatType(character) == ChatType.RANDCHAT && rcm.currentScreen == RandChatMain.ERandomChatScreen.CHAT && rcm.chattingTargetCharCode == character.charCode && currentApp == rcm.gameObject)
                                    {
                                        character.currentScriptProcess += deltaTime * typingSpeed;
                                        if (script.val.Length <= (int)character.currentScriptProcess - 1)
                                        {
                                            SendToChatLog(currentScript.scripts[character.currentScriptPosition], character.charCode);
                                            ++character.currentScriptPosition;
                                            character.currentScriptProcess = 0f;
                                            rcm.SetTypeText("");
                                            //rcm.ScrollDown();
                                        }
                                        else
                                        {
                                            rcm.SetTypeText(script.val.Substring(0, (int)character.currentScriptProcess));
                                        }
                                    }
                                    else if (ToolClass.GetCharacterChatType(character) == ChatType.CHAT && msg.currentScreen == MessangerMain.EMSGScreen.CHAT && msg.chattingTargetCharCode == character.charCode && currentApp == msg.gameObject)
                                    {
                                        character.currentScriptProcess += deltaTime * typingSpeed;
                                        if (script.val.Length <= (int)character.currentScriptProcess - 1)
                                        {
                                            SendToChatLog(currentScript.scripts[character.currentScriptPosition], character.charCode);
                                            ++character.currentScriptPosition;
                                            character.currentScriptProcess = 0f;
                                            msg.SetTypeText("");
                                            //msg.ScrollDown();
                                        }
                                        else
                                        {
                                            msg.SetTypeText(script.val.Substring(0, (int)character.currentScriptProcess));
                                        }
                                    }
                                }
                                else
                                {
                                    SendToChatLog(currentScript.scripts[character.currentScriptPosition], character.charCode);
                                    ++character.currentScriptPosition;
                                    character.currentScriptProcess = 0f;
                                }
                            }
                            break;
                        case ScriptType.CHANGE_NAME:
                            character.name = script.val;
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.GOTO_LINE:
                            character.currentScriptPosition = int.Parse(script.val);
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.CHANGE_STATUS:
                            character.statusMsg = script.val;
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.CHANGE_PHOTO:
                            ToolClass.CollectPhoto(character.charCode, script.val);
                            character.curMsgPic = script.val;
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.SELECT_LIST:
                            // SE를 만날떄까지 이동
                            for (int k = character.currentScriptPosition + 1; k < currentScript.scripts.Count; ++k)
                            {
                                ScriptParsedData scriptData = ToolClass.ParseScript(currentScript.scripts[k]);
                                if (scriptData.type == ScriptType.SELECT_END)
                                {
                                    character.currentScriptPosition = k + 1;
                                    character.currentScriptProcess = 0f;
                                    break;
                                }
                            }
                            break;
                        case ScriptType.GAME_LOW:
                        case ScriptType.GAME_IGNORE:
                        case ScriptType.GAME_HIGH:
                        case ScriptType.GAME_SAME:
                            // GE를 만날떄까지 이동
                            for (int k = character.currentScriptPosition + 1; k < currentScript.scripts.Count; ++k)
                            {
                                ScriptParsedData scriptData = ToolClass.ParseScript(currentScript.scripts[k]);
                                if (scriptData.type == ScriptType.GAME_END)
                                {
                                    character.currentScriptPosition = k;
                                    character.currentScriptProcess = 0f;
                                    break;
                                }
                            }
                            break;
                        case ScriptType.GAME_END:
                            character.onGameCompetition = false;
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.SELECT_END:
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.SELECT_START:
                        case ScriptType.GAME_START:
                            if (character.currentScriptProcess != 1f)
                            {
                                character.currentScriptProcess = 1f;
                                SendToChatLog(currentScript.scripts[character.currentScriptPosition], character.charCode);
                            }
                            break;
                        case ScriptType.DEAD:
                            if (ToolClass.GetCharacterChatType(character) == ChatType.RANDCHAT)
                            {
                                rcm.ExitChatByChar(character.charCode);
                            }
                            break;
                        case ScriptType.TRIGGER_ON:
                            mainData.trigger[int.Parse(script.val)] = true;
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.TRIGGER_OFF:
                            mainData.trigger[int.Parse(script.val)] = false;
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            break;
                        case ScriptType.WAIT_TRIGGER:
                            int pos = int.Parse(script.val);
                            if (mainData.trigger[pos] && mainData.trigger[pos] == true)
                            {
                                ++character.currentScriptPosition;
                                character.currentScriptProcess = 0f;
                            }
                            break;
                        default:
                            ++character.currentScriptPosition;
                            character.currentScriptProcess = 0f;
                            print("예상치 못한 스크립트 : " + script.type.ToString());
                            break;
                    }
                }
                //catch
                {
                    //Modal.GetInstance().CreateNewOkModal("(" + character.currentScriptCode + ")" + character.currentScriptPosition+1 + "줄에서 오류 발생");
                }
            }
        }

        // SNS 해금 처리
        foreach (var post in mainData.posts)
        {
            if (!post.isPosted)
            {
                MyCharacter character = ToolClass.GetCharacter(post.charCode);
                if (character == null) continue;
                if (character.level >= post.level && character.scriptPriority >= post.priority && character.bOnSns)
                {
                    SnsMain.GetInstance().MakeLog(post);
                    ToolClass.CollectPhoto(post.charCode, post.picCode);
                }
            }
        }

        if (GetComponent<ScriptMaker>() || GetComponent<SnsMaker>()) return;

        // 잠긴 어플 해금 처리
        for (int i = lockedApps.Count - 1; i >= 0; --i)
        {
            var lockedApp = lockedApps[i];
            if (lockedApp.targetCharacter!=null && (lockedApp.targetCharacter.level >= 5 || (lockedApp.targetCharacter.charCode == 1 && lockedApp.targetCharacter.level >= 2)))
            {
                lockedApp.SetEnabled(true);
                lockedApps.Remove(lockedApp);
                ToolClass.MakeNotify(lockedApp.appType, "비밀 어플리케이션이 개방되었습니다.");
                lockedApp.myApp.GetComponent<AppMain>().AddAlram(1);
            }
        }

        // MiniGame 해금 처리
        foreach (var character in mainData.miniGameData.characters)
        {
            if (!character.onUnlocked)
            {
                MyCharacter ownerCharacer = ToolClass.GetCharacter(character.charCode);
                if (ownerCharacer != null &&
                    character.level <= ownerCharacer.level &&
                    character.priority <= ownerCharacer.scriptPriority &&
                    character.love <= ownerCharacer.love)
                {
                    character.onUnlocked = true;
                    character.onAlram = true;
                    MiniGameMain.GetInstance().MakeAlram();
                    MiniGameMain.GetInstance().RefreshCharacterList();
                }
            }
        }
    }
    public void GotoScriptMaker()
    {
        SceneManager.LoadScene("DeveloperMenu");
    }
    public void SpeedChange(bool val)
    {
        if (!val)
        {
            typingSpeed = 8f;
            skip = false;
        }
        else
        {
            typingSpeed = 80f;
            skip = true;
        }
    }
    public void Test()
    {
        foreach(var j in mainData.chatLogs)
        {
            if (j.charCode == 1)
            {
                for (int i = 1; i < 1000; ++i)
                {
                    BeforeChat test = new BeforeChat();
                    test.chat = "aasdad";
                    test.date = DateTime.Now;
                    j.beforeChats.Add(test);
                }
                break;
            }
        }
        //OnApplicationPause(true);
        /*for(int i=1; i<=8; ++i)
        {
            ToolClass.GetCharacter(i).level = ToolClass.GetCharacter(i).level + 1;
        }*/
        /*
        BeforeChat bc = new BeforeChat();
        bc.chat = "[Ph 1]";
        bc.date = DateTime.Now;
        mainData.randomChatLogs[0].beforeChats.Add(bc);
        rcm.Resort();*/

        /*ChatLog item = new ChatLog();
        BeforeChat bc = new BeforeChat();
        bc.chat = "12";
        bc.date = DateTime.Now;
        item.charCode = 1;
        item.beforeChats = new List<BeforeChat>();
        item.beforeChats.Add(bc);
        mainData.randomChatLogs.Add(item);
        rcm.Resort();*/

        /*++TTT;
        SendToChatLog(TTT +"\n2\n2\n4\n\n6", 1);*/
    }
    // 다음 스크립트로 넘겨줌
    public void GotoNextScript(MyCharacter character)
    {
        // 랜덤 채팅 상태 
        if (ToolClass.GetCharacterChatType(character.charCode) == ChatType.RANDCHAT)
        {
            if (character.isGuest && character.currentScriptCode!=0)
            {
                character.currentScriptCode = 0;
                character.currentScriptPosition = 0;
                character.currentScriptProcess = 0f;
                character.bOnRand = false;
                RandChatMain.GetInstance().ExitChatByChar(character.charCode);
            }
            else
            {
                // 마지막으로 한 것이 랜덤챗 종료 이벤트라면 더이상 진행하지 않음
                if (character.currentScriptCode != 0 && ToolClass.GetScript(character.currentScriptCode).scriptEventType == ScriptEventType.RANDOMCHAT_END)
                {
                    character.currentScriptCode = 0;
                    character.currentScriptPosition = 0;
                    character.currentScriptProcess = 0f;
                }
                // 아니라면 다른거 찾긔
                else
                {
                    ChatScript newScript;

                    // 호감도를 다채웠고, 중복 스크립트 중이라면 End로 넘겨줌
                    if (character.onOverlapScript && character.love >= 100)
                    {
                        newScript = GetNewScript(character.charCode, -1, -1, ScriptEventType.RANDOMCHAT_END);
                    }
                    else
                    {
                        newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);
                        // 현재 prioity에 스크립트가 없다면
                        if (newScript == null)
                        {
                            character.scriptPriority += 1;
                            newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);
                            // 모든 스크립트가 종료
                            if (newScript == null)
                            {
                                if (character.love >= 100)
                                {
                                    newScript = GetNewScript(character.charCode, -1, -1, ScriptEventType.RANDOMCHAT_END);
                                }
                                else
                                {
                                    character.onOverlapScript = true;
                                    --character.scriptPriority;
                                    newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);
                                }
                            }
                        }
                    }
                    character.currentScriptCode = newScript.scriptCode;
                    character.scriptCheck[ToolClass.GetScript(character.currentScriptCode).scriptCode] = true;
                    character.currentScriptPosition = 0;
                    character.currentScriptProcess = 0f;
                }
            }
        }
        // 채팅 상태
        else if (ToolClass.GetCharacterChatType(character.charCode) == ChatType.CHAT)
        {
            // 중복 실행중에 호감도가 다 채워진다면 레벨을 올려주고 중복 실행 제거.
            if(character.onOverlapScript && character.love >= 100 && GetNewScript(character.charCode, character.level+1, 0, ScriptEventType.NORMAL) != null)
            {
                ++character.level;
                character.scriptPriority = 1;
                character.onOverlapScript = false;
                character.love = 0;
                ToolClass.MakeNotify(AppType.Contact, character.name + "의 관계가 진전되었습니다.");
                ContactMain.GetInstance().GetComponent<AppMain>().AddAlram(1);
                ContactMain.GetInstance().Resort();
            }

            ChatScript newScript;
            if (character.onGameCompetition) {
                newScript = GetNewScript(character.charCode, character.level, -1, ScriptEventType.GAME_COMPETITION);
            }
            else newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);

            // 현재 priority에 스크립트가 없으면
            if (newScript == null)
            {
                character.scriptPriority += 1;
                newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);
                // 더이상 우선순위가 없음.
                if(newScript == null)
                {
                    // 모든 스크립트가 종료
                    
                    // 호감도가 높다면 레벨을 올려줌.
                    if (character.love >= 100)
                    {
                        ++character.level;
                        character.scriptPriority = 1;
                        character.onOverlapScript = false;
                        character.love = 0;

                        newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);

                        // 더이상 레벨이 없다.
                        if(newScript == null)
                        {
                            --character.level;
                            character.scriptCheck.Clear();
                            newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);
                        }
                        else
                        {
                            ToolClass.MakeNotify(AppType.Contact, character.name + "의 관계가 진전되었습니다.");
                            ContactMain.GetInstance().GetComponent<AppMain>().AddAlram(1);
                            ContactMain.GetInstance().Resort();
                        }
                    }
                    // 호감도가 미달성이라면 중독 스크립트 실행하도록
                    else
                    {
                        character.scriptPriority -= 1;
                        character.onOverlapScript = true;
                        newScript = GetNewScript(character.charCode, character.level, character.scriptPriority, ScriptEventType.NORMAL);
                    }
                }
            }
            character.currentScriptCode = newScript.scriptCode;
            character.scriptCheck[ToolClass.GetScript(character.currentScriptCode).scriptCode] = true;
            character.currentScriptPosition = 0;
            character.currentScriptProcess = 0f;
        }
    }
    public void SendToChatLog(string script, int charCode)
    {
        switch (ToolClass.GetCharacterChatType(charCode))
        {
            case ChatType.CHAT:
                {
                    msg.AddToLog(charCode, script);
                    break;
                }
            case ChatType.RANDCHAT:
                {
                    rcm.AddToLog(charCode, script);
                    break;
                }
            case ChatType.NO_CHAT:
                throw new Exception("No chat but yes script...");
        }
    }
    ChatScript GetNewScript(int charCode, int level, int priority, ScriptEventType type)
    {
        // 같은 캐릭터 코드를 찾고,
        // 거기서 우선순위와 레벨을 매칭한뒤
        // 메인 퀘스트, 원타임 이벤트, 등등을 체크한다.
        List<ChatScript> mainScripts = new List<ChatScript>();
        List<ChatScript> subScripts = new List<ChatScript>();

        foreach (ChatScript script in mainData.scripts)
        {
            int index = mainData.scripts.IndexOf(script);
            MyCharacter character = ToolClass.GetCharacter(charCode);
            if (type == script.scriptEventType &&
                charCode == script.charCode && // 캐릭터 코드가 같은가
                (level==-1 || level==script.level) && // 레벨이 같은가
                (priority==-1 || priority == script.priority) && // 우선순위가 같은가?
                (!script.isOnlyOneTime || !character.scriptCheck.ContainsKey(script.scriptCode) || 
                (character.scriptCheck.ContainsKey(script.scriptCode) && character.scriptCheck[script.scriptCode]==false)) &&
                // 원타임이 아닌가? 또는 원타임을 클리어 하지 않았는가?
                (!character.scriptCheck.ContainsKey(script.scriptCode) || (character.scriptCheck.ContainsKey(script.scriptCode) && !character.scriptCheck[script.scriptCode]) ||
                (character.scriptCheck.ContainsKey(script.scriptCode) && character.scriptCheck[script.scriptCode] && character.onOverlapScript))
                ) // 이미 클리어했는데 자유선택인가? 또는 클리어하지 않았는가?
            {
                if (script.isMain)
                {
                    mainScripts.Add(script);
                }
                else
                {
                    subScripts.Add(script);
                }
            }
        }

        // 메인퀘스트가 존재
        if (mainScripts.Count >= 1)
        {
            return mainScripts[UnityEngine.Random.Range(0, mainScripts.Count)];
        }
        // 서브퀘스트가 존재
        else if(subScripts.Count >= 1)
        {
            return subScripts[UnityEngine.Random.Range(0, subScripts.Count)];
        }
        // 더이상 퀘스트가 존재하지 않음
        else
        {
            return null;
        }
    }

    void AppTouchRealese()
    {
        // Spawn Effect
        GameObject particle = Instantiate(pt_touchEnd, nowPoint, transform.rotation);
        Vector3 particle_pos = particle.transform.position;
        particle_pos.z = -5f;
        particle.transform.position = particle_pos;

        bOnTouch = false;
    }
    bool onMouseDown;
    void TouchEffectProc()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButtonDown(0))
        {
            onMouseDown = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            onMouseDown = false;
            GameObject particle = Instantiate(pt_touchEnd, Camera.main.ScreenToWorldPoint(Input.mousePosition), transform.rotation);
            Vector3 particle_pos = particle.transform.position;
            particle_pos.z = -5f;
            particle.transform.position = particle_pos;
        }
        if (onMouseDown)
        {
            touchEffect_respawn -= Time.deltaTime;
            if (touchEffect_respawn < 0f)
            {
                touchEffect_respawn = touchEffect_respawnTime;
                GameObject particle = Instantiate(pt_move, Camera.main.ScreenToWorldPoint(Input.mousePosition), transform.rotation);
                Vector3 particle_pos = particle.transform.position;
                particle_pos.z = -5f;
                particle.transform.position = particle_pos;
            }
        }
#elif UNITY_ANDROID


        // Touch Realese
        if (Input.touchCount == 0 && myTouchid != -1)
        {
            myTouchid = -1;

            if (bOnTouch)
            {
                AppTouchRealese();
            }
        }

        // Touch Start
        if (Input.touchCount > 0 && myTouchid == -1)
        {
            bOnTouch = true;

            // Set Touch Info
            Touch touch = Input.GetTouch(0);
            myTouchid = touch.fingerId;
            Vector2 touchDeltaPosition = touch.position;
            nowPoint = Camera.main.ScreenToWorldPoint(touchDeltaPosition);

        }
        // Touch Move
        else if (bOnTouch)
        {
            bool bNoTouchId = true;
            for (int i = 0; i < Input.touchCount; ++i)
            {
                Touch touch = Input.GetTouch(i);
                if (touch.fingerId == myTouchid)
                {
                    // My Touch
                    bNoTouchId = false;

                    // Set Touch Info
                    Vector2 touchDeltaPosition = touch.position;
                    nowPoint = Camera.main.ScreenToWorldPoint(touchDeltaPosition);

                    // Respawn Effect Proc
                    touchEffect_respawn -= Time.deltaTime;
                    if (touchEffect_respawn < 0f)
                    {
                        touchEffect_respawn = touchEffect_respawnTime;

                        // Spawn Effect
                        GameObject particle = Instantiate(pt_move, nowPoint, transform.rotation);
                        Vector3 particle_pos = particle.transform.position;
                        particle_pos.z = -5f;
                        particle.transform.position = particle_pos;
                    }

                    break;
                }
            }

            if (bNoTouchId)
            {
                AppTouchRealese();

            }
        }
#endif
        }
    private Vector3 GetCursorPos()
    {
        return Input.GetTouch(0).position;
    }

    private static GameMain instance;
    public static GameMain GetInstance()
    {
        if (!instance)
        {
            instance = GameObject.FindObjectOfType<GameMain>();
            if (!instance)
                Debug.Log("There needs to be one active MyClass script on a GameObject in your scene.");
        }

        return instance;
    }
}